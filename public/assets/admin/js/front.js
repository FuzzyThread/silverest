/*global $, document, Chart, LINECHART, data, options, window*/
$(document).ready(function () {

    'use strict';

    // // Main Template Color
    var brandPrimary = '#2b90d9';

    // ------------------------------------------------------- //
    // Custom Scrollbar
    // ------------------------------------------------------ //
    if ($(window).outerWidth() > 992) {
        $("nav.side-navbar").niceScroll({
            cursorcolor: brandPrimary,
            cursorwidth: '3px',
            cursorborder: 'none',
            scrollspeed: 20,
            mousescrollstep: 60,
            autohidemode: true
        });
    }

    $(".total-notifications").niceScroll({
        cursorcolor: brandPrimary,
        cursorwidth: '3px',
        cursorborder: 'none',
        scrollspeed: 20,
        mousescrollstep: 60,
        autohidemode: true
    });

    // ------------------------------------------------------- //
    // Side Navbar Functionality
    // ------------------------------------------------------ //
    $('nav.side-navbar').toggleClass('show-sm');
    $('.page').toggleClass('active-sm');


    // ------------------------------------------------------- //
    // Login form validation
    // ------------------------------------------------------ //
    $('#login-form').validate({
        messages: {
            loginUsername: 'please enter your username',
            loginPassword: 'please enter your password'
        }
    });

    // ------------------------------------------------------- //
    // Welcome form validation
    // ------------------------------------------------------ //
    $('#register-form').validate({
        messages: {
            registerUsername: 'please enter your first name',
            registerEmail: 'please enter a vaild Email Address',
            registerPassword: 'please enter your password'
        }
    });

    // ------------------------------------------------------- //
    // Transition Placeholders
    // ------------------------------------------------------ //
    $('input').on('focus', function () {
        $(this).siblings('.label-custom').addClass('active');
    });

    $('input').on('blur', function () {
        $(this).siblings('.label-custom').removeClass('active');

        if ($(this).val() !== '') {
            $(this).siblings('.label-custom').addClass('active');
        } else {
            $(this).siblings('.label-custom').removeClass('active');
        }
    });


    // ------------------------------------------------------- //
    // Jquery Progress Circle
    // ------------------------------------------------------ //
    var progress_circle = $("#progress-circle").gmpc({
        color: brandPrimary,
        line_width: 5,
        percent: 0
    });
    progress_circle.gmpc('animate', 15, 800);

    // ------------------------------------------------------- //
    // External links to new window
    // ------------------------------------------------------ //

    $('.external').on('click', function (e) {

        e.preventDefault();
        window.open($(this).attr("href"));
    });

});
