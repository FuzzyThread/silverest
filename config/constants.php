<?php

return [
    'STATUS' => [
        'AWAITING_PAYMENT' => 1,
        'PREPARATION' => 2,
        'REFUNDED' => 3,
        'DISPATCHED' => 4,
        'COMPLETED' => 5,
        'CANCELLED' => 6,
        'PAYMENT_DECLINED' => 7,
        'ERROR' => 8
    ]
];
