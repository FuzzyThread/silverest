<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // security:
//        $this->call(LaratrustSeeder::class);

        // products:
//        $this->call(CategoriesSeeder::class);
//        $this->call(ColorsSeeder::class);
//        $this->call(MaterialsSeeder::class);
//        $this->call(SizesSeeder::class);
//        $this->call(StylesSeeder::class);
//        $this->call(TagsSeeder::class);

        // orders:
        $this->call(StatusSeeder::class);
//        $this->call(ProductSeeder::class);
    }


}
