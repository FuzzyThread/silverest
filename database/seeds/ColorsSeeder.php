<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating Colors Table');
        $this->truncateColors();

        $colors_arr = array('#008000' => 'LightSalmon', '#000' => 'Dark Salmon', '#ff0000' => 'Tomato', '#0000ff' => 'Deep Sky Blue',
            '#fff' => 'Electric Purple', '#42aaff' => 'Atlantis', '#C78DF6' => 'LightSalmon2', '#ffc0cb' => 'Dark Salmon2',
            '#ffff00' => 'Electric Purple2', '#964b00' => 'Atlantis2', 'transparent' => 'Transparent');

        foreach ($colors_arr as $color => $name) {
            $color = \App\Color::create([
                'value' => $color,
                'name' => $name,
            ]);
        }

        $this->command->info('Creating Colors Table');
    }

    public function truncateColors()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        \App\Color::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
