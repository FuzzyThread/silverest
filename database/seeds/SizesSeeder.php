<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating Sizes Table');
        $this->truncateSizes();

        $sizes_arr = array(7, 7.5, 8, 8.5, 9, 9.5, 10,
            10.5, 11, 11.5, 12, 12.5,
            13, 13.5, 14, 14.5, 15, 15.5,
            16, 16.5, 17, 17.5, 18, 18.5,
            19, 19.5, 20, 20.5, 21, 21.5,
            22, 22.5, 23, 23.5, 24);

        foreach ($sizes_arr as $size) {
            \App\Size::create([
                'value' => $size
            ]);
        }

        $this->command->info('Creating Sizes Table');
    }

    public function truncateSizes()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        \App\Size::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
