<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating Tags table');
        $this->truncateSizes();

        $tags_arr = array(
            array('Подвески из золота', 'Pendants made of gold', 'Kuldripatsid'),
            array('Подвески с бриллиантами', 'Pendants with Diamonds', 'Ripatsid teemantidega'),
            array('Украшения из золота', 'Jewelery from gold', 'Kullast ehted'),
            array('Украшения из золота с бриллиантами', 'Jewelery from gold with diamonds', 'Ehted kullast teemantidega'),
            array('Золотые украшения с авантюрином', 'Gold ornaments with aventurine', 'Ehted kullast avantüriiniga'),
            array('Украшения с бриллиантами', 'Jewelry with Diamonds', 'Ehted teemantidega'),
            array('Украшения для путешествий', 'Jewelery for Traveling', 'Ehted reisile'),
            array('Кольца из серебра', 'Rings of silver', 'Hõbesõrmused'),
            array('Кольца с фианитами', 'Rings with cubic Zirkonia', 'Sõrmused tsirkoonidega'),
            array('Кольца с эмалью', 'Rings with enamel', 'Sõrmused emailiga'),
            array('Кольца в виде листьев', 'Rings in the form of leaves', 'Lehekujulised sõrmused'),
            array('Украшения из серебра', 'Jewelery from silver', 'Hõbeehted'),
            array('Украшения из серебра с фианитами', 'Ornaments from silver with cubic Zirkonia', 'Hõbeehted tsirkoonidega'),
            array('Серебряные украшения с эмалью', 'Silver jewelry with enamel', 'Hõbeehted  emailiga'),
            array('Украшения с фианитами', 'Ornaments with cubic Zirkonia', 'Ehted tsirkoonidega'),
            array('Женские кольца', 'Women\'s Rings', 'Sõrmused naistele'),
            array('Украшения для женщин', 'Jewelery for Women', 'Ehted naistele'),
            array('Украшения для мужчин', 'Jewelery for men', 'Ehted meestele'),
            array('Серебряные подвески', 'Silver pendants', 'Hõberipatsid'),
            array('Золотые подвестки', 'Gold pendants', 'Kuldripatsid'),
            array('Подвески с фианитами', 'Pendants with cubic Zirkonia', 'Ripatsid tsirkoonidega'),
            array('Нательные крестики из серебра', 'Silver crosses made of silver', 'Hõberistid'),
            array('Кресты', 'Crosses', 'Ristid')
        );

        foreach ($tags_arr as $tag_name) {
            $tag = new  \App\Tag();
            $tag->setTranslation('name', 'ru', $tag_name[0]);
            $tag->setTranslation('name', 'en', $tag_name[1]);
            $tag->setTranslation('name', 'et', $tag_name[2]);
            $tag->save();
        }

        $this->command->info('Creating Tags Table');
    }

    public function truncateSizes()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        \App\Tag::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
