<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating Statuses Table');
        $this->truncateProductTables();

        $statuses_arr = array('#8a008a' => 'Awaiting Payment', '#003adc' => 'Preparation in Progress',
            '#5bc0de' => 'Refunded', '#f98e00' => 'Dispatched', '#4ac32b' => 'Completed', '#ff0000' => 'Cancelled',
            '#d7e219' => 'Payment Declined', '#888888' => 'Script Error');

        foreach ($statuses_arr as $color => $name) {
            $color = \App\Status::create([
                'value' => $color,
                'name' => $name,
            ]);
        }

        $this->command->info('Creating Statuses Table');
    }

    public function truncateProductTables()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        \App\Status::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
