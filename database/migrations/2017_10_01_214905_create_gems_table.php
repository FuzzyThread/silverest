<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gems', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->string('type')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });

        Schema::create('gem_product', function (Blueprint $table) {
            $table->integer('gem_id')->unsigned()->index();
            $table->foreign('gem_id')->references('id')->on('gems')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->index();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gems');
        Schema::dropIfExists('gem_product');
    }
}
