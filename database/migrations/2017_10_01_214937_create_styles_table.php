<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('styles', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->string('code');
            $table->timestamps();
        });

        Schema::create('product_style', function (Blueprint $table) {
            $table->integer('product_id')->unsigned()->index();
            $table->integer('style_id')->unsigned()->index();
            $table->foreign('style_id')->references('id')->on('styles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('styles');
        Schema::dropIfExists('product_style');
    }
}
