<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

    // Soft Delete:
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function items() {
        return $this->hasMany('App\Item');
    }

    public function status() {
        return $this->belongsTo('App\Status');
    }

    public function shipment() {
        return $this->hasOne('App\Delivery');
    }

    public function payment() {
        return $this->hasOne('App\Payment');
    }

    public function delivery() {
        return $this->hasOne('App\Delivery');
    }

    public function scopeSearch($query, $str)
    {
        return $query->where('id', 'like', '%' . $str . '%');
    }
}
