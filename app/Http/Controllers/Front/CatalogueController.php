<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\RecentProduct;
use App\Subcategory;
use App\Category;
use App\Product;
use App\Color;
use App\Style;
use App\Cart;
use App\Tag;
use App\Gem;

class CatalogueController extends Controller
{
    private $products_per_page = 24;

    public function index(Request $request)
    {
        $this->validate($request, [
            'min_price' => 'integer',
            'max_price' => 'integer',
            'color' => 'letters|nullable',
            'tag' => 'letters|nullable',
            'style' => 'letters|nullable',
            'gem' => 'letters|nullable',
            'view' => 'letters|nullable',
            'category' => 'letters|nullable',
            'sort' => 'letters|nullable',
            'search' => array(
                'nullable',
                'regex:/(^([\da-zA-z ]+)$)/u'
            )
        ]);

        $products = new Product();
        $max_price = Product::max('price');
        $min_price = Product::min('price');
        $category_name = '';
        $recent_products = null;
        $queries = [];
        $search_field = '';

        // recent products:
        if (Auth::user()) {
            $recent_products = Auth::user()->recent_products()->where('is_visible','1')->distinct()->get();
            if (count($recent_products) > 4) {
                $recent_products = $recent_products->take(-4);
            }
        }

        // categories that have subcategories that have products which are visible:
        $categories_with_subcategories = Category::whereHas('subcategories', function ($query) {
            $query->whereHas('products', function ($query) {
                $query->where('is_visible', '1');
            });
        })->get();

        // solo categories with products which are visible:
        $categories = Category::whereHas('products', function ($query) {
            $query->where('is_visible', '1');
        })->get();

        // combine categories and subcategories:
        $categories = $categories_with_subcategories->merge($categories);

        // only colors with products that are visible:
        $colors = Color::whereHas('products', function ($query) {
            $query->where('is_visible', '1');
        })->get();

        // only styles with products that are visible:
        $styles = Style::whereHas('products', function ($query) {
            $query->where('is_visible', '1');
        })->get();

        // only styles with products that are visible:
        $gems = Gem::whereHas('products', function ($query) {
            $query->where('is_visible', '1');
        })->get();

        // sort by color:
        if (request()->has('color')) {
            $color_name = request('color');
            $color = Color::where('name', $color_name)->first();
            if ($color) {
                $products = $color->products();
                $queries['color'] = $color_name;
            }
        }

        // sort by gem:
        if (request()->has('gem')) {
            $gem_code = request('gem');
            $gem = Gem::where('code', $gem_code)->first();
            if ($gem) {
                $products = $gem->products();
                $queries['gem'] = $gem_code;
            }
        }

        // sort by style:
        if (request()->has('style')) {
            $style_code = request('style');
            $style = Style::where('code', $style_code)->first();
            if ($style) {
                $products = $style->products();
                $queries['style'] = $style_code;
            }
        }

        // sort by category:
        if (request()->has('category')) {
            $category = null;
            $subcategory = null;
            $category_name = trim(request('category'));

            if (strpos($category_name, '_')) {
                $start_id = strpos($category_name, '_');
                $length = strlen($category_name) - $start_id;
                $subcategory_name = substr($category_name, $start_id, $length);
                $subcategory_name = substr($subcategory_name, 1, strlen($subcategory_name));

                if ($subcategory_name) {
                    $subcategory = Subcategory::where('code', $subcategory_name)->first();
                    $category = $subcategory ? $subcategory->category()->first() : null;
                }

            } else {
                if ($category_name) {
                    $category = Category::where('code', $category_name)->first();
                }
            }

            if ($subcategory && $category) {
                $products = $subcategory->products();
                $queries['category'] = $category_name;
                $category_name = ($category->name . ' ( ' . str_limit($subcategory->name,10) . ' ) ');
            } elseif ($category) {
                $products = $category->products();
                $queries['category'] = $category_name;
                $category_name = $category->name;
            } else {
                $category_name = 'Category Not Found';
            }
        }

        // sort by:
        if (request()->has('sort')) {

            switch (request('sort')) {
                case 'new':
                    $products = $products->orderBy('created_at', 'desc');
                    break;
                case 'price_down':
                    $products = $products->orderBy('price', 'desc');
                    break;
                case 'price_up':
                    $products = $products->orderBy('price', 'asc');
                    break;
                case 'bestseller':
                    $products = $products->orderBy('sold_number', 'desc');
                    break;
                case 'discount':
                    $products = $products->orderBy('discount_rate', 'desc'); // TODO: find a way to include discount date
                    break;
                default:
                    $products = $products->orderBy('views_number', 'desc');
            }
            $queries['sort'] = request('sort');
        } else {
            $products = $products->orderBy('views_number', 'desc');
        }

        // filter by price:
        if (request()->has('min_price') && request()->has('max_price')) {

            $min_price_request = request('min_price');
            $max_price_request = request('max_price');
            $products = $products->whereBetween('price', array($min_price_request, $max_price_request));
            $queries['min_price'] = $min_price_request;
            $queries['max_price'] = $max_price_request;
        }

        // search by product code or name:
        if (request()->has('search')) {
            $search_field = request('search');
            $products = $products->search($search_field);
            $queries['search'] = $search_field;
        }

        // change view type:
        if (request()->has('view') && request('view') == 'list') {
            $queries['view'] = request('view');
            $products = $products->where('is_visible', '1')->paginate($this->products_per_page - 8)->appends($queries);
        } else {
            $products = $products->where('is_visible', '1')->paginate($this->products_per_page)->appends($queries);
        }

        return view('front.catalogue.products', compact('products', 'categories', 'styles', 'gems', 'recent_products', 'colors', 'search_field', 'max_price', 'min_price','category_name'));
    }

    public function ajaxCatalogue(Request $request)
    {
        if ($request->ajax()) {

            $this->validate($request, [
                'min_price' => 'integer',
                'max_price' => 'integer',
                'color' => 'letters|nullable',
                'tag' => 'letters|nullable',
                'style' => 'letters|nullable',
                'gem' => 'letters|nullable',
                'view' => 'letters|nullable',
                'category' => 'letters|nullable',
                'sort' => 'letters|nullable',
                'search' => array(
                    'nullable',
                    'regex:/(^([\da-zA-z ]+)$)/u'
                )
            ]);

            $products = new Product();
            $queries = null;

            // sort by color:
            if ($request->has('color')) {
                $color_name = $request->color;
                $color = Color::where('name', $color_name)->first();
                if ($color) {
                    $products = $color->products();
                    $queries['color'] = $color_name;
                }
            }

            // sort by style:
            if ($request->has('style')) {
                $style_code = $request->style;
                $style = Style::where('code', $style_code)->first();
                if ($style) {
                    $products = $style->products();
                    $queries['style'] = $style_code;
                }
            }

            // sort by gem:
            if (request()->has('gem')) {
                $gem_code = request('gem');
                $gem = Gem::where('code', $gem_code)->first();
                if ($gem) {
                    $products = $gem->products();
                    $queries['gem'] = $gem_code;
                }
            }

            if (request()->has('category')) {
                $category = null;
                $subcategory = null;
                $category_name = trim(request('category'));

                if (strpos($category_name, '_')) {
                    $start_id = strpos($category_name, '_');
                    $length = strlen($category_name) - $start_id;
                    $subcategory_name = substr($category_name, $start_id, $length);
                    $subcategory_name = substr($subcategory_name, 1, strlen($subcategory_name));

                    if ($subcategory_name) {
                        $subcategory = Subcategory::where('code', $subcategory_name)->first();
                    }
                } else {
                    if ($category_name) {
                        $category = Category::where('code', $category_name)->first();
                    }
                }

                if ($subcategory) {
                    $products = $subcategory->products();
                    $queries['category'] = $category_name;
                } elseif ($category) {
                    $products = $category->products();
                    $queries['category'] = $category_name;
                }
            }

            // sort by price:
            if ($request->has('min_price') && $request->has('max_price')) {
                $min_price = $request->min_price;
                $max_price = $request->max_price;
                $products = $products->whereBetween('price', array($min_price, $max_price));
                $queries['min_price'] = $min_price;
                $queries['max_price'] = $max_price;
            }

            // search by:
            if ($request->has('search')) {
                $search = $request->search;
                $queries['search'] = $search;
                $products = $products->search($search);
            }

            // sort by:
            if ($request->has('sort')) {

                switch ($request->sort) {
                    case 'new':
                        $products = $products->orderBy('created_at', 'desc');
                        break;
                    case 'price_down':
                        $products = $products->orderBy('price', 'desc');
                        break;
                    case 'price_up':
                        $products = $products->orderBy('price', 'asc');
                        break;
                    case 'discount':
                        $products = $products->orderBy('discount_rate', 'desc');
                        break;
                    case 'bestseller':
                        $products = $products->orderBy('sold_number', 'desc');
                        break;
                    default:
                        $products = $products->orderBy('views_number', 'desc');
                }
                $queries['sort'] = $request->sort;
            } else {
                $products = $products->orderBy('views_number', 'desc');
            }

            $products = $products->where('is_visible', '1')->orderBy('id', 'desc');

            // change view type:
            if ($request->has('view') && $request->view == 'list') {
                $products = $products->paginate($this->products_per_page - 8);
                $queries['view'] = $request->view;
            } else {
                $products = $products->paginate($this->products_per_page);
            }

            $products = $products->appends($queries);

            // custom partial view:
            $product_catalogue = view("partials.front._product_catalogue", compact('products'))->render();
            return $product_catalogue;
        } else {
            return 'error';
        }
    }

    public function product($id)
    {
        $product = Product::findOrFail($id);
        $reviews = $product->reviews()->where('is_visible', '1')->paginate(4);
        $already_reviewed = false;

        //add to recent products:
        if (Auth::check()) {
            $user = Auth::user();
            $user->recent_products()->attach($id);
            $already_reviewed = $user->reviews->where('user_id', '=', $user->id)->where('product_id', '=', $product->id)->count() > 0;
        }
        if ($product->is_visible == 1) {
            $product->views_number = ($product->views_number + 1);
            $product->save();

            //get related products based on similar tags:
            $product_tags = $product->tags()->get();
            $match_accuracy = 1;
            $related_products = Product::where('is_visible', '1')->whereHas('tags', function ($q) use ($product_tags) {
                $q->whereIn('id', $product_tags);
            }, '=', $match_accuracy)->limit(12)->get();
            $sizes = $product->sizes()->orderBy('value', 'asc')->get();
            $set = $product->sets()->first();
            $set_products = array();
            if ($set) {
                $set_products = $set->products()->where('product_id', '!=', $product->id)->where('is_visible', '1')->get();
            }
            return view('front.catalogue.product', compact('product', 'related_products', 'sizes', 'set_products', 'product_tags', 'reviews', 'already_reviewed'));
        } else {
            return abort(404);
        }
    }

    // get product in cart preview:
    public function ajaxPreviewProduct(Request $request)
    {
        if ($request->ajax()) {

            $this->validate($request, [
                'product_id' => 'numbers',
            ]);

            $cart_product = Product::findOrFail($request->product_id);
            $sizes = $cart_product->sizes()->orderBy('value', 'asc')->get();

            //custom partial view:
            $cart = view("partials.front._cart_preview", compact('cart_product', 'sizes'))->render();
            return $cart;
        } else {
            return 'error';
        }
    }


}
