<?php

namespace App\Http\Controllers\Front;

use App\Mail\Quote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\Cancellation;
use App\RecentProduct;
use App\Mail\Invoice;
use App\Subcategory;
use App\Category;
use App\Delivery;
use App\Address;
use App\Payment;
use App\Product;
use App\Review;
use App\Status;
use App\Style;
use App\Color;
use App\Order;
use App\Cart;
use App\Item;
use App\Tag;
use App\Gem;
use PDF;

class OrderController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('front.account.orders', compact('user'));
    }

    public function order($id)
    {
        $user = Auth::user();
        if ($user->orders->contains($id)) {
            $order = $user->orders()->where('id', '=', $id)->first();
            if ($order) {
                return view('front.account.order', compact('user', 'order'));
            } else {
                return abort(404);
            }
        } else {
            return abort(404);
        }
    }

    public function cancelOrder(Request $request)
    {
        $user = Auth::user();
        $order = Order::findOrFail($request->order_id);

        if ($order
            &&  $order->payment
            && ($order->status_id == config('constants.STATUS.AWAITING_PAYMENT')
            ||  $order->status_id == config('constants.STATUS.PREPARATION'))
        ) {
            // Request cancellation:
            $order->status_id = config('constants.STATUS.CANCELLED');
            $order->save();

            // Cancellation email:
            Mail::to($user->email)->send(new Cancellation($user, $order));
            $message = 'Order: #' . $order->id . ' Cancelled';
        } else {
            $message = 'Order Already Cancelled';
        }
        session()->flash('notification', $message);
        return redirect()->back();
    }

    /**
     * Payment methods:
     * android_pay_card
     * apple_pay_card
     * credit_card
     * masterpass_card
     * paypal_account
     * venmo_account
     * visa_checkout_card
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function completeOrder(Request $request)
    {
        $user = Auth::user();
        $cart_amount = 0;
        $cart_total = 0;

        $this->validate($request, [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'email' => 'required|max:100|unique:users,email,' . $user->id,
            'phone_number' => 'required|max:100',
            'company_name' => 'nullable|max:100',
            'dob' => 'nullable|max:100',
            'delivery_method' => 'required|max:255',
            'address_line' => 'nullable|max:255',
            'city' => 'nullable|max:255',
            'state' => 'nullable|max:255',
            'country' => 'nullable|max:255',
            'zip' => 'nullable|max:10',
            'omniva_delivery' => 'nullable|max:255'
        ]);

        // Shipping total:
        $shipping_total = strcmp($request->delivery_method, "omniva") == 0 ? 3.0 : 5.0;

        // Cart total:
        $cart_products = $user->carts()->get()->keyBy('id');
        foreach ($cart_products as $cart_product) {

            // Check if product is in stock and visible:
            if ($cart_product->quantity < $cart_product->pivot->quantity || !$cart_product->is_visible) {
                Auth::user()->carts()->detach($cart_product);
                $cart_products->forget($cart_product->id);
            } else {
                $cart_total += (($cart_product->getDiscountPrice()) ? $cart_product->getDiscountPrice() : $cart_product->price) * $cart_product->pivot->quantity;
                $cart_amount += $cart_product->pivot->quantity;
            }
        }

        // Cart is not empty:
        if ($cart_amount > 0) {

            $user = $this->updateUser($request, $user);

            // Bank Quote:
            if ($request->quote) {

                $order = $this->createOrder($request, $user, $cart_products);

                // Payment:
                $this->createPayment($order_id = $order->id, '', 'quote', '', '', $shipping_total, $cart_total);

                // Quote email:
                $adminEmail = env("ADMIN_EMAIL", "silverest@silverest.ee");
                $quote = new Quote($user, $order);
                Mail::to($user->email)->bcc($adminEmail)->send($quote);

                // Session message:
                $message = 'Thank You!';
                session()->flash('thank_you', $message);
                return redirect(route('account.order', $order->id));

            } else {
                // TODO: add cart payment handler
            }
        } else {
            $message = 'Error';
            session()->flash('notification', $message);
            return redirect(route('account.order.checkout'));
        }
    }

    public function ajaxOrderNotes(Request $request)
    {
        if ($request->ajax()) {

            $this->validate($request, [
                'order_id' => 'numbers',
            ]);

            // check if user has the order:
            $user = Auth::user();
            $exists = $user->orders->contains($request->order_id);
            if ($exists) {
                $order = Order::findOrFail($request->order_id);
                $order_notes = view("partials.front._order_update_notes", compact('order'))->render();
                return $order_notes;
            } else {
                return 'access denied';
            }
        } else {
            return 'error';
        }
    }

    public function updateOrderNotes(Request $request)
    {
        $this->validate($request, [
            'notes' => 'required|max:255',
            'order_id' => 'numbers',
        ]);

        // Check if user has the order:
        $user = Auth::user();
        $exists = $user->orders->contains($request->order_id);
        if ($exists) {
            $order = Order::findOrFail($request->order_id);
            $order->notes = $request->notes;
            $order->save();
            $message = 'Order #' . $order->id . ' Notes Updated ';
        } else {
            $message = 'Access Denied';
        }

        session()->flash('notification', $message);
        return redirect()->back();
    }

    public function checkoutOrder()
    {
        $user = Auth::user();
        $cart_subtotal = 0;
        $cart_amount = 0;
        $cart_products = $user->carts()->get()->keyBy('id');

        foreach ($cart_products as $cart_product) {

            if ($cart_product->quantity < $cart_product->pivot->quantity) {
                $cart_products->forget($cart_product->id);
            } else {
                $cart_subtotal += (($cart_product->getDiscountPrice()) ? $cart_product->getDiscountPrice() : $cart_product->price) * $cart_product->pivot->quantity;
                $cart_amount += $cart_product->pivot->quantity;
            }
        }

        $delivery_address = $user->address()->first();
        return view('front.account.checkout', compact('user', 'delivery_address', 'cart_products', 'cart_amount', 'cart_subtotal'));
    }

    public function downloadInvoice($id)
    {
        $user = Auth::user();
        if ($user->orders->contains($id)) {
            $order = $user->orders()->where('id', '=', $id)->first();
            $pdf = PDF::loadView('pdfs.invoice', compact('order', 'user'));
            return $pdf->download('invoice_' . $order->id . '.pdf');
        } else {
            return abort(404);
        }
    }

    private function createOrder(Request $request, $user, $cart_products)
    {
        // Order:
        $order = new Order();
        $order->status_id = config('constants.STATUS.AWAITING_PAYMENT');
        $order->user_id = $user->id;
        $order->save();

        // Delivery:
        $delivery = new Delivery();
        $delivery->order_id = $order->id;
        $delivery->method = $request->delivery_method;
        $delivery->customer_name = ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
        $delivery->customer_phone = $user->phone_number;
        $delivery->customer_email = $user->email;
        $delivery->customer_address = strcmp($request->delivery_method, "omniva") == 0 ? $request->omniva_delivery : $request->address_line . ', ' . $request->city . ', ' . $request->state . ', ' . $request->country . ', ' . $request->zip;
        $delivery->save();

        // Items:
        foreach ($cart_products as $cart_product) {

            // Product update:
            $product = Product::find($cart_product->id);
            $product->quantity = $product->quantity - $cart_product->pivot->quantity;
            $product->is_visible = ($product->quantity > 0) ? true : false;
            $product->save();

            // Item:
            $order_item = new Item();
            $order_item->order_id = $order->id;
            $order_item->quantity = $cart_product->pivot->quantity;
            $order_item->product_id = $product->id;
            $order_item->size = $cart_product->pivot->size;
            $order_item->discount_price = $cart_product->pivot->discount_price;
            $order_item->save();
        }

        // Clear the cart:
        $user->carts()->detach();

        return $order;
    }

    private function updateUser(Request $request, $user)
    {
        // User:
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->dob = $request->dob;
        $user->company_name = $request->company_name;
        $user->save();

        // Address:
        $user_address = $user->address()->first();
        if (!$user_address) {
            $user_address = new Address();
            $user_address->user_id = $user->id;
        }
        $user_address->city = $request->city;
        $user_address->state = $request->state;
        $user_address->country = $request->country;
        $user_address->zip = $request->zip;
        $user_address->address_line = $request->address_line;
        $user_address->save();

        return $user;
    }

    private function createPayment($order_id, $braintree_id, $method, $card_type, $card_last_digits, $shipping_total, $cart_total)
    {
        $payment = new Payment();
        $payment->order_id = $order_id;
        $payment->braintree_id = $braintree_id;
        $payment->method = $method;
        $payment->card_type = $card_type;
        $payment->card_last_digits = $card_last_digits;
        $payment->shipping_total = $shipping_total;
        $payment->cart_total = $cart_total;
        $payment->subtotal = ($cart_total + $shipping_total);
        $payment->save();
    }
}
