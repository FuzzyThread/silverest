<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Address;

class AccountController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $orders_amount = $user->orders()->count();
        $wishlist_amount = $user->wishlists()->count();
        $cart_amount = $user->carts()->count();

        return view('front.account.dashboard', compact('orders_amount', 'wishlist_amount', 'cart_amount'));
    }

    public function editAccount()
    {
        $user = Auth::user();
        $delivery_address = $user->address()->first();
        return view('front.account.edit', compact('user', 'delivery_address'));
    }

    public function updateAccount(Request $request)
    {

        $user = Auth::user();

        $this->validate($request, [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'email' => 'required|max:100|unique:users,email,' . $user->id,
            'phone_number' => 'required|max:100',
            'company_name' => 'nullable|max:100',
            'dob' => 'nullable|max:100',
            'address_line' => 'required|max:255',
            'city' => 'required|max:255',
            'state' => 'required|max:255',
            'country' => 'required|max:255',
            'zip' => 'required|max:10'
        ]);

        // Update user:
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->dob = $request->dob;
        $user->company_name = $request->company_name;

        if ($request->password && $request->password == $request->confirm_password) {
            $user->password = bcrypt($request->password);
        }

        $user->save();

        // Update delivery address:

        $delivery_address = $user->address()->first();

        if (!$delivery_address) {
            $delivery_address = new Address();
            $delivery_address->user_id = $user->id;
        }

        $delivery_address->city = $request->city;
        $delivery_address->state = $request->state;
        $delivery_address->country = $request->country;
        $delivery_address->zip = $request->zip;
        $delivery_address->address_line = $request->address_line;
        $delivery_address->save();

        $message = 'Account Updated';
        session()->flash('notification', $message);
        return redirect(route('account'));
    }
}
