<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Review;

class ReviewController extends Controller
{
    public function publish(Request $request)
    {
        $this->validate($request, [
            'g-recaptcha-response' => 'required|recaptcha',
            'review' => 'required|max:500',
            'score' => 'required|numbers',
            'product_id' => 'required|numbers'
        ]);

        $user = Auth::user();
        $review = new Review();
        $review->user_id = $user->id;
        $review->product_id = $request->product_id;
        $review->body = $request->review;
        $review->score = $request->score;
        $review->save();

        $message = 'Review Published';
        session()->flash('notification', $message);
        return redirect()->back();
    }
}
