<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Photo;
use Image;
use Session;
use DB;

class PhotoController extends Controller
{

    public function index()
    {
        $photos = new Photo();
        $queries = [];
        $search_field = '';

        //search by:
        if (request()->has('search')) {
            $search_field = request('search');
            $photos = $photos->search($search_field);
            $queries['search'] = $search_field;
        }

        //sort by:
        if (request()->has('sort')) {
            $photos = $photos->orderBy('id', request('sort'));
            $queries['sort'] = request('sort');
        } else {
            $photos = $photos->orderBy('id', 'desc');
        }

        $photos = $photos->paginate(32)->appends($queries);

        return view('admin.photos.index', compact('photos', 'search_field'));
    }

    public function store(Request $request)
    {
        if ($file = $request->file('file')) {

            //save file to images folder with name modified:
            $path = time() . $file->getClientOriginalName();
            $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file->move('photos', $path);

            //preview
            Image::make('photos/' . $path)->resize(400, 400)->save('preview/' . $path);

            //save file path to DB:
            $photo = new Photo();
            $photo->name = $name;
            $photo->path = $path;
            $photo->save();

            //custom:
            $photos = Photo::orderBy('updated_at', 'desc')->paginate(32);
            $photo_card = view("partials.admin._photo_gallery", compact('photos'))->render();
            return $photo_card;
        }
    }

    public function edit($id)
    {
        $photo = Photo::where('id', $id)->first();
        return view("admin.photos.edit")->withPhoto($photo);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $photo = Photo::findOrFail($id);

        if ($file = $request->file('image')) {

            //save file to images folder with name modified:
            $path = time() . $file->getClientOriginalName();
            $file->move('photos', $path);

            //preview
            Image::make('photos/' . $path)->resize(400, 400)->save('preview/' . $path);

            //save file path to DB:
            $photo->path = $path;
        }

        $photo->name = $request->name;

        if ($photo->save()) {

            //show notification:
            $message = 'Photo updated : ' . str_limit($photo->name,20);
            session()->flash('notification', $message);

            return redirect()->route('photos.index');

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('photos.index');
        }
    }

    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);

        //delete user photo:
        unlink(public_path() . $photo->getPhotoPath());
        unlink(public_path() . $photo->getPreviewPath());

        $photo->delete();

        //show notification:
        $message = 'Photo deleted : ' . str_limit($photo->name,20);
        session()->flash('notification', $message);

        return redirect()->route('photos.index');

    }

    public function dropzoneRemove(Request $request)
    {
        if ($request->ajax()) {
            $photo = Photo::find($request->photoId); //Get image by id or desired parameters

            $destinationPath = '/photos/';

            if (File::exists($destinationPath . $photo->file_name)) { //Check if file exists
                unlink(public_path() . $photo->path);
            }
            $photo->delete();   //Delete file record from DB
            return response('Photo deleted', 200); //return success
        }
    }


}
