<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subcategory;
use App\Material;
use App\Category;
use App\Product;
use App\Photo;
use App\Color;
use App\Style;
use App\Size;
use App\Gem;
use App\Tag;
use Session;
use DB;

class ProductController extends Controller
{

    public function index()
    {
        $products = new Product();
        $queries = [];
        $search_field = '';

        //search by:
        if (request()->has('search')) {
            $search_field = request('search');
            $products = $products->search($search_field);
            $queries['search'] = $search_field;
        }

        //sort:
        if (request()->has('sort_date')) {
            $products = $products->orderBy('created_at', request('sort_date'));
            $queries['sort_date'] = request('sort_date');
        } elseif (request()->has('sort_code')){
            $products = $products->orderBy('code', request('sort_code'));
            $queries['sort_code'] = request('sort_code');
        } else {
            $products = $products->orderBy('created_at', 'desc');
        }


        $products = $products->with('categories', 'sizes', 'gems', 'colors', 'styles', 'tags')->paginate(20)->appends($queries);
        return view('admin.products.index', compact('products', 'search_field'));

    }

    public function create()
    {
        $styles = Style::pluck('name', 'id')->all();
        $sizes = Size::pluck('value', 'id')->all();
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $tags = Tag::pluck('name', 'id')->all();
        $colors = Color::pluck('value', 'id')->all();
        $gems = Gem::all();
        $materials = Material::all();
        $photos = Photo::orderBy('id', 'desc')->get();
        return view('admin.products.create', compact('categories', 'subcategories', 'styles', 'sizes', 'tags', 'materials', 'colors', 'gems', 'photos'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'code' => 'required|max:100',
            'price' => 'required|numeric|min:0.01',
            'weight' => 'required|numeric|min:0.01',
            'quantity' => 'required|integer|min:0|max:100',
        ]);

        $product = new Product();
        $product->setTranslation('name', 'en', $request->name_en);
        $product->setTranslation('name', 'ru', $request->name_ru);
        $product->setTranslation('name', 'et', $request->name_et);
        $product->code = $request->code;
        $product->weight = $request->weight;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->discount_rate = $request->discount_rate;

        if ($product->discount_rate == 0) {
            $product->discount_end_date = null;
        } else {
            $product->discount_end_date = $request->discount_end_date;
        }

        $product->is_visible = isset($request->is_visible) ? true : false;
        $product->is_featured = isset($request->is_featured) ? true : false;

        if ($material = Input::get('material')) {
            $product->material_id = $material;
        }

        if ($product->save()) {

            $categories = Input::get('categories');
            $category_arr = array();
            $subcategory_arr = array();

            if ($categories) {
                // split categories and subcategories:
                foreach ($categories as $category) {
                    if (substr($category, 0, 1) === '_') {
                        array_push($subcategory_arr, substr($category, 1, strlen($category)));
                    } else {
                        array_push($category_arr, $category);
                    }
                }

                // add from subcategory to category:
                if ($subcategory_arr) {
                    $categories_extra = Category::whereHas('subcategories', function ($q) use ($subcategory_arr) {
                        $q->whereIn('id', $subcategory_arr);
                    })->pluck('id')->toArray();
                    $category_arr = array_merge($category_arr, $categories_extra);
                }
            }

            $product->categories()->sync($category_arr);
            $product->subcategories()->sync($subcategory_arr);

            $sizes = Input::get('sizes');
            $product->sizes()->sync($sizes);

            $styles = Input::get('styles');
            $product->styles()->sync($styles);

            $tags = Input::get('tags');
            $product->tags()->sync($tags);

            $colors = Input::get('colors');
            $product->colors()->sync($colors);

            $gems = Input::get('gems');
            $product->gems()->sync($gems);

            $photos = Input::get('photos');
            $product->photos()->sync($photos);
        }

        //show notification:
        $message = 'Product created : ' . $product->name . ' ( ' . $product->code . ' ) ';
        session()->flash('notification', $message);

        return redirect()->route('products.index');
    }

    public function edit($id)
    {
        $product = Product::where('id', $id)->with('categories', 'photos', 'sizes', 'gems', 'colors', 'styles', 'tags')->first();
        $styles = Style::pluck('name', 'id')->all();
        $sizes = Size::pluck('value', 'id')->all();
        $subcategories = Subcategory::all();
        $categories = Category::all();
        $tags = Tag::pluck('name', 'id')->all();
        $colors = Color::pluck('value', 'id')->all();
        $gems = Gem::all();
        $materials = Material::all();
        $photos = Photo::orderBy('id', 'desc')->get();

        return view('admin.products.edit', compact('product', 'subcategories', 'categories', 'styles', 'sizes', 'tags', 'materials', 'colors', 'gems', 'photos'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'code' => 'required|max:6',
            'price' => 'required|numeric|min:0.01',
            'weight' => 'required|numeric|min:0.01',
            'quantity' => 'required|integer|min:0|max:10000'
        ]);

        $product = Product::findOrFail($id);
        $product->setTranslation('name', 'en', $request->name_en);
        $product->setTranslation('name', 'ru', $request->name_ru);
        $product->setTranslation('name', 'et', $request->name_et);
        $product->code = $request->code;
        $product->weight = $request->weight;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->is_visible = isset($request->is_visible) ? true : false;
        $product->is_featured = isset($request->is_featured) ? true : false;
        $product->discount_rate = $request->discount_rate;

        if ($product->discount_rate == 0) {
            $product->discount_end_date = null;
        } else {
            $product->discount_end_date = $request->discount_end_date;
        }

        if ($material = Input::get('material')) {
            $product->material_id = $material;
        }

        if ($product->save()) {

            $categories = Input::get('categories');
            $category_arr = array();
            $subcategory_arr = array();

            if ($categories) {
                // split categories and subcategories:
                foreach ($categories as $category) {
                    if (substr($category, 0, 1) === '_') {
                        array_push($subcategory_arr, substr($category, 1, strlen($category)));
                    } else {
                        array_push($category_arr, $category);
                    }
                }

                // add from subcategory to category:
                if ($subcategory_arr) {
                    $categories_extra = Category::whereHas('subcategories', function ($q) use ($subcategory_arr) {
                        $q->whereIn('id', $subcategory_arr);
                    })->pluck('id')->toArray();
                    $category_arr = array_merge($category_arr, $categories_extra);
                }
            }

            $product->categories()->sync($category_arr);
            $product->subcategories()->sync($subcategory_arr);

            $sizes = Input::get('sizes');
            $product->sizes()->sync($sizes);

            $styles = Input::get('styles');
            $product->styles()->sync($styles);

            $tags = Input::get('tags');
            $product->tags()->sync($tags);

            $colors = Input::get('colors');
            $product->colors()->sync($colors);

            $gems = Input::get('gems');
            $product->gems()->sync($gems);

            $photos = Input::get('photos');
            $product->photos()->sync($photos);
        }

        //show notification:
        $message = 'Product updated : ' . $product->name . ' ( ' . $product->code . ' ) ';
        session()->flash('notification', $message);

        return redirect()->route('products.index');
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        //show notification:
        $message = 'Product deleted : ' . $product->name . ' ( ' . $product->code . ' ) ';
        session()->flash('notification', $message);

        return redirect()->route('products.index');
    }

    public function productVisibleChange(Request $request)
    {
        $product = Product::findOrFail($request->product_id);
        if ($request->is_visible) {
            $product->is_visible = ($request->is_visible === 'true');
        }
        if ($product->save()) {
            return response('Product ' . $product->name . ' Visible : ' . $request->is_visible, 200); //return success
        } else {
            return response('Error with saving product', 200); //return success
        }
    }

    public function productQuantityChange(Request $request)
    {
        $product = Product::findOrFail($request->product_id);
        if ($request->quantity || $request->quantity == 0) {
            $product->quantity = $request->quantity;
        }
        if ($product->save()) {
            return response('Product ' . $product->name . ' Quantity : ' . $request->quantity, 200); //return success
        } else {
            return response('Error with saving product', 200); //return success
        }
    }


}
