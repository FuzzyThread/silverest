<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tag;
use Session;
use DB;

class TagController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function index()
    {
        $tags = Tag::orderBy('id','asc')->paginate(10);
        return view('admin.tags.index')->withTags($tags);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255'
        ]);

        $tag = new Tag();
        $tag->setTranslation('name', 'en', $request->name_en);
        $tag->setTranslation('name', 'ru', $request->name_ru);
        $tag->setTranslation('name', 'et', $request->name_et);

        if ($tag->save()) {

            //show notification:
            $message = 'Tag added : ' .  $tag->name;
            session()->flash('notification', $message);

            return redirect()->route('tags.index', $tag->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('tags.index');
        }
    }

    public function show($id)
    {
        $tag = Tag::where('id',$id)->with('products')->first();
        return view("admin.tags.show")->withTag($tag);
    }

    public function edit($id)
    {
        $tag = Tag::where('id',$id)->first();
        return view("admin.tags.edit")->withTag($tag);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255'
        ]);

        $tag = Tag::findOrFail($id);
        $tag->setTranslation('name', 'en', $request->name_en);
        $tag->setTranslation('name', 'ru', $request->name_ru);
        $tag->setTranslation('name', 'et', $request->name_et);

        if ($tag->save()) {

            //show notification:
            $message = 'Tag updated : ' .  $tag->name;
            session()->flash('notification', $message);

            return redirect()->route('tags.index', $tag->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('tags.index');
        }
    }

    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();

        //show notification:
        $message = 'Tag deleted : ' .  $tag->name;
        session()->flash('notification', $message);

        return redirect()->route('tags.index');
    }
}
