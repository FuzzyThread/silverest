<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subcategory;
use App\Material;
use App\Category;
use App\Product;
use App\Style;
use App\Photo;
use App\Color;
use App\User;
use App\Size;
use App\Tag;
use App\Gem;
use Session;
use DB;

class CustomerController extends Controller
{

    public function index()
    {
        $customers = new User();
        $queries = [];
        $search_field = '';

        //search by:
        if (request()->has('search')) {
            $search_field = request('search');
            $customers = $customers->search($search_field);
            $queries['search'] = $search_field;
        }

        //sort by:
        if (request()->has('sort')) {
            $customers = $customers->orderBy('id', request('sort'));
            $queries['sort'] = request('sort');
        } else {
            $customers = $customers->orderBy('id', 'asc');
        }

        $customers = $customers->with('address')->paginate(20)->appends($queries);
        return view('admin.customers.index', compact('customers', 'search_field'));

    }

    public function show($id)
    {

        $customer = User::findOrFail($id);
        return view('admin.customers.show', compact('customer'));

    }

    public function destroy($id)
    {
        $customer = User::findOrFail($id);
        $customer->delete();

        //show notification:
        $message = 'Customer deleted : ' .  $customer->email . ' ( ' . $customer->id . ' ) ';
        session()->flash('notification', $message);

        return redirect()->route('customers.index');
    }

}
