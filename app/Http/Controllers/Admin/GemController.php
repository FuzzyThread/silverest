<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Gem;
use Session;
use DB;

class GemController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function index()
    {
        $gems = Gem::orderBy('id','asc')->paginate(24);
        return view('admin.gems.index')->withGems($gems);
    }

    public function create()
    {
        return view('admin.gems.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'type' => 'required|max:255',
            'code' => 'required|max:255',
            'image'=>'image|mimes:jpg,jpeg,png|max:10000'
        ]);

        $gem = new Gem();

        if ($file = $request->file('image')) {

            //save file to images folder with name modified:
            $file_name = time() . $file->getClientOriginalName();
            $file->move('gems', $file_name);

            //save file path to DB:
            $gem->image = $file_name;
        }

        $gem->setTranslation('name', 'en', $request->name_en);
        $gem->setTranslation('name', 'ru', $request->name_ru);
        $gem->setTranslation('name', 'et', $request->name_et);
        $gem->type = $request->type;
        $gem->code = $request->code;

        if ($gem->save()) {

            //show notification:
            $message = 'Gem added : ' .  $gem->name;
            session()->flash('notification', $message);

            return redirect()->route('gems.index', $gem->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('gems.index');
        }
    }

    public function show($id)
    {
        $gem = Gem::where('id',$id)->with('products')->first();
        return view("admin.gems.show")->withGem($gem);
    }

    public function edit($id)
    {
        $gem = Gem::where('id',$id)->first();
        return view("admin.gems.edit")->withGem($gem);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'type' => 'required|max:255',
            'code' => 'required|max:255'
        ]);

        $gem = Gem::findOrFail($id);

        if ($file = $request->file('image')) {

            //save file to images folder with name modified:
            $file_name = time() . $file->getClientOriginalName();
            $file->move('gems', $file_name);

            //save file path to DB:
            $gem->image = $file_name;
        }

        $gem->setTranslation('name', 'en', $request->name_en);
        $gem->setTranslation('name', 'ru', $request->name_ru);
        $gem->setTranslation('name', 'et', $request->name_et);
        $gem->type = $request->type;
        $gem->code = $request->code;


        if ($gem->save()) {

            //show notification:
            $message = 'Gem updated : ' .  $gem->name;
            session()->flash('notification', $message);

            return redirect()->route('gems.index', $gem->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('gems.index');
        }

    }

    public function destroy($id)
    {
        $gem = Gem::findOrFail($id);

        //delete user photo:
        unlink(public_path() . $gem->image);

        $gem->delete();

        //show notification:
        $message = 'Gem deleted : ' .  $gem->name;
        session()->flash('notification', $message);

        return redirect()->route('gems.index');
    }
}
