<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Size;
use Session;
use DB;

class SizeController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function index()
    {
        $sizes = Size::orderBy('id','asc')->paginate(10);
        return view('admin.sizes.index')->withSizes($sizes);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'value' => 'required|numeric|min:1|max:50|unique:sizes,value',
        ]);

        $size = new Size();
        $size->value = $request->value;

        if ($size->save()) {

            //show notification:
            $message = 'Size added : ' .  $size->value;
            session()->flash('notification', $message);

            return redirect()->route('sizes.index', $size->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('sizes.index');
        }
    }

    public function show($id)
    {
        $size = Size::where('id',$id)->with('products')->first();
        return view("admin.sizes.show")->withSize($size);
    }

    public function edit($id)
    {
        $size = Size::where('id',$id)->first();
        return view("admin.sizes.edit")->withSize($size);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'value' => 'required|numeric|min:1|max:50|unique:sizes,value,'. $id,
        ]);

        $size = Size::findOrFail($id);
        $size->value = $request->value;

        if ($size->save()) {

            //show notification:
            $message = 'Size updated : ' .  $size->value;
            session()->flash('notification', $message);

            return redirect()->route('sizes.index', $size->id);

        } else {
            Session::flash('notification', 'Error');
            return redirect()->route('sizes.index');
        }
    }

    public function destroy($id)
    {
        $size = Size::findOrFail($id);
        $size->delete();

        //show notification:
        $message = 'Size deleted : ' .  $size->value;
        session()->flash('notification', $message);

        return redirect()->route('sizes.index');
    }
}
