<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Color;
use Session;
use DB;

class ColorController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function index()
    {
        $colors = Color::orderBy('id','asc')->paginate(10);
        return view('admin.colors.index')->withColors($colors);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'value' => 'required|max:255|unique:colors,value',
            'name' => 'required|max:255|unique:colors,name'
        ]);

        $color = new Color();
        $color->value = $request->value;
        $color->name = $request->name;

        if ($color->save()) {

            //show notification:
            $message = 'Color added : ' .  $color->name;
            session()->flash('notification', $message);

            return redirect()->route('colors.index');

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('colors.index');
        }
    }

    public function show($id)
    {
        $color = Color::where('id',$id)->with('products')->first();
        return view("admin.colors.show")->withColor($color);
    }

    public function edit($id)
    {
        $color = Color::where('id',$id)->first();
        return view("admin.colors.edit")->withColor($color);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'value' => 'required|max:255',
            'name' => 'required|max:255'
        ]);

        $color = Color::findOrFail($id);
        $color->value = $request->value;
        $color->name = $request->name;

        if ($color->save()) {

            //show notification:
            $message = 'Color updated : ' .  $color->name;
            session()->flash('notification', $message);

            return redirect()->route('colors.index');

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('colors.index');
        }
    }

    public function destroy($id)
    {
        $color = Color::findOrFail($id);
        $color->delete();

        //show notification:
        $message = 'Color deleted : ' .  $color->name;
        session()->flash('notification', $message);

        return redirect()->route('colors.index');
    }
}
