<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Category;
use App\Role;
use App\User;
use Session;
use DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function index()
    {
        $categories = Category::orderBy('id','asc')->paginate(10);
        return view('admin.categories.index')->withCategories($categories);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'code' => 'required|max:255'
        ]);

        $category = new Category();
        $category->setTranslation('name', 'en', $request->name_en);
        $category->setTranslation('name', 'ru', $request->name_ru);
        $category->setTranslation('name', 'et', $request->name_et);
        $category->code = $request->code;

        if ($category->save()) {

            //show notification:
            $message = 'Category added : ' .  $category->name;
            session()->flash('notification', $message);

            return redirect()->route('categories.index', $category->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('categories.index');
        }
    }

    public function show($id)
    {
        $category = Category::where('id',$id)->with('products')->first();
        return view("admin.categories.show")->withCategory($category);
    }

    public function edit($id)
    {
        $category = Category::where('id',$id)->first();
        return view("admin.categories.edit")->withCategory($category);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'code' => 'required|max:255'
        ]);

        $category = Category::findOrFail($id);
        $category->setTranslation('name', 'en', $request->name_en);
        $category->setTranslation('name', 'ru', $request->name_ru);
        $category->setTranslation('name', 'et', $request->name_et);
        $category->code = $request->code;

        if ($category->save()) {

            //show notification:
            $message = 'Category updated : ' .  $category->name;
            session()->flash('notification', $message);

            return redirect()->route('categories.index', $category->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('categories.index');
        }
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        //show notification:
        $message = 'Category deleted : ' .  $category->name;
        session()->flash('notification', $message);

        return redirect()->route('categories.index');
    }
}
