<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\LogGroup;
use App\Order;
use App\Review;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index()
    {
        $monthly_income = 0;
        $overall_income = 0;
        $income_by_months = array();

        //stats:
        $new_clients = User::all()->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-7 days')))->count();
        $new_reviews = Review::all()->where('is_visible', '0')->count();
        $all_orders = Order::all()->count();
        $all_users = User::all()->count();
        $online_users = User::whereNotNull('remember_token')->count();

        //memory:
        $memory_usage = $this->convert(memory_get_usage(true));
        $memUsage = $this->getServerMemoryUsage(false);
        $total_memory = $this->getNiceFileSize($memUsage["total"]);

        $montly_orders = Order::where('status_id', config('constants.STATUS.COMPLETED'))->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-30 days')))->get();
        foreach ($montly_orders as $order) {
            $monthly_income += $order->payment ? $order->payment->subtotal : 0;
        }

        $all_time_orders = Order::where('status_id', config('constants.STATUS.COMPLETED'))->get();
        foreach ($all_time_orders as $order) {
            $overall_income += $order->payment ? $order->payment->subtotal : 0;
        }

        //income by months stats:
        for ($i = 1; $i <= 12; $i++) {
            $temp_income = 0;
            $temp_orders = Order::whereMonth('created_at', $i)->whereYear('created_at', date('Y'))->where('status_id', config('constants.STATUS.COMPLETED'))->get();
            foreach ($temp_orders as $order) {
                $temp_income += $order->payment ? $order->payment->subtotal : 0;
            }
            $income_by_months[] = $temp_income;
        }
        //orders:
        $awaiting_payment = Order::where('status_id', config('constants.STATUS.AWAITING_PAYMENT'))->count();
        $preparation = Order::where('status_id', config('constants.STATUS.PREPARATION'))->count();
        $refunded = Order::where('status_id', config('constants.STATUS.REFUNDED'))->count();
        $dispatched = Order::where('status_id', config('constants.STATUS.DISPATCHED'))->count();
        $completed = Order::where('status_id', config('constants.STATUS.COMPLETED'))->count();
        $cancelled = Order::where('status_id', config('constants.STATUS.CANCELLED'))->count();
        $error = Order::where('status_id', config('constants.STATUS.ERROR'))->count();
        $payment_declined = Order::where('status_id', config('constants.STATUS.PAYMENT_DECLINED'))->count();

        return view('admin.main.dashboard', compact('new_clients', 'awaiting_payment', 'preparation',
            'refunded', 'dispatched', 'completed', 'cancelled', 'error', 'payment_declined', 'new_reviews',
            'all_orders', 'all_users', 'monthly_income', 'overall_income', 'income_by_months', 'total_memory',
            'memory_usage', 'online_users'));
    }

    public function info()
    {
        return view('admin.system.info');
    }

    public function logs()
    {
        $queries = [];

        // sort by:
        if (request()->has('sort')) {
            $log_groups = LogGroup::whereHas('logs')->orderBy('id', 'desc');
            $queries['with_logs'] = 'true';
        }else{
            $log_groups = LogGroup::orderBy('id', 'desc');
        }

        $log_groups = $log_groups->paginate(50)->appends($queries);
        return view('admin.system.logs', compact('log_groups'));
    }

    public function convert($size)
    {
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

    function getServerMemoryUsage($getPercentage = true)
    {
        $memoryTotal = null;
        $memoryFree = null;

        if (stristr(PHP_OS, "win")) {
            // Get total physical memory (this is in bytes)
            $cmd = "wmic ComputerSystem get TotalPhysicalMemory";
            @exec($cmd, $outputTotalPhysicalMemory);

            // Get free physical memory (this is in kibibytes!)
            $cmd = "wmic OS get FreePhysicalMemory";
            @exec($cmd, $outputFreePhysicalMemory);

            if ($outputTotalPhysicalMemory && $outputFreePhysicalMemory) {
                // Find total value
                foreach ($outputTotalPhysicalMemory as $line) {
                    if ($line && preg_match("/^[0-9]+\$/", $line)) {
                        $memoryTotal = $line;
                        break;
                    }
                }

                // Find free value
                foreach ($outputFreePhysicalMemory as $line) {
                    if ($line && preg_match("/^[0-9]+\$/", $line)) {
                        $memoryFree = $line;
                        $memoryFree *= 1024;  // convert from kibibytes to bytes
                        break;
                    }
                }
            }
        } else {
            if (is_readable("/proc/meminfo")) {
                $stats = @file_get_contents("/proc/meminfo");

                if ($stats !== false) {
                    // Separate lines
                    $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
                    $stats = explode("\n", $stats);

                    // Separate values and find correct lines for total and free mem
                    foreach ($stats as $statLine) {
                        $statLineData = explode(":", trim($statLine));

                        // Total memory
                        if (count($statLineData) == 2 && trim($statLineData[0]) == "MemTotal") {
                            $memoryTotal = trim($statLineData[1]);
                            $memoryTotal = explode(" ", $memoryTotal);
                            $memoryTotal = $memoryTotal[0];
                            $memoryTotal *= 1024;  // convert from kibibytes to bytes
                        }

                        // Free memory
                        if (count($statLineData) == 2 && trim($statLineData[0]) == "MemFree") {
                            $memoryFree = trim($statLineData[1]);
                            $memoryFree = explode(" ", $memoryFree);
                            $memoryFree = $memoryFree[0];
                            $memoryFree *= 1024;  // convert from kibibytes to bytes
                        }
                    }
                }
            }
        }

        if (is_null($memoryTotal) || is_null($memoryFree)) {
            return null;
        } else {
            if ($getPercentage) {
                return (100 - ($memoryFree * 100 / $memoryTotal));
            } else {
                return array(
                    "total" => $memoryTotal,
                    "free" => $memoryFree,
                );
            }
        }
    }

    function getNiceFileSize($bytes, $binaryPrefix = true)
    {
        if ($binaryPrefix) {
            $unit = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
            if ($bytes == 0) return '0 ' . $unit[0];
            return @round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), 2) . ' ' . (isset($unit[$i]) ? $unit[$i] : 'B');
        } else {
            $unit = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
            if ($bytes == 0) return '0 ' . $unit[0];
            return @round($bytes / pow(1000, ($i = floor(log($bytes, 1000)))), 2) . ' ' . (isset($unit[$i]) ? $unit[$i] : 'B');
        }
    }

}
