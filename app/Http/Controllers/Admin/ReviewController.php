<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subcategory;
use App\Material;
use App\Category;
use App\Product;
use App\Review;
use App\Style;
use App\Photo;
use App\Color;
use App\Size;
use App\Gem;
use App\Tag;
use Session;
use DB;

class ReviewController extends Controller
{

    public function index()
    {
        $reviews = new Review();
        $queries = [];
        $search_field = '';

        //search by:
        if (request()->has('search')) {
            $search_field = request('search');
            $reviews = $reviews->search($search_field);
            $queries['search'] = $search_field;
        }

        //sort by:
        if (request()->has('sort')) {
            $reviews = $reviews->orderBy('created_at', request('sort'));
            $queries['sort'] = request('sort');
        } else {
            $reviews = $reviews->orderBy('created_at', 'desc');
        }

        $reviews = $reviews->paginate(10)->appends($queries);
        return view('admin.reviews.index', compact('reviews', 'search_field'));

    }


    public function edit($id)
    {
        $review = Review::findOrFail($id);
        return view('admin.reviews.edit', compact('review'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'body' => 'required|max:500',
            'score' => 'required|numbers'
        ]);

        $review = Review::findOrFail($id);
        $review->is_visible = isset($request->is_visible) ? true : false;
        $review->body = $request->body;
        $review->score = $request->score;
        $review->save();

        //show notification:
        $message = 'Review updated : ' .  $review->id ;
        session()->flash('notification', $message);

        return redirect()->route('reviews.index');
    }

    public function destroy($id)
    {
        $review = Review::findOrFail($id);
        $review->delete();

        //show notification:
        $message = 'Review deleted : ' .  $review->id;
        session()->flash('notification', $message);

        return redirect()->route('reviews.index');
    }

    public function reviewVisibleChange(Request $request)
    {
        $review = Review::findOrFail($request->product_id);
        if ($request->is_visible) {
            $review->is_visible = ($request->is_visible === 'true');
        }
        if ($review->save()) {
            return response('Product ' . $review->id . ' Visible : ' . $request->is_visible, 200); //return success
        } else {
            return response('Error with saving product', 200); //return success
        }
    }


}
