<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Style;
use Session;
use DB;

class StyleController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function index()
    {
        $styles = Style::orderBy('id','asc')->paginate(10);
        return view('admin.styles.index')->withStyles($styles);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'code' => 'required|max:255'
        ]);

        $style = new Style();
        $style->setTranslation('name', 'en', $request->name_en);
        $style->setTranslation('name', 'ru', $request->name_ru);
        $style->setTranslation('name', 'et', $request->name_et);
        $style->code = $request->code;

        if ($style->save()) {

            //show notification:
            $message = 'Style added : ' .  $style->name;
            session()->flash('notification', $message);

            return redirect()->route('styles.index', $style->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('styles.index');
        }
    }

    public function show($id)
    {
        $style = Style::where('id',$id)->with('products')->first();
        return view("admin.styles.show")->withStyle($style);
    }

    public function edit($id)
    {
        $style = Style::where('id',$id)->first();
        return view("admin.styles.edit")->withStyle($style);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'code' => 'required|max:255'
        ]);

        $style = Style::findOrFail($id);
        $style->setTranslation('name', 'en', $request->name_en);
        $style->setTranslation('name', 'ru', $request->name_ru);
        $style->setTranslation('name', 'et', $request->name_et);
        $style->code = $request->code;

        if ($style->save()) {

            //show notification:
            $message = 'Style updated : ' .  $style->name;
            session()->flash('notification', $message);

            return redirect()->route('styles.index', $style->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('styles.index');
        }
    }

    public function destroy($id)
    {
        $style = Style::findOrFail($id);
        $style->delete();

        //show notification:
        $message = 'Style deleted : ' .  $style->name;
        session()->flash('notification', $message);

        return redirect()->route('styles.index');
    }
}
