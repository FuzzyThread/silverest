<?php

namespace App;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;


class Gem extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    protected $fillable = [
        'name','type','image'
    ];

    public $prefix = '/gems/';

    public function getImageAttribute($value)
    {
        return $this->prefix . $value;
    }

    public function products(){
        return $this->belongsToMany('App\Product');
    }
}
