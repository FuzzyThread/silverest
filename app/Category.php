<?php

namespace App;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasTranslations;

    protected $fillable = [
        'name'
    ];

    public $translatable = ['name'];

    public function products() {
        return $this->belongsToMany('App\Product');
    }

    public function subcategories() {
        return $this->hasMany('App\Subcategory');
    }

}
