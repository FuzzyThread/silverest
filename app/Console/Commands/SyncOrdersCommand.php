<?php

namespace App\Console\Commands;

use App\LogGroup;
use App\Order;
use App\Log;
use Illuminate\Console\Command;

class SyncOrdersCommand extends Command
{
    protected $signature = 'sync:orders';
    protected $description = 'Sync orders based on status';
    private $orders = null;

    public function __construct()
    {
        parent::__construct();
        $this->orders = Order::whereHas('status', function ($query) {
            $query->where('id', config('constants.STATUS.AWAITING_PAYMENT'))
                ->orWhere('id', config('constants.STATUS.CANCELLED'));
        })->whereHas('payment', function ($query) {
            $query->where('method', '!=', 'quote');
        })->get();
    }

    public function handle()
    {
        if ($this->orders) {

            // TODO: refactor payments

//            $log_group = LogGroup::create(['name' => 'CRON - ORDER'])->id;
//
//            foreach ($this->orders as $order) {
//
//                if ($order->payment && $order->payment->braintree_id) {
//
//                    switch ($order->status_id) {
//
//                        case config('constants.STATUS.AWAITING_PAYMENT'):
//
//                            // payment type (in)
//                            if ($order->payment->type == 'in') {
//
//                                // payment settling:
//                                if (!$order->payment->is_settled) {
//
//                                    try {
//                                        // call braintree:
//                                        $braintree_transaction = Braintree_Transaction::find($order->payment->braintree_id);
//
//                                        if ($braintree_transaction && ($braintree_transaction->status == 'settled')) {
//
//                                            // update order:
//                                            $order->status_id = config('constants.STATUS.PREPARATION');
//                                            $order->save();
//
//                                            // update payment:
//                                            $payment = $order->payment;
//                                            $payment->is_settled = true;
//                                            $payment->save();
//
//                                            Log::create(['key' => 'order #' . $order->id, 'type' => 'success', 'description' => 'AWAITING PAYMENT: Payment Settled, Order Status Changed To PREPARATION', 'log_group_id' => $log_group]);
//                                        } else {
//                                            Log::create(['key' => 'order #' . $order->id, 'type' => 'info', 'description' => 'AWAITING PAYMENT: Payment Settling', 'log_group_id' => $log_group]);
//                                        }
//                                    } catch (\Exception $ignore) {
//                                        Log::create(['key' => 'order #' . $order->id, 'type' => 'error', 'description' => 'FATAL ERROR: AWAITING PAYMENT', 'log_group_id' => $log_group]);
//                                    }
//                                }
//                            }
//                            break;
//
//                        case config('constants.STATUS.CANCELLED'):
//
//                            // payment type (in)
//                            if ($order->payment->type == 'in') {
//
//                                // payment settling:
//                                if (!$order->payment->is_settled) {
//
//                                    try {
//                                        // call braintree:
//                                        $braintree_transaction = Braintree_Transaction::find($order->payment->braintree_id);
//
//                                        if ($braintree_transaction && ($braintree_transaction->status == 'settled')) {
//
//                                            // update payment:
//                                            $payment = $order->payment;
//                                            $payment->is_settled = true;
//                                            $payment->save();
//
//                                            Log::create(['key' => 'order #' . $order->id, 'type' => 'success', 'description' => 'CANCELLATION: Payment Settled', 'log_group_id' => $log_group]);
//                                        } else {
//                                            Log::create(['key' => 'order #' . $order->id, 'type' => 'info', 'description' => 'CANCELLATION: Payment Settling', 'log_group_id' => $log_group]);
//                                        }
//
//                                    } catch (\Exception $ignore) {
//                                        Log::create(['key' => 'order #' . $order->id, 'type' => 'error', 'description' => 'FATAL ERROR: CANCELLATION', 'log_group_id' => $log_group]);
//                                    }
//                                } else {
//
//                                    try {
//                                        // refund:
//                                        $result = Braintree_Transaction::refund($order->payment->braintree_id);
//                                        if ($result->success) {
//
//                                            // update payment (out):
//                                            $payment = $order->payment;
//                                            $payment->type = 'out';
//                                            $payment->is_settled = false;
//                                            $payment->braintree_id = $result->transaction->id;
//                                            $payment->save();
//
//                                            Log::create(['key' => 'order #' . $order->id, 'type' => 'success', 'description' => 'CANCELLATION: Created New Refund', 'log_group_id' => $log_group]);
//                                        } else {
//                                            Log::create(['key' => 'order #' . $order->id, 'type' => 'warning', 'description' => 'CANCELLATION: Refund Already Created', 'log_group_id' => $log_group]);
//                                        }
//                                    } catch (\Exception $ignore) {
//                                        Log::create(['key' => 'order #' . $order->id, 'type' => 'error', 'description' => 'FATAL ERROR: CANCELLATION (CREATING REFUND)', 'log_group_id' => $log_group]);
//                                    }
//                                }
//                            } else {
//
//                                // payment settling:
//                                if (!$order->payment->is_settled) {
//
//                                    try {
//                                        // call braintree:
//                                        $braintree_transaction = Braintree_Transaction::find($order->payment->braintree_id);
//
//                                        if ($braintree_transaction && ($braintree_transaction->status == 'settled')) {
//
//                                            // update order:
//                                            $order->status_id = config('constants.STATUS.REFUNDED');
//                                            $order->save();
//
//                                            // update payment:
//                                            $payment = $order->payment;
//                                            $payment->is_settled = true;
//                                            $payment->save();
//
//                                            Log::create(['key' => 'order #' . $order->id, 'type' => 'success', 'description' => 'REFUND: Payment Settled, Order Status Changed To REFUNDED', 'log_group_id' => $log_group]);
//                                        } else {
//                                            Log::create(['key' => 'order #' . $order->id, 'type' => 'info', 'description' => 'REFUND: Payment Settling', 'log_group_id' => $log_group]);
//                                        }
//
//                                    } catch (\Exception $ignore) {
//                                        Log::create(['key' => 'order #' . $order->id, 'type' => 'error', 'description' => 'FATAL ERROR: REFUND', 'log_group_id' => $log_group]);
//                                    }
//                                } else {
//                                    Log::create(['key' => 'order #' . $order->id, 'type' => 'warning', 'description' => 'REFUND: Payment Already Settled', 'log_group_id' => $log_group]);
//                                }
//                            }
//                            break;
//                    }
//                } else {
//                    Log::create(['key' => 'order #' . $order->id, 'type' => 'error', 'description' => 'FATAL ERROR: INVALID PAYMENT', 'log_group_id' => $log_group]);
//                }
//            }
        }
    }
}
