<?php

namespace App\Console\Commands;

use App\LogGroup;
use App\Log;
use Illuminate\Console\Command;

class RemoveLogsCommand extends Command
{
    protected $signature = 'remove:logs';
    protected $description = 'Remove logs if more than 10000';
    private $logs = null;

    public function __construct()
    {
        parent::__construct();
        $this->logs = LogGroup::all();
    }

    public function handle()
    {
        if ($this->logs) {

            $log_group = LogGroup::create(['name' => 'CRON - LOGS'])->id;

            try {
                $count = LogGroup::count();

                LogGroup::latest()->take($count)->skip(10000)->get()->each(function ($row) {
                    $row->delete();
                });

                Log::create(['key' => 'logs deleted', 'type' => 'success', 'description' => 'LOGS DELETED REACHED 10000 LOGS', 'log_group_id' => $log_group]);
            } catch (\Exception $ignore) {
                Log::create(['key' => 'logs deleted', 'type' => 'error', 'description' => 'FATAL ERROR: DELETION FAILED', 'log_group_id' => $log_group]);
            }
        }
    }
}
