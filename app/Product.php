<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    use HasTranslations;
    public $translatable = ['name'];

    use SoftDeletes;
    protected $dates = ['deleted_at', 'discount_end_date'];
    protected $discount_price = null;

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function subcategories()
    {
        return $this->belongsToMany('App\Subcategory');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function sets()
    {
        return $this->belongsToMany('App\Set');
    }

    public function photos()
    {
        return $this->belongsToMany('App\Photo');
    }

    public function colors()
    {
        return $this->belongsToMany('App\Color');
    }

    public function styles()
    {
        return $this->belongsToMany('App\Style');
    }

    public function gems()
    {
        return $this->belongsToMany('App\Gem');
    }

    public function sizes()
    {
        return $this->belongsToMany('App\Size');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function material()
    {
        return $this->belongsTo('App\Material');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function scopeSearch($query, $str)
    {
        return $query->where('name', 'like', '%' . $str . '%')->orWhere('code', 'like', '%' . $str . '%');
    }

    public function carts()
    {
        return $this->belongsToMany('App\Cart');
    }


    public function getDiscountPrice()
    {

        if ($this->discount_end_date && $this->discount_rate > 0 && (date('Y-m-d') <= $this->discount_end_date)) {
            $this->discount_price = round(($this->price - ($this->discount_rate * $this->price / 100)));
        }

        return $this->discount_price;
    }

    public function getRating()
    {
        if ($this->reviews->count() > 0) {
            $rating = 0;
            foreach ($this->reviews->where('is_visible','1') as $review) {
                $rating += $review->score;
            }
            return ($rating / $this->reviews->count());
        } else {
            return 0;
        }
    }

    public function setIsVisibleAttribute($value)
    {
        if ($this->quantity < 1) {
            $this->attributes['is_visible'] = 0;
        } else {
            $this->attributes['is_visible'] = $value;
        }
    }


}
