@extends('layouts.front')

@section('styles')

    <style>
        #cancel_search:hover {
            color: #1a237e;
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .plr-200 {
            padding-left: 150px;
            padding-right: 150px;
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }
    </style>

@endsection

@section('content')

    <div id="page-content" class="page-wrapper">

        <!-- LOGIN SECTION START -->
        <div class="login-section mb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="registered-customers">
                            <h6 class="widget-title border-left mb-50">{{trans('front/auth.reset_the_password')}}</h6>
                            <form method="POST" action="{{ route('password.request') }}">
                                {{ csrf_field() }}
                                <div class="login-account p-30 box-shadow">
                                    <p style="font-size: 16px">{{trans('front/auth.please_new_password')}}.</p>
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="text" name="email" placeholder="{{trans('front/auth.email')}}" value="{{ $email or old('email') }}" required autofocus placeholder="{{trans('front/auth.email_here')}}...">
                                        @if ($errors->has('email'))
                                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input id="password" type="password" name="password" required placeholder="{{trans('front/auth.password')}}">
                                        @if ($errors->has('password'))
                                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <input id="password-confirm" type="password" name="password_confirmation" required placeholder="{{trans('front/auth.confirm_password')}}">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button class="submit-btn-1 mt-20 btn-hover-1" type="submit">{{trans('front/auth.reset_password')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- LOGIN SECTION END -->
    </div>

@endsection

