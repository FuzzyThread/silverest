<div class="row">
    @if(!$photos->isEmpty())
        @foreach($photos as $photo)
            <div class="col-not-for-mac col-lg-3 col-md-4 col-sm-4">
                <div class="card">
                    <div class="card-header" style="height: auto">
                        <h2 style="font-size: 18px">{{str_limit($photo->name,20)}}</h2>
                        <p style="margin-top: 10px;margin-bottom: 0px;">{{$photo->updated_at ? $photo->updated_at->diffForHumans():'No Date'}}</p>
                    </div>
                    <div class="card-block" style="text-align: center">
                        <a href="{{$photo->getPhotoPath()}}" data-lightbox="gallery">
                            <img src="{{$photo->getPhotoPath()}}" style="width: 100%; border-radius: 0px;height: auto" alt="" class="loading img-thumbnail">
                        </a>
                        <div class="line" style="margin: 19px 0 18px"></div>
                        <div class="row">
                            <div class="col-xs-4" style="margin: 0">
                                <a href="{{route('photos.edit',$photo->id)}}" style="margin-left: 20px;margin-right: 20px; color: #515354;"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-xs-4" style="margin: 0">
                                {!! Form::open(['method'=>'DELETE','route'=>['photos.destroy', $photo->id],'class'=>'delete_photo']) !!}
                                <button type="submit" style="border: none;background: transparent;padding: 0;outline: none;color: red;margin-right: 20px">
                                    <i class="fa fa-trash-o fa-lg" aria-hidden="true" style="color: red"></i>
                                </button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header" style="height: 100px">
                    <h2 style="font-size: 24px;text-align: center;margin: 25px 0">There are no photos yet.</h2>
                </div>
            </div>
        </div>
    @endif
</div>
<div style="margin-bottom: 20px">
    {{$photos->links()}}
</div>