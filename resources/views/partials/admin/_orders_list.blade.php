<div class="row">
    @foreach($orders as $order)
        <div class="col-lg-12" style="margin-bottom: 10px">
            <div class="card">
                <div class="card-header" style="padding: 0px;height: 44px;">
                    <div class="row">
                        <div class="col-md-1" style="margin: auto 0;">
                            <p id="order_viewed{{$order->id}}" style="margin-left: 20px;margin-bottom: 0px;font-size: 16px;{{$order->is_viewed?'':'color:#043180;'}}">
                                {{$order->id}}
                            </p>
                            <span class="status" style="background-color:{{$order->status ? $order->status->value : 'white' }} "></span>
                        </div>
                        <div class="col-md-5" style="margin: auto 0;">
                            <div class="row" style="padding: 0 auto;">
                                <p class="col-sm-7" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                    {{$order->delivery ? $order->delivery->customer_email : 'Error'}}
                                </p>
                                <p class="col-sm-5" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                    {{trans('admin/orders.delivery')}}:
                                    <span style="font-size: 16px;">{{ucfirst($order->delivery ? $order->delivery->method: 'Error')}}</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin: auto 0;">
                            <div class="row" style="padding: 0 auto;">
                                <p class="col-sm-5" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                    {{trans('admin/orders.total')}}:
                                    <span style="font-size: 16px;">{{$order->payment ? $order->payment->subtotal . '&euro;' : 'Error'}}</span>
                                </p>

                                <p class="col-sm-7" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                    {{trans('admin/orders.placed')}}:
                                    <span style="font-size: 16px">{{($order->created_at  ? $order->created_at->diffForHumans():'No Date')}} </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-2" style="margin: auto 0;">
                            <div style="float: right;margin-right: 35px;margin-top: 0px">
                                <div class="row" style=" height: 24px;">
                                    <div class="col-xs-4" style="margin: 0">
                                        @if($order->payment && strcmp($order->payment->method, "quote") == 0)

                                            {{-- Show statuses --}}
                                            @if($order->status &&
                                                ($order->status->id == config('constants.STATUS.AWAITING_PAYMENT')
                                                ||$order->status->id == config('constants.STATUS.DISPATCHED')
                                                || $order->status->id == config('constants.STATUS.PREPARATION')
                                                || ($order->status->id == config('constants.STATUS.CANCELLED') && $order->payment->is_settled)))

                                                <a class="dropdown-toggle link-order-status" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-square" style="color: {{$order->status->value}}"></i>
                                                </a>
                                                <ul class="dropdown-menu custom-dropdown animated flipInX">
                                                    <li class="dropdown-header" style="font-size: 17px">Order Statuses</li>
                                                    <div class="dropdown-divider"></div>

                                                    {{-- Show available choices for each status --}}
                                                    @foreach($statuses as $status)

                                                        {{-- Hide same order status--}}
                                                        @if($status->id != $order->status->id)

                                                            {{-- Awaiting payment -> (Cancelled, Preparation, Payment Declined) --}}
                                                            @if($order->status->id == config('constants.STATUS.AWAITING_PAYMENT')
                                                                && ($status->id == config('constants.STATUS.CANCELLED')
                                                                || $status->id == config('constants.STATUS.PREPARATION')
                                                                || $status->id == config('constants.STATUS.PAYMENT_DECLINED')))

                                                                <li class="dropdown-item order_status" data-order-id="{{$order->id}}" data-status-id="{{$status->id}}">
                                                                    <button style="border: none;background: transparent;padding: 0;outline: none;margin-right: 20px">
                                                                        <i class="fa fa-square" style="color: {{$status->value}}"></i> &nbsp;&nbsp;{{$status->name}}
                                                                    </button>
                                                                </li>
                                                            @endif

                                                            {{-- Preparation -> (Cancelled, Dispatched) --}}
                                                            @if($order->status->id == config('constants.STATUS.PREPARATION')
                                                                && ($status->id == config('constants.STATUS.CANCELLED')
                                                                || $status->id == config('constants.STATUS.DISPATCHED')))

                                                                <li class="dropdown-item order_status" data-order-id="{{$order->id}}" data-status-id="{{$status->id}}">
                                                                    <button style="border: none;background: transparent;padding: 0;outline: none;margin-right: 20px">
                                                                        <i class="fa fa-square" style="color: {{$status->value}}"></i> &nbsp;&nbsp;{{$status->name}}
                                                                    </button>
                                                                </li>
                                                            @endif

                                                            {{-- Dispatched -> (Completed) --}}
                                                            @if($order->status->id == config('constants.STATUS.DISPATCHED')
                                                                && $status->id == config('constants.STATUS.COMPLETED'))

                                                                <li class="dropdown-item order_status" data-order-id="{{$order->id}}" data-status-id="{{$status->id}}">
                                                                    <button style="border: none;background: transparent;padding: 0;outline: none;margin-right: 20px">
                                                                        <i class="fa fa-square" style="color: {{$status->value}}"></i> &nbsp;&nbsp;{{$status->name}}
                                                                    </button>
                                                                </li>
                                                            @endif

                                                            {{-- Cancelled -> (Refunded) --}}
                                                            @if($order->status->id == config('constants.STATUS.CANCELLED')
                                                                && $status->id == config('constants.STATUS.REFUNDED'))

                                                                <li class="dropdown-item order_status" data-order-id="{{$order->id}}" data-status-id="{{$status->id}}">
                                                                    <button style="border: none;background: transparent;padding: 0;outline: none;margin-right: 20px">
                                                                        <i class="fa fa-square" style="color: {{$status->value}}"></i> &nbsp;&nbsp;{{$status->name}}
                                                                    </button>
                                                                </li>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @else
                                                <a class="dropdown-toggle link-order-status">
                                                    <i class="fa fa-square" style="color: white"></i>
                                                </a>
                                            @endif
                                        @else
                                            {{-- Show statuses --}}
                                            @if($order->status &&
                                                ($order->status->id == config('constants.STATUS.AWAITING_PAYMENT')
                                                || $order->status->id == config('constants.STATUS.DISPATCHED')
                                                || $order->status->id == config('constants.STATUS.PREPARATION')))

                                                <a class="dropdown-toggle link-order-status" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-square" style="color: {{$order->status->value}}"></i>
                                                </a>
                                                <ul class="dropdown-menu custom-dropdown animated flipInX">
                                                    <li class="dropdown-header" style="font-size: 17px">Order Statuses</li>
                                                    <div class="dropdown-divider"></div>

                                                    {{-- Show available choices for each status --}}
                                                    @foreach($statuses as $status)

                                                        {{-- Hide same order status--}}
                                                        @if($status->id != $order->status->id)

                                                            {{-- Awaiting payment -> (Cancelled) ; (Preparation, Payment Declined -> AUTO) --}}
                                                            @if($order->status->id == config('constants.STATUS.AWAITING_PAYMENT')
                                                                && $status->id == config('constants.STATUS.CANCELLED'))

                                                                <li class="dropdown-item order_status" data-order-id="{{$order->id}}" data-status-id="{{$status->id}}">
                                                                    <button style="border: none;background: transparent;padding: 0;outline: none;margin-right: 20px">
                                                                        <i class="fa fa-square" style="color: {{$status->value}}"></i> &nbsp;&nbsp;{{$status->name}}
                                                                    </button>
                                                                </li>
                                                            @endif

                                                            {{-- Preparation -> (Cancelled, Dispatched) --}}
                                                            @if($order->status->id == config('constants.STATUS.PREPARATION')
                                                                && ($status->id == config('constants.STATUS.CANCELLED')
                                                                || $status->id == config('constants.STATUS.DISPATCHED')))

                                                                <li class="dropdown-item order_status" data-order-id="{{$order->id}}" data-status-id="{{$status->id}}">
                                                                    <button style="border: none;background: transparent;padding: 0;outline: none;margin-right: 20px">
                                                                        <i class="fa fa-square" style="color: {{$status->value}}"></i> &nbsp;&nbsp;{{$status->name}}
                                                                    </button>
                                                                </li>
                                                            @endif

                                                            {{-- Dispatched -> (Completed) --}}
                                                            @if($order->status->id == config('constants.STATUS.DISPATCHED')
                                                                && $status->id == config('constants.STATUS.COMPLETED'))

                                                                <li class="dropdown-item order_status" data-order-id="{{$order->id}}" data-status-id="{{$status->id}}">
                                                                    <button style="border: none;background: transparent;padding: 0;outline: none;margin-right: 20px">
                                                                        <i class="fa fa-square" style="color: {{$status->value}}"></i> &nbsp;&nbsp;{{$status->name}}
                                                                    </button>
                                                                </li>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @else
                                                <a class="dropdown-toggle link-order-status">
                                                    <i class="fa fa-square" style="color: white"></i>
                                                </a>
                                            @endif
                                        @endif
                                    </div>
                                    <div class="col-xs-4" style="margin: 0">
                                        <a href="{{route('orders.edit',$order->id)}}" style="margin-right: 20px;margin-left: 22px;color: #0275d8;cursor: pointer"><i class="fa fa-pencil  fa-lg" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col-xs-4" style="margin: 0">
                                        <a class="{{$order->is_viewed ? '':'open-order'}}" value="{{$order->id}}" style="color: #515354;" data-toggle="collapse" href="#product-{{$order->id}}-details" aria-expanded="true" aria-controls="test-block"><i class="fa fa-expand fa-lg" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="product-{{$order->id}}-details" class="collapse">
                    <div class="card-block" style="border-bottom: 1px solid #f2f2f2; padding-top: 0px;padding-bottom: 0px">
                        <div class="row">

                            <div class="col-md-6" style="border-right: 1px solid #dfdfdf;padding:20px;margin-bottom: 0px">
                                <div class="billing-details pr-10">
                                    <h5 class="widget-title mb-20 mt-10" style="color: #1a237e;margin-bottom: 15px">
                                        <i class="fa fa-truck" aria-hidden="true"></i>&nbsp; {{trans('admin/orders.delivery')}}:
                                    </h5>
                                </div>
                                <table class="payment-details">
                                    @if($order->delivery)
                                        <tr>
                                            <td class="td-title-1">
                                                {{($order->delivery->method == 'omniva') ? trans('admin/orders.shipping_notes') : trans('admin/orders.address') }}
                                            </td>
                                            <td class="td-title-2">
                                                {{$order->delivery->customer_address}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-title-1">{{trans('admin/orders.name')}}</td>
                                            <td class="td-title-2">
                                                {{$order->delivery->customer_name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-title-1">{{trans('admin/orders.phone')}}</td>
                                            <td class="td-title-2">{{$order->delivery->customer_phone}}</td>
                                        </tr>
                                        <tr style="border: none">
                                            <td class="td-title-1">{{trans('admin/orders.email')}}</td>
                                            <td class="td-title-2">{{$order->delivery->customer_email}}</td>
                                        </tr>
                                        <tr style="border: none">
                                            <td class="td-title-1">Tracking {{trans('admin/orders.number')}}</td>
                                            <td class="td-title-2">
                                                {!! Form::open(['method'=>'POST','route'=>'order.tracking.number.update']) !!}
                                                <div class="input-group tracking_number">
                                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                                    <input type="text" class="form-control" name="tracking_number" placeholder="Tracking {{trans('admin/orders.number')}}" value="{{$order->delivery?$order->delivery->tracking_number:''}}">
                                                    <span class="input-group-btn">
                                                        <button type="submit" class="btn btn-primary" style="    padding: 10px;">
                                                            <i class="fa fa-check " aria-hidden="true"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                {!! Form::close() !!}

                                            </td>
                                        </tr>
                                    @else
                                        @php
                                            $order->status_id = config('constants.STATUS.ERROR');
                                            $order->save();
                                        @endphp
                                        <tr style="border: none">
                                            <td class="td-title-1">{{trans('admin/orders.delivery')}}</td>
                                            <td class="td-title-2" style="color: red">Error</td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                            <div class="col-md-6" style="padding:20px;margin-bottom: 0px">
                                <div class="billing-details pr-10">
                                    <h5 class="widget-title mb-20 mt-10" style="color: #1a237e;margin-bottom: 15px">
                                        <i class="fa fa-info" aria-hidden="true"></i>&nbsp; {{trans('admin/orders.details')}}:
                                    </h5>
                                </div>
                                <table class="payment-details">
                                    @if($order->delivery)
                                        @if($order->status)
                                            <tr>
                                                <td class="td-title-1">{{trans('admin/orders.status')}}</td>
                                                <td class="td-title-2">{{ucwords($order->status->name)}}</td>
                                            </tr>
                                        @endif
                                        @if($order->payment)
                                            <tr>
                                                <td class="td-title-1">{{trans('admin/orders.method')}}</td>
                                                <td class="td-title-2">{{ucwords($order->payment->method)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="td-title-1">{{trans('admin/orders.cart_total')}}</td>
                                                <td class="td-title-2">{{number_format(($order->payment->cart_total),2)}} &euro;</td>
                                            </tr>
                                            <tr>
                                                <td class="td-title-1">{{trans('admin/orders.shipping_total')}}</td>
                                                <td class="td-title-2">{{number_format(($order->payment->shipping_total),2)}} &euro;</td>
                                            </tr>
                                            <tr>
                                                <td class="td-title-1">{{trans('admin/orders.order_subtotal')}}</td>
                                                <td class="td-title-2">{{number_format(($order->payment->subtotal),2)}} &euro;</td>
                                            </tr>
                                            <tr style="border: none">
                                                <td class="td-title-1">{{trans('admin/orders.date_ordered')}}</td>
                                                <td class="td-title-2">{{date('j F Y', strtotime($order->created_at))}}</td>
                                            </tr>
                                        @endif
                                    @else
                                        @php
                                            $order->status_id = config('constants.STATUS.ERROR');
                                            $order->save();
                                        @endphp
                                        <tr style="border: none">
                                            <td class="td-title-1">{{trans('admin/orders.payment')}}</td>
                                            <td class="td-title-2" style="color: red">Error</td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>

                        @if($order->notes)
                            <div class="row" style="border-top: 1px solid #dfdfdf;padding-bottom: 0px;padding-top: 20px">
                                <div class="col-md-12" style="margin-bottom: 0px">
                                    <h5 class="widget-title mt-10" style="color: #1a237e;margin-bottom: 20px;line-height: 1.6;">
                                        <i class="fa fa-comments" aria-hidden="true"></i>&nbsp; {{trans('admin/orders.notes')}}: &nbsp;
                                        <span style="font-size: 14px">{{$order->notes}}</span>
                                    </h5>
                                </div>
                            </div>
                        @endif

                        <div class="row" style="border-top: 1px solid #dfdfdf;padding-bottom: 0px;padding-top: 20px">

                            @if($order->items()->count() > 0)
                                @foreach($order->items as $item)
                                    @php
                                        $product = \App\Product::withTrashed()->find($item->product_id);
                                    @endphp
                                    <div class="product-list" style="margin-bottom: 20px;">
                                        @if($product)
                                            <div class="product-thumbnail cart_thumbnail">
                                                <div class="pro-thumbnail-img">
                                                    <img src="{{$product->photos()->count() > 0 ?  $product->photos->first()->getPreviewPath() : ''}}" alt="" class="cart_product_img">
                                                </div>
                                                <div class="pro-thumbnail-info text-left">
                                                    <h6 class="product-title-2">
                                                        <a>{{$product->name}} ( {{$product->code}} )</a>
                                                    </h6>
                                                    @if($product->gems()->count() > 0)
                                                        <p class="small-title">
                                                            <span>{{trans('admin/orders.gem')}}
                                                                <strong>: &nbsp;</strong></span>
                                                            @foreach($product->gems()->limit(2)->get() as $gem)
                                                                @if($loop->last)
                                                                    {{str_limit($gem->name,7)}}
                                                                @else
                                                                    {{str_limit($gem->name,7)}},
                                                                @endif
                                                            @endforeach
                                                        </p>
                                                    @endif
                                                    @if($item->size)
                                                        <p class="small-title">
                                                            <span>{{trans('admin/orders.size')}}
                                                                <strong>: &nbsp;</strong></span>{{$item->size}}
                                                        </p>
                                                    @endif
                                                    <p class="small-title">
                                                        <span>{{trans('admin/orders.q_ty')}}
                                                            <strong>: &nbsp;</strong></span>{{$item->quantity}}
                                                    </p>
                                                </div>
                                            </div>
                                        @else
                                            <div class="product-thumbnail cart_thumbnail">
                                                <div class="pro-thumbnail-img">
                                                </div>
                                                <div class="pro-thumbnail-info text-left">
                                                    <h6 class="product-title-2">
                                                        Error
                                                    </h6>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach

                            @else
                                @php
                                    $order->status_id = config('constants.STATUS.ERROR');
                                    $order->save();
                                @endphp
                                <div class="col-md-12 payment-method">
                                    <!-- our order -->
                                    <div class="payment-details">
                                        <h4 style="color: #1a237e;margin-bottom: 0px"> {{trans('admin/orders.no_products')}} (ERROR)</h4>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
<div style="margin-top: 20px">
    {{$orders->links()}}
</div>
