@extends('layouts.front')

@section('title',"Silverest | Wishlist")

@section('styles')

    <!-- Custom Styling -->
    <style>
        .cart_product_img {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.15);
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .qtybutton {
            cursor: default;
        }

        a.previous, a.next {
            text-decoration: none;
            display: inline-block;
            padding: 8px 16px;
        }

        a.previous:hover, a.next:hover {
            background-color: #ddd;
            color: black;
        }

        .previous {
            background-color: #f1f1f1;
            color: black;
        }

        .next {
            background-color: #1a237e;
            color: white;
        }

        .old-price {
            color: #a6a6a6;
            font-size: 12px;
            text-decoration: line-through;
        }

        .new-price {
            color: #666;
        }
    </style>

@endsection

@section('content')

    <!-- Start page content -->
    <section id="page-content" class="page-wrapper">

        <!-- SHOP SECTION START -->
        <div class="shop-section mb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <ul class="cart-tab">
                            <li>
                                <a class="active" href="{{route('account.cart')}}">
                                    <span>01</span>
                                    {{trans('front/wishlist.shopping_cart')}}
                                </a>
                            </li>
                            <li>
                                <a class="active" href="{{route('account.wishlist')}}">
                                    <span>02</span>
                                    {{trans('front/wishlist.wishlist')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{route('account.order.checkout')}}">
                                    <span>03</span>
                                    {{trans('front/wishlist.checkout')}}
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span>04</span>
                                    {{trans('front/wishlist.order_complete')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    @if($wishlist_products->count() > 0)
                        <div class="col-md-10">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- wishlist start -->
                                <div class="tab-pane active" id="wishlist">
                                    <div class="wishlist-content">
                                        <div class="table-content table-responsive mb-50">
                                            <table class="text-center">
                                                <thead>
                                                <tr>
                                                    <th class="product-thumbnail">{{trans('front/wishlist.product')}}</th>
                                                    <th class="product-price">{{trans('front/wishlist.price')}}</th>
                                                    <th class="product-stock">{{trans('front/wishlist.stock_status')}}</th>
                                                    <th class="product-add-cart">{{trans('front/wishlist.add_cart')}}</th>
                                                    <th class="product-remove">{{trans('front/wishlist.remove')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($wishlist_products as $wishlist_product)
                                                    <!-- tr -->
                                                    <tr>
                                                        <td class="product-thumbnail cart_thumbnail">
                                                            <div class="pro-thumbnail-img">
                                                                <img src="{{$wishlist_product->photos()->count() > 0 ?  $wishlist_product->photos->first()->getPreviewPath() : ''}}" alt="" class="cart_product_img">
                                                            </div>
                                                            <div class="pro-thumbnail-info text-left">
                                                                <h6 class="product-title-2">
                                                                    <a href="{{route('catalogue.product',$wishlist_product->id)}}">{{$wishlist_product->name}} ( {{$wishlist_product->code}} )</a>
                                                                </h6>
                                                                @if($wishlist_product->gems->count() > 0 )
                                                                    <p>{{trans('front/wishlist.gem')}} :
                                                                        @foreach($wishlist_product->gems as $gem)
                                                                            @if($loop->last)
                                                                                {{$gem->name}}
                                                                            @else
                                                                                {{$gem->name}},
                                                                            @endif
                                                                        @endforeach
                                                                    </p>
                                                                @endif
                                                                @if($wishlist_product->sizes->count() > 0 )
                                                                    <p>{{trans('front/wishlist.size')}} :
                                                                        @foreach($wishlist_product->sizes as $size)
                                                                            @if($loop->last)
                                                                                {{$size->value}}
                                                                            @else
                                                                                {{$size->value}},
                                                                            @endif
                                                                        @endforeach
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td class="product-price">
                                                            @if($wishlist_product->getDiscountPrice())
                                                                <span class="new-price">&euro; {{number_format ($wishlist_product->getDiscountPrice(),2)}}</span><br>
                                                                <span class="old-price">&euro; {{number_format ($wishlist_product->price,2)}}</span>
                                                            @else
                                                                <span class="new-price">&euro; {{number_format ($wishlist_product->price,2)}}</span>
                                                            @endif
                                                        </td>
                                                        <td class="product-stock text-uppercase">{{$wishlist_product->quantity > 0 ? 'in stock':'out of stock' }}</td>
                                                        <td class="product-add-cart">
                                                            @if($wishlist_product->quantity > 0)
                                                                <a class="cart_product tippy" data-toggle="modal" data-target="#productModal" title="Add To Cart" data-product="{{$wishlist_product->id}}">
                                                                    <i class="zmdi zmdi-shopping-cart-plus"></i>
                                                                </a>
                                                            @endif
                                                        </td>
                                                        <td class="product-remove">
                                                            {!! Form::open(['method'=>'POST','route'=>['account.wishlist.change']]) !!}
                                                            <input type="hidden" name="product_id" value="{{$wishlist_product->id}}">
                                                            <button type="submit">
                                                                <i class="zmdi zmdi-close"></i>
                                                            </button>
                                                            {!! Form::close() !!}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- wishlist end -->
                            </div>
                            {{$wishlist_products->links('vendor.pagination.front')}}
                            <div class="mt-50">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('account.cart')}}" class="previous f-left"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp; {{trans('front/wishlist.back')}}</a>
                                        <a href="{{route('account.order.checkout')}}" class="next f-right">{{trans('front/wishlist.next')}} &nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-10">
                            <div class="tab-content mb-40">
                                <!-- checkout start -->
                                <div class="tab-pane active" id="checkout">
                                    <div class="checkout-content box-shadow p-30">
                                        <div class="payment-method">
                                            <!-- our order -->
                                            <div class="payment-details">
                                                <h4 style="color: #1a237e;margin-bottom: 0px;font-size: 20px;text-align: center"> {{trans('front/wishlist.is_empty')}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-50">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('account.cart')}}" class="previous f-left"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp; {{trans('front/wishlist.back')}}</a>
                                        <a href="{{route('account.order.checkout')}}" class="next f-right">{{trans('front/wishlist.next')}} &nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- SHOP SECTION END -->

    </section>
    <!-- End page content -->

@endsection
