@extends('layouts.front')

@section('title',"Silverest | Checkout")

@section('styles')

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/sweetalert2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://www.omniva.ee/widget/widget.css">

    <!-- Custom Styling -->
    <style>
        .cart_product_img {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.15);
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .qtybutton {
            cursor: default;
        }

        td.cart_thumbnail {
            padding: 20px 30px 20px 20px !important;
        }

        input, textarea {
            font-family: 'Pathway Gothic One', sans-serif;
            font-size: 16px !important;
            color: #1a237e !important;
        }

        .widget-title {
            font-size: 15px;
        }

        span.required {
            color: red;
            font-size: 20px;
        }

        .ap-input-icon svg {
            transform: translateY(-100%);
        }

        input {
            border-radius: 0px !important;
        }

        .ap-input-icon.ap-icon-clear svg {
            transform: translateY(-130%);
        }

        .error {
            border: 1px solid red !important;
        }

        td {
            font-size: 16px !important;
        }

        a.previous, a.next, button.next {
            text-decoration: none;
            display: inline-block;
            padding: 8px 16px;
            cursor: default;
        }

        a.previous:hover, a.next:hover, button.next:hover {
            background-color: #ddd;
            color: black;
        }

        .previous {
            background-color: #f1f1f1;
            color: black;
        }

        .next {
            background-color: #1a237e;
            color: white;
            padding: 8px 16px;
            cursor: default;
        }

        h1.step {
            font-family: "geometria_regular", "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 26px;
            color: #1a237e;
        }

        .payment-title {
            height: auto;
            padding-left: 0px;
        }

        i.check {
            font-size: 30px;
            color: #212c9a;
            margin-top: 35px;
            margin-left: 35px;
        }

        .old-price {
            color: #a6a6a6;
            font-size: 12px;
            text-decoration: line-through;
        }

        .new-price {
            color: #666;
        }

        .no-border-panel {
            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>

    <!-- Sweetalert -->
    <style>
        .sa-button-container {
            margin-bottom: 15px;
        }
    </style>

    <!-- Omniva -->
    <style>
        #omniva_select1 {
            background-color: white;
            border-radius: 0;
            border: 1px solid #d2d8d8;
            margin-bottom: 20px;
            margin-top: 10px;
            width: 100%;
            height: 40px;
            font-size: 16px;
            color: #666666 !important;
        }

        .ow_td {
            padding: 0;
        }
    </style>

@endsection

@section('content')

    <!-- Start page content -->
    <section id="page-content" class="page-wrapper">

        <!-- SHOP SECTION START -->
        <div class="shop-section mb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <ul class="cart-tab">
                            <li>
                                <a class="active" href="{{route('account.cart')}}">
                                    <span>01</span>
                                    {{trans('front/checkout.shopping_cart')}}
                                </a>
                            </li>
                            <li>
                                <a class="active" href="{{route('account.wishlist')}}">
                                    <span>02</span>
                                    {{trans('front/checkout.wishlist')}}
                                </a>
                            </li>
                            <li>
                                <a class="active" href="{{route('account.order.checkout')}}">
                                    <span>03</span>
                                    {{trans('front/checkout.checkout')}}
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span>04</span>
                                    {{trans('front/checkout.order_complete')}}
                                </a>
                            </li>
                        </ul>
                    </div>

                    @if($cart_amount > 0)

                        {!! Form::open(['method'=>'POST','route'=>['account.order.complete'],'id' => 'checkout_form']) !!}

                        <div class="col-md-10">

                            <h1 class="step">{{trans('front/checkout.step')}} 1</h1>

                            <div class="tab-content mb-40">
                                <!-- checkout start -->
                                <div class="tab-pane active" id="checkout">
                                    <div class="checkout-content box-shadow p-30">
                                        <div class="row">
                                            <!-- billing details -->
                                            <div class="col-md-12">
                                                <div class="billing-details pr-10">
                                                    <h6 class="widget-title border-left mb-20">
                                                        <i class="fa fa-user" aria-hidden="true"></i>&nbsp; {{trans('front/checkout.personal_details')}}
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="billing-details pr-10">
                                                    <label>{{trans('front/checkout.first_name')}}
                                                        <span class="required">*</span></label>
                                                    <input type="text" placeholder="{{trans('front/checkout.first_name')}}" name="first_name" value="{{$user->first_name}}" class="form_field {{$errors->has('first_name') ? 'error':''}}" required>
                                                    <label>{{trans('front/checkout.email_address')}}
                                                        <span class="required">*</span></label>
                                                    <input type="text" placeholder="{{trans('front/checkout.email_address')}}" name="email" value="{{$user->email}}" class="form_field {{$errors->has('email') ? 'error':''}}" required>
                                                    <label>{{trans('front/checkout.company_name')}} ({{trans('front/checkout.optional')}})</label>
                                                    <input type="text" placeholder="{{trans('front/checkout.company_name')}}" name="company_name" value="{{$user->company_name}}" class="{{$errors->has('company_name') ? 'error':''}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="billing-details pr-10">
                                                    <label>{{trans('front/checkout.last_name')}}
                                                        <span class="required">*</span></label>
                                                    <input type="text" placeholder="{{trans('front/checkout.last_name')}}" name="last_name" value="{{$user->last_name}}" class="form_field {{$errors->has('last_name') ? 'error':''}}" required>
                                                    <label>{{trans('front/checkout.phone_number')}}
                                                        <span class="required">*</span></label>
                                                    <input type="text" placeholder="{{trans('front/checkout.phone_number')}}" name="phone_number" value="{{$user->phone_number}}" class="form_field {{$errors->has('phone_number') ? 'error':''}}" required>
                                                    <label>{{trans('front/checkout.dob')}} ({{trans('front/checkout.optional')}})</label>
                                                    <input type="text" class="dob" name="dob" class="date_size" placeholder="{{trans('front/checkout.dob')}}" value="{{$user->dob != '0000-00-00' ? $user->dob : ''}}" class="{{$errors->has('dob') ? 'error':''}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- checkout end -->
                            </div>

                            <h1 class="step">{{trans('front/checkout.step')}} 2</h1>

                            <div class="tab-content mb-40">
                                <!-- checkout start -->
                                <div class="tab-pane active" id="checkout">
                                    <div class="checkout-content box-shadow p-30">

                                        <div class="row">
                                            <!-- billing details -->
                                            <div class="col-md-12">
                                                <div class="billing-details pr-10">
                                                    <h6 class="widget-title border-left mb-20">
                                                        <i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp; {{trans('front/checkout.delivery_address')}}
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="accordion">
                                            <div class="panel">
                                                <h4 class="payment-title box-shadow">
                                                    <a data-toggle="collapse" data-parent="#accordion" id="default_address" href="#bank-transfer">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <img src="{{asset('/assets/front/images/banners/eestipost.jpg')}}" style="width: 100px;height: 100px">
                                                            </div>
                                                            <div class="col-md-8 pt-20">
                                                                <h2>{{trans('front/checkout.delivery_by')}}</h2>
                                                                <p>{{trans('front/checkout.package')}} &nbsp;
                                                                    <span style="font-size: 16px;color: #1a237e">5 &euro; </span>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <i class="fa fa-lg fa-check check" id="check-default" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </h4>
                                                <div id="bank-transfer" class="panel-collapse collapse in">
                                                    <div class="row" style="padding-left: 10px">
                                                        <!-- billing details -->
                                                        <div class="col-md-12">
                                                            <div class="billing-details pr-10 mt-20">
                                                                <label>{{trans('front/checkout.find_address')}}</label>
                                                                <input type="text" placeholder="{{trans('front/checkout.start_typing')}}..." id="form-address">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="billing-details pr-10">
                                                                <label>{{trans('front/checkout.address_line')}}
                                                                    <span class="required">*</span></label>
                                                                <input class="default_delivery form_field" type="text" placeholder="{{trans('front/checkout.address_line')}}" id="form-address2" name="address_line" value="{{$delivery_address ? $delivery_address->address_line : '' }}" class="{{$errors->has('address_line') ? 'error':''}}" required>
                                                                <label>{{trans('front/checkout.country')}}
                                                                    <span class="required">*</span></label>
                                                                <input class="default_delivery form_field" type="text" placeholder="{{trans('front/checkout.country')}}" id="form-country" name="country" value="{{$delivery_address ? $delivery_address->country : '' }}" class="{{$errors->has('country') ? 'error':''}}" required>
                                                                <label>{{trans('front/checkout.state_region')}}
                                                                    <span class="required">*</span></label>
                                                                <input class="default_delivery form_field" type="text" placeholder="{{trans('front/checkout.state_region')}}" id="form-state" name="state" value="{{$delivery_address ? $delivery_address->state : '' }}" class="{{$errors->has('state') ? 'error':''}}" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="billing-details pr-10">
                                                                <label>{{trans('front/checkout.city')}}
                                                                    <span class="required">*</span></label>
                                                                <input class="default_delivery form_field" type="text" placeholder="{{trans('front/checkout.city')}}" id="form-city" name="city" value="{{$delivery_address ? $delivery_address->city : '' }}" class="{{$errors->has('city') ? 'error':''}}" required>
                                                                <label>{{trans('front/checkout.zip')}}
                                                                    <span class="required">*</span></label>
                                                                <input class="default_delivery form_field" type="text" placeholder="{{trans('front/checkout.zip')}}" id="form-zip" name="zip" value="{{$delivery_address ? $delivery_address->zip : '' }}" class="{{$errors->has('zip') ? 'error':''}}" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <h4 class="payment-title box-shadow">
                                                    <a class="collapsed" id="omniva_address" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <img src="{{asset('/assets/front/images/banners/omniva.jpg')}}" style="width: 100px;height: 100px">
                                                            </div>
                                                            <div class="col-md-8 pt-20">
                                                                <h2>{{trans('front/checkout.delivery')}}</h2>
                                                                <p>{{trans('front/checkout.package')}} &nbsp;
                                                                    <span style="font-size: 16px;color: #1a237e">3 &euro; </span>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <i class="fa fa-lg fa-check check" id="check-omniva" aria-hidden="true" style="display: none"></i>
                                                            </div>
                                                        </div>
                                                    </a>

                                                </h4>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="row" style="padding-left: 10px">
                                                        <!-- billing details -->
                                                        <div class="col-md-12">
                                                            <div class="billing-details pr-10 mt-20">
                                                                <label>{{trans('front/checkout.enter_omniva')}}:</label>
                                                                {{--<input class="form_field omniva_delivery" name="omniva_delivery" type="text" placeholder="{{trans('front/checkout.enter_closest_omniva')}}"/>--}}
                                                                <div id="omniva_container1">
                                                                    <select id="omniva_select1" class="form_field" placeholder="{{trans('front/checkout.enter_closest_omniva')}}">
                                                                    </select>
                                                                    <input name="omniva_delivery" type="hidden"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <p style="font-size: 16px" class="mt-50">{{trans('front/checkout.order_sent')}}*</p>

                                        <input type="hidden" name="delivery_method" value="local post">

                                    </div>
                                </div>
                                <!-- checkout end -->
                            </div>

                            <h1 class="step">{{trans('front/checkout.step')}} 3</h1>

                            <div class="tab-content mb-40">
                                <!-- checkout start -->
                                <div class="tab-pane active" id="checkout">
                                    <div class="checkout-content box-shadow p-30">
                                        <div class="row">
                                            <!-- billing details -->
                                            <div class="col-md-12">
                                                <div class="billing-details pr-10">
                                                    <h6 class="widget-title border-left mb-20">
                                                        <i class="fa fa-tasks" aria-hidden="true"></i>&nbsp; {{trans('front/checkout.order_details')}}
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="tab-content">
                                                    <!-- shopping-cart start -->
                                                    <div class="tab-pane active" id="shopping-cart">
                                                        <div class="shopping-cart-content">
                                                            <div class="table-content table-responsive mb-20">
                                                                <table class="text-center">
                                                                    <thead>
                                                                    <tr>
                                                                        <th class="product-thumbnail">{{trans('front/checkout.product')}}</th>
                                                                        <th class="product-price" style="width: 130px;">{{trans('front/checkout.price')}}</th>
                                                                        <th class="product-quantity">{{trans('front/checkout.quantity')}}</th>
                                                                        <th class="product-subtotal" style="min-width: 130px;">{{trans('front/checkout.total')}}</th>
                                                                        <th class="product-remove">{{trans('front/checkout.remove')}}</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($cart_products as $cart_product)
                                                                        <!-- tr -->
                                                                        <tr>
                                                                            <td class="product-thumbnail cart_thumbnail">
                                                                                <div class="pro-thumbnail-img">
                                                                                    <img src="{{$cart_product->photos()->count() > 0 ?  $cart_product->photos->first()->getPreviewPath() : ''}}" alt="" class="cart_product_img">
                                                                                </div>
                                                                                <div class="pro-thumbnail-info text-left">
                                                                                    <h6 class="product-title-2">
                                                                                        <a>{{$cart_product->name}} ( {{$cart_product->code}} )</a>
                                                                                    </h6>
                                                                                    @if($cart_product->gems()->count() > 0)
                                                                                        <p>
                                                                                            <span>{{trans('front/checkout.gem')}}
                                                                                                <strong>: </strong></span>
                                                                                            @foreach($cart_product->gems as $gem)
                                                                                                @if($loop->last)
                                                                                                    {{$gem->name}}
                                                                                                @else
                                                                                                    {{$gem->name}},
                                                                                                @endif
                                                                                            @endforeach
                                                                                        </p>
                                                                                    @endif
                                                                                    @if($cart_product->pivot->size > 0)
                                                                                        <p>
                                                                                            <span>{{trans('front/checkout.size')}}
                                                                                                <strong>: </strong></span>{{$cart_product->pivot->size}}
                                                                                        </p>
                                                                                    @endif
                                                                                </div>
                                                                            </td>
                                                                            <td class="product-price">
                                                                                @if($cart_product->getDiscountPrice())
                                                                                    <span class="new-price">&euro; {{number_format ($cart_product->getDiscountPrice(),2)}}</span>
                                                                                    <br>
                                                                                    <span class="old-price">&euro; {{number_format ($cart_product->price,2)}}</span>
                                                                                @else
                                                                                    <span class="new-price">&euro; {{number_format ($cart_product->price,2)}}</span>
                                                                                @endif
                                                                            </td>
                                                                            <td class="product-quantity">
                                                                                <div class="cart-plus-minus f-left">
                                                                                    <input type="text" value="{{$cart_product->pivot->quantity}}" max="{{$cart_product->quantity}}" min="0" disabled name="qtybutton" data-product="{{$cart_product->id}}" class="cart-plus-minus-box">
                                                                                </div>
                                                                            </td>
                                                                            <td class="product-subtotal">&euro; {{($cart_product->getDiscountPrice() ? number_format ($cart_product->getDiscountPrice() * $cart_product->pivot->quantity,2) : number_format ($cart_product->price * $cart_product->pivot->quantity,2))}}</td>
                                                                            <td class="product-remove">
                                                                                <button class="cart_remove" value="{{$cart_product->id}}">
                                                                                    <i class="zmdi zmdi-close"></i>
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- shopping-cart end -->
                                                </div>

                                                <div class="payment-method">
                                                    <!-- our order -->
                                                    <div class="payment-details">
                                                        <table>
                                                            <tr>
                                                                <td class="td-title-1">{{trans('front/checkout.cart_subtotal')}}</td>
                                                                <td class="td-title-2">&euro; {{number_format ($cart_subtotal,2)}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td-title-1">{{trans('front/checkout.shipping_handling')}}</td>
                                                                <td class="td-title-2">&euro;
                                                                    <span id="shipping_price">5.00</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td-title-1">{{trans('front/checkout.vat')}}</td>
                                                                <td class="td-title-2">20 %</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="order-total">{{trans('front/checkout.order_total')}}</td>
                                                                <td class="order-total-price">&euro;
                                                                    <span id="total_price">{{number_format (($cart_subtotal + 5.00) ,2)}}</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- checkout end -->
                            </div>

                            <h1 class="step">{{trans('front/checkout.step')}} 4</h1>

                            <div class="tab-content">
                                <!-- checkout start -->
                                <div class="tab-pane active" id="checkout">
                                    <div class="checkout-content box-shadow p-30">
                                        <div class="row">
                                            <!-- billing details -->
                                            <div class="col-md-12">
                                                <div class="billing-details pr-10">
                                                    <h6 class="widget-title border-left mb-20">{{trans('front/checkout.payment_method')}}</h6>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div id="accordion_payment">
                                                    <div class="panel">
                                                        <h4 class="payment-title box-shadow">
                                                            <a data-toggle="collapse" data-parent="#accordion_payment" id="quote_payment" href="#bankQuote">
                                                                <div class="row">
                                                                    <div class="col-md-10 pt-20">
                                                                        <h2 class="ml-20">{{trans('front/checkout.pay_by_bank_transfer')}}</h2>
                                                                        <p class="ml-20">
                                                                            {{trans('front/checkout.you_will_receive')}}
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <i class="fa fa-lg fa-check check" id="check-quote" aria-hidden="true"></i>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </h4>
                                                        <div id="bankQuote" class="panel-collapse collapse in">
                                                            <div class="row" style="padding-left: 10px">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="panel" style="background-color: #f3f3f3;">
                                                        <h4 class="payment-title box-shadow">
                                                            <a class="collapsed">
                                                            {{-- TODO: uncomment --}}
                                                            {{--<a class="collapsed" id="online_payment" data-toggle="collapse" data-parent="#accordion_payment" href="#collapseOnlinePayment">--}}
                                                                <div class="row">
                                                                    <div class="col-md-10 pt-20">
                                                                        <h2 class="ml-20">{{trans('front/checkout.pay_by_card_or_paypal')}}</h2>
                                                                        <p class="ml-20">
                                                                            {{trans('front/checkout.you_could_pay_by')}}
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <i class="fa fa-lg fa-check check" id="check-card" aria-hidden="true" style="display: none"></i>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </h4>
                                                        <div id="collapseOnlinePayment" class="panel-collapse collapse">
                                                            <div class="row" style="padding: 10px;padding-top: 20px;">
                                                                <!-- billing details -->
                                                                <div class="col-md-12">
                                                                    <div id="dropin-container"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- checkout end -->
                            </div>

                            <div style="margin-top: 50px">
                                <a href="{{route('account.wishlist')}}" class="previous f-left"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp; {{trans('front/checkout.back')}}
                                </a>
                                <button id="submit-button" type="submit" class="next f-right">{{trans('front/checkout.place_order')}} &nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    @else
                        <div class="col-md-10">
                            <div class="tab-content mb-40">
                                <!-- checkout start -->
                                <div class="tab-pane active" id="checkout">
                                    <div class="checkout-content box-shadow p-30">
                                        <div class="payment-method">
                                            <!-- our order -->
                                            <div class="payment-details">
                                                <h4 style="color: #1a237e;margin-bottom: 0px;font-size: 20px;text-align: center"> {{trans('front/checkout.shopping_cart_empty')}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-50">
                                <a href="{{route('account.wishlist')}}" class="previous f-left"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp; {{trans('front/checkout.back')}}
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- SHOP SECTION END -->

    </section>
    <!-- End page content -->

@endsection

@section('scripts')

    <!-- Javascript files -->
    <script src="{{asset('assets/front/js/cleave.min.js')}}"></script>
    <script src="{{asset('assets/front/js/places.js')}}"></script>
    <script src="{{asset('assets/front/js/sweetalert.min.js')}}"></script>
    <script src="https://js.braintreegateway.com/web/dropin/1.9.3/js/dropin.min.js"></script>
    <script type="text/javascript" src="https://www.omniva.ee/widget/widget.js"></script>

    <!-- Auto complete -->
    <script>
        var cleave = new Cleave('.dob', {
            date: true,
            datePattern: ['Y', 'm', 'd']
        });
    </script>

    <!-- Places -->
    <script>
        (function () {
            var placesAutocomplete = places({
                container: document.querySelector('#form-address')
            });
            placesAutocomplete.on('change', function resultSelected(e) {
                document.querySelector('#form-address2').value = e.suggestion.name || '';
                document.querySelector('#form-state').value = e.suggestion.administrative || '';
                document.querySelector('#form-country').value = e.suggestion.country || '';
                document.querySelector('#form-city').value = e.suggestion.city || '';
                document.querySelector('#form-zip').value = e.suggestion.postcode || '';
            });
        })();
    </script>

    <!-- Cart -->
    <script>
        /* ********************************************
		   Cart Plus Minus Button
	    ******************************************** */
        $(".cart-plus-minus").prepend('<div class="dec qtybutton">-</div>');
        $(".cart-plus-minus").append('<div class="inc qtybutton">+</div>');
        $(".qtybutton").on("click", function () {
            var $button = $(this);
            var oldValue = $button.parent().find("input").val();
            if ($button.text() == "+") {
                var newVal = parseFloat(oldValue) + 1;
            }
            else {
                // Don't allow decrementing below zero
                if (oldValue > 0) {
                    var newVal = parseFloat(oldValue) - 1;
                }
                else {
                    newVal = 0;
                }
            }

            var product_id = $button.parent().find("input").attr('data-product');
            var data = {
                _token: '{!! csrf_token() !!}',
                product_id: product_id,
                quantity: newVal
            }

            $.ajax({
                'url': "{{route('account.cart.quantity.ajax')}}",
                'data': data,
                'method': 'POST',
                success: function (response) {
                    location.reload();
                }
            });

            $button.parent().find("input").val(newVal);
        });
    </script>

    @if($cart_amount > 0)

        <!-- Update Delivery Address -->
        <script>
            const total_price = parseFloat({{$cart_subtotal}});

            $("#default_address").on("click", function (e) {
                $("#shipping_price").text("5.00");
                $("input[name=delivery_method]").val("local post");
                $("#total_price").text((total_price + 5).toFixed(2));
                $("input.default_delivery").prop('required', true);
                $("select#omniva_select1").prop('required', false);

                $("i[id=check-omniva]").hide();
                $("i[id=check-default]").show();
            });

            $("#omniva_address").on("click", function (e) {
                // reset omniva
                $('select#omniva_select1').val('');
                $("select#omniva_select1").append('<option disabled selected value> -- {{trans('front/checkout.please_select_automaat')}} --</option>\n');

                $("#shipping_price").text("3.00");
                $("input[name=delivery_method]").val('omniva');
                $("#total_price").text((total_price + 3.0).toFixed(2));

                $("input.default_delivery").prop('required', false);
                $("select#omniva_select1").prop('required', true);

                $("i[id=check-omniva]").show();
                $("i[id=check-default]").hide();
            });
        </script>

        <!-- Update Payment -->
        <script>
            var quote = $('<input>').attr({type: 'hidden', id: 'quote', name: 'quote', value: 'quote_transfer'});
            $(quote).appendTo('#bankQuote');

            $("#quote_payment").on("click", function (e) {
                $(quote).appendTo('#bankQuote');

                // TODO: uncomment
                // $("i[id=check-card]").hide();
                // $("i[id=check-quote]").show();
            });

            // TODO: uncomment
            // $("#online_payment").on("click", function (e) {
            //     $("i[id=check-quote]").hide();
            //     $("i[id=check-card]").show();
            //     $('#quote').remove();
            // });
        </script>

        <!-- Update Cart -->
        <script>
            $(".cart_remove").on("click", function (e) {
                e.preventDefault();
                var product_id = $(this).val();

                var data = {
                    _token: '{!! csrf_token() !!}',
                    product_id: product_id,
                };

                $.ajax({
                    'url': "{{route('account.cart.remove')}}",
                    'data': data,
                    'method': 'POST',
                    success: function (response) {
                        location.reload();
                    }
                });
            });

            function validateForm() {
                var isValid = true;
                $('input.form_field[required]').each(function () {
                    if ($(this).val() === '') {
                        isValid = false;
                        $(this).addClass('error');
                    }
                });

                if ($('select#omniva_select1').prop('required') && !$('select#omniva_select1').val()) {
                    $('select#omniva_select1').addClass('error');
                    isValid = false;
                }

                if (!isValid) {
                    swal("Please Enter All Fields!", "", "error");
                }
                return isValid;
            }
        </script>

        <!-- Omniva -->
        <script>
            OmnivaWidget({
                compact_mode: true,
                country_id: 'EE',
                show_logo: false,
                show_offices: false,
                custom_html: true
            });

            $('select#omniva_select1').on('change', function (x) {
                $('input[name="omniva_delivery"]').val('Code: ' + $(this).val() + ' Address: ' + $('select#omniva_select1 option:selected').text());
                $('select#omniva_select1').removeClass('error');
            });
        </script>

    @endif

@endsection
