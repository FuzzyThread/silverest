@extends('layouts.front')

@section('title',"Silverest | Orders")

@section('styles')

    <link rel="stylesheet" href="{{asset('assets/admin/css/sweetalert2.min.css')}}">

    <!-- Custom Styling -->
    <style>
        #cancel_search:hover {
            color: #1a237e;
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .date_size {
            width: 100%;
            line-height: 33px;
            padding-left: 20px;
        }

        input, textarea {
            font-family: 'Pathway Gothic One', sans-serif;
            font-size: 16px !important;
            color: #1a237e !important;
        }

        .btn-large {
            border: 1px solid #1a237e;
            padding: 20px;
            margin-top: 40px;
        }

        .btn-large:hover {
            border: 1px solid #2529c3;
            color: #1a237e;
        }

        a:hover {
            color: #1a237e;
        }

        a.previous, button.previous {
            text-decoration: none;
            display: inline-block;
            border: 1px solid #1a237e;
            padding: 8px 16px;
            width: 100%;
        }

        a.previous:hover, button.previous:hover {
            background-color: #ddd;
            color: black;
        }

        .previous {
            background-color: #f1f1f1;
            color: black;
        }

        .next {
            background-color: #1a237e;
            color: white;
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .qtybutton {
            cursor: default;
        }

        td.cart_thumbnail {
            padding: 20px 30px 20px 20px !important;
        }

        .small-title {
            /*font-size: 16px;*/
            /*margin-bottom: 10px !important;*/
        }

        .product-thumbnail {
            border: 1px solid #dfdfdf;;
            height: 107px;
            width: 334px;
        }

        .product-list {
            margin-bottom: 20px;
        }

        .product-title-2 {
            margin-bottom: 5px;
        }

        .cart_product_img {
            border-right: 1px solid #dfdfdf;
        }

        .pro-thumbnail-info {
            width: 68%;
        }

        .pro-thumbnail-img {
            float: left;
            width: 32%;
        }
    </style>

    <!-- Style Tweaks #1 -->
    <style>
        .sa-button-container {
            margin-bottom: 15px;
        }

        a {
            cursor: default;
        }

        #note-textarea {
            width: 100%;
            height: 150px;
            padding: 15px;
        }

        .note-info {
            background: #ffffff none repeat scroll 0 0;
            float: left;
            padding: 15px 15px 15px 15px;
            text-align: left;
            width: 100%;
        }

        .single_add_to_cart_button {
            margin-left: 0px !important;
            margin-top: 30px;
            float: right !important;
        }

    </style>

    <!-- Style Tweaks #2 -->
    <style>
        a.previous_bt {
            text-decoration: none;
            display: inline-block;
            padding: 8px 16px;
        }

        a.previous_bt:hover {
            background-color: #ddd;
            color: black;
        }

        .previous_bt {
            background-color: #1a237e;
            color: white;
        }
    </style>

@endsection

@section('content')

    <!-- Start page content -->
    <div id="page-content" class="page-wrapper">

        <!-- LOGIN SECTION START -->
        <div class="login-section mb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="my-account-content">
                            <!-- My Personal Information -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h1>{{trans('front/orders.orders')}}</h1>
                                </div>


                                <div class="row" style="padding: 20px;padding-top: 0px;margin-bottom: 20px">
                                    <div class="col-md-12">

                                        @if($user->orders->count() > 0)
                                            @foreach($user->orders as $order)
                                                <div class="btn-large">
                                                    <h5 style="margin-bottom: 20px">
                                                        <div class="row" style="border-bottom: 1px solid #1a237e; padding-bottom: 20px;">
                                                            <div class="col-md-3">
                                                                <i class="fa fa-tasks" aria-hidden="true"></i>&nbsp; Order # {{$order->id}}
                                                            </div>
                                                            <div class="col-md-3">
                                                                Status :
                                                                <span style="color: #1a237e">{{$order->status ? $order->status->name : 'Error'}}</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                                Total :
                                                                <span style="color: #1a237e"> {{$order->payment ? '&euro; ' . $order->payment->subtotal : 'Error'}}</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                                Order Placed :
                                                                <span style="color: #1a237e">{{date('j F Y', strtotime($order->created_at))}}</span>
                                                            </div>
                                                        </div>
                                                    </h5>
                                                    <div class="row">
                                                        @if($order->items()->count() > 0)
                                                            @foreach($order->items as $item)
                                                                @php
                                                                    $product = \App\Product::withTrashed()->find($item->product_id);
                                                                @endphp
                                                                @if($product)
                                                                    <div class="col-md-4 product-list">
                                                                        <div class="product-thumbnail cart_thumbnail">
                                                                            <div class="pro-thumbnail-img" style="width: 31% !important;">
                                                                                <img src="{{$product->photos()->count() > 0 ?  $product->photos->first()->getPreviewPath() : ''}}" alt="" class="cart_product_img">
                                                                            </div>
                                                                            <div class="pro-thumbnail-info text-left" style="width: 69% !important;">
                                                                                <h6 class="product-title-2">
                                                                                    <a>{{$product->name}} ( {{$product->code}} )</a>
                                                                                </h6>
                                                                                @if($product->gems()->count() > 0)
                                                                                    <p class="small-title">
                                                                                        <span>{{trans('front/orders.gem')}}
                                                                                            <strong>: </strong></span>
                                                                                        @foreach($product->gems()->limit(2)->get() as $gem)
                                                                                            @if($loop->last)
                                                                                                {{str_limit($gem->name,7)}}
                                                                                            @else
                                                                                                {{str_limit($gem->name,7)}},
                                                                                            @endif
                                                                                        @endforeach
                                                                                    </p>
                                                                                @endif
                                                                                @if($item->size)
                                                                                    <p class="small-title">
                                                                                        <span>{{trans('front/orders.size')}}
                                                                                            <strong>: </strong></span>{{$item->size}}
                                                                                    </p>
                                                                                @endif
                                                                                <p class="small-title">
                                                                                    <span>{{trans('front/orders.quantity')}}
                                                                                        <strong>: </strong></span>{{$item->quantity}}
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    <div class="col-md-4 product-list">
                                                                        <div class="product-thumbnail cart_thumbnail">
                                                                            <div class="pro-thumbnail-img" style="width: 31% !important;">
                                                                            </div>
                                                                            <div class="pro-thumbnail-info text-left" style="width: 69% !important;">
                                                                                <h6 class="product-title-2">
                                                                                    {{$item->product_id}}
                                                                                    Error {{$product}}
                                                                                </h6>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @php
                                                                $order->status_id = \Illuminate\Support\Facades\Config::get('constants.STATUS.ERROR');
                                                                $order->save();
                                                            @endphp
                                                            <div class="col-md-12 payment-method">
                                                                <!-- our order -->
                                                                <div class="payment-details">
                                                                    <h4 style="color: #1a237e;margin-bottom: 0px"> {{trans('front/orders.there_are_no_products')}} (ERROR)</h4>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="row" style="padding-top: 20px;border-top: 1px solid #dfdfdf;">
                                                        <div class="col-md-3">
                                                            <a href="{{route('account.order',$order->id)}}" class="previous">
                                                                <i class="fa fa-eye" aria-hidden="true"></i>&nbsp;&nbsp; {{trans('front/orders.show_more_details')}}
                                                            </a>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <a class="previous" href="{{route('account.order.invoice.download',$order->id)}}">
                                                                <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>&nbsp;&nbsp; {{trans('front/orders.download_invoice')}}
                                                            </a>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <a data-order="{{$order->id}}" data-toggle="modal" data-target="#productModal" class="previous order_notes">
                                                                <i class="fa fa-comments" aria-hidden="true"></i> &nbsp; {{trans('front/orders.extra_notes')}}
                                                            </a>
                                                        </div>
                                                        <div class="col-md-3">
                                                            @if($order->payment)
                                                                @if($order->status->id == config('constants.STATUS.AWAITING_PAYMENT') ||
                                                                    $order->status->id == config('constants.STATUS.PREPARATION'))
                                                                    {!! Form::open(['method'=>'POST','route'=> 'account.order.cancel','class'=>'order-cancel']) !!}
                                                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                                                    <button class="previous" type="submit" style="text-align: left">
                                                                        <i class="fa fa-ban" aria-hidden="true"></i>&nbsp;&nbsp; {{trans('front/orders.cancel_order')}}
                                                                    </button>
                                                                    {!! Form::close() !!}
                                                                @endif
                                                            @else
                                                                <button class="previous" type="submit" style="text-align: left">
                                                                    <i class="fa fa-ban" aria-hidden="true"></i>&nbsp;&nbsp; Error
                                                                </button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        @else
                                            <div class="btn-large">
                                                <h2>
                                                    {{trans('front/orders.no_orders')}}
                                                </h2>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div style="margin-top: 50px">
                            <a href="{{route('account')}}" class="previous_bt f-left"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp; {{trans('front/orders.back')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- LOGIN SECTION END -->
    </div>
    <!-- End page content -->

@endsection

@section('scripts')

    <!-- Javascript files -->
    <script src="{{asset('assets/front/js/sweetalert.min.js')}}"></script>

    <!-- Order cancel -->
    <script>
        $(".order-cancel").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You want to cancel this order",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, leave it live",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Order Cancelled!',
                            text: 'Order is successfully cancelled!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your order is still live", "error");
                    }
                }
            );
        });
    </script>

    <!-- Order notes -->
    <script>
        // get product cart preview:
        $('a.order_notes').click(function () {

            var order_id = $(this).attr('data-order');
            var data = {
                _token: '{!! csrf_token() !!}',
                order_id: order_id
            }

            $.ajax({
                'url': "{{route('account.order.notes.ajax')}}",
                'data': data,
                'method': 'POST',
                success: function (response) {
                    console.log(response);
                    var order_notes = $('.modal-product');
                    order_notes.html(response);
                }
            });
        });
    </script>

@endsection
