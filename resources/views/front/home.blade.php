@extends('layouts.front')

@section('title',"Silverest | Home")

@section('styles')

    <!-- Custom Styling -->
    <style>
        .subcribe .submit-btn-2 {
            border-radius: 0px;
        }

        .up-comming-pro-img img {
            border: 1px solid #f1f1f1;
        }

        .banner-img img {
            border: 1px solid #e1e1e1;
            /*box-shadow: 0px 2px 2px 2px rgba(0, 0, 0, 0.05);*/
            /*width: calc(100% - 5px);*/
        }

        .gray-bg {
            background: #f6f6f6;
        }

        .slick-arrow-2 .arrow-next.slick-arrow, .slick-arrow-2 .arrow-prev.slick-arrow {
            border-radius: 0px;
        }

        .product-item-2 img, .product-item-2 .product-info, .product-item-2 .action-button {
            border-radius: 0px;
            box-shadow: 0px 2px 2px 2px rgba(0, 0, 0, 0.05);
        }

        .up-comming-pro-info p {
            margin-bottom: 10px;
        }

        h3.price-discount {
            margin: 20px 0px 30px 0px;
        }

        .s-price-box.new-product {
            padding: 0;
            padding-top: 5px;
            border: none;
            width: 100%;
        }

        .s-price-box.banner {
            padding: 20px 0;
        }

        .s-price-box.banner .new-price {
            font-size: 24px;
        }

        .s-price-box.banner .old-price {
            font-size: 18px;
        }

    </style>

    <!-- Style Tweaks -->
    <style>
        .featured-product-section.section-bg-tb::before, .product-tab-section.section-bg-tb::before {
            transform: none;
            opacity: 0.95;
        }

        .ribbon-price {
            left: 22px;
            position: absolute;
            top: 34px;
            transform: rotate(-42deg);
        }

        .slider-info {
            /*z-index: -1;*/
            border: 1px solid #1a237e;

        }
    </style>

@endsection

@section('content')

    @if($featured_products)
        <!-- START SLIDER AREA -->
        <div class="slider-area  plr-200  mb-80" style="margin: 25px 0">
            <div class="container">
                <div class="slider-content" style="border: 1px solid #1a237e;padding: 30px 20px 10px 20px">
                    <div class="row">
                        <div class="active-slider-1 slick-arrow-1 slick-dots-1">
                        @foreach($featured_products as $featured_product)
                            <!-- layer-1 Start -->
                                <div class="col-md-12">
                                    <div class="layer-1">
                                        <div class="row" style="margin: 0 auto">
                                            <div class="col-md-4">
                                                <img class="loading" src="{{$featured_product->photos()->count() > 0 ? $featured_product->photos->first()->getPhotoPath() : '' }}" alt=""
                                                     style="max-width: 100%;max-height: 100%;">
                                            </div>
                                            <div class="col-md-8">
                                                <div style="margin-left: 50px;">
                                                    <h1 class="slider-title-1 text-uppercase text-black-1">
                                                        <a href="{{route('catalogue.product',$featured_product->id)}}">{{$featured_product->name}} &nbsp;&nbsp;&nbsp;&nbsp;</a>
                                                    </h1>
                                                    <div class="clearfix">
                                                        <h6 class="brand-name mb-60 f-left">
                                                            <i style="font-size: 16px">{{trans('front/home.code')}}: {{$featured_product->code}}</i>
                                                        </h6>
                                                        <div class="pro-rating product-rating f-left" data-rating="{{$featured_product->getRating()}}" style="font-size: 16px;margin-left: 40px"></div>
                                                    </div>
                                                    <a href="{{route('catalogue.product',$featured_product->id)}}" class="button extra-small button-black">
                                                        <span class="text-uppercase">{{trans('front/home.check_out')}}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- layer-1 end -->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SLIDER AREA -->
    @endif


    <!-- START PAGE CONTENT -->
    <section id="page-content" class="page-wrapper">

    @if($new_products)

        <!-- FEATURED PRODUCT SECTION START -->
            <div class="featured-product-section section-bg-tb pt-80 pb-55 bg-3" style="margin: 0;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-left mb-20">
                                <h2 class="uppercase">{{trans('front/home.new_products')}}</h2>
                                <h6>{{trans('front/home.catalogue_update')}}</h6>
                            </div>
                        </div>
                    </div>
                    <div class="featured-product">
                        <div class="row active-featured-product slick-arrow-2">

                        @foreach($new_products as $new_product)
                            <!-- product-item start -->
                                <div class="col-xs-12">
                                    <div class="product-item product-item-2">
                                        <div class="product-img">
                                            <a href="{{route('catalogue.product',$new_product->id)}}">
                                                <img class="loading" src="{{$new_product->photos()->count() > 0 ? $new_product->photos->first()->getPreviewPath() :'' }}" alt=""/>
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h6 class="product-title">
                                                <a href="{{route('catalogue.product',$new_product->id)}}">{{$new_product->name}}</a>
                                            </h6>
                                            <h6 class="brand-name">{{$new_product->code}}</h6>
                                            {{--<h3 class="price-discount">Price: &nbsp;&nbsp;&nbsp;&euro; <span style="color: #1a237e">{{$discount_offer_product->getDiscountPrice()}}</span> <span style="color: gray">({{$discount_offer_product->price}})</span></h3>--}}
                                            <div class="s-price-box new-product">
                                                @if($new_product->getDiscountPrice())
                                                    <span class="new-price">&euro; {{$new_product->getDiscountPrice()}}</span>
                                                    <span class="old-price">&euro; {{$new_product->price}}</span>
                                                @else
                                                    <span class="new-price">&euro; {{$new_product->price}}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <ul class="action-button">
                                            <li>
                                                {!! Form::open(['method'=>'POST','route'=>['account.wishlist.change']]) !!}
                                                <input type="hidden" name="product_id" value="{{$new_product->id}}">
                                                <button class="cart_wishlist_home {{(Auth::check() && Auth::user()->wishlists->contains($new_product->id))?'active':''}} tippy" title="{{trans('front/home.add_to_wishlist')}}" data-product="{{$new_product->id}}">
                                                    <i class="zmdi zmdi-favorite"></i>
                                                </button>
                                                {!! Form::close() !!}
                                            </li>
                                            <li>
                                                <a class="cart_product tippy" title="{{trans('front/home.add_to_cart')}}" data-toggle="modal" data-target="#productModal" data-product="{{$new_product->id}}"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- product-item end -->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- FEATURED PRODUCT SECTION END -->
    @endif

    @if($discount_offer_product && $discount_offer_product->photos()->count() > 1)
        <!-- UP COMMING PRODUCT SECTION START -->
            <div class="up-comming-product-section ptb-60">
                <div class="container">
                    <div class="row">
                        <!-- up-comming-pro -->
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <div class="gray-bg clearfix">
                                {{--<div class="up-comming-pro gray-bg up-comming-pro-2 clearfix">--}}
                                <div class="up-comming-pro-img f-left">
                                    <a href="{{route('catalogue.product',$discount_offer_product->id)}}">
                                        <img class="loading" src="{{$discount_offer_product->photos()->count() > 0 ? $discount_offer_product->photos->first()->getPhotoPath() : ''}}" alt="">
                                    </a>
                                </div>
                                <div class="up-comming-pro-info f-left">
                                    <h3>
                                        <a href="{{route('catalogue.product',$discount_offer_product->id)}}">{{$discount_offer_product->name}}</a>
                                    </h3>
                                    <div class="clearfix">
                                        <h6 class="brand-name mb-30 f-left">
                                            <i style="font-size: 16px">CODE: {{$discount_offer_product->code}}</i>
                                        </h6>
                                        <div class="pro-rating product-rating f-left" data-rating="{{$discount_offer_product->getRating()}}" style="font-size: 16px;margin-left: 40px"></div>
                                    </div>
                                    <div class="s-price-box banner mb-30">
                                        @if($discount_offer_product->getDiscountPrice())
                                            <span class="new-price">&euro; {{$discount_offer_product->getDiscountPrice()}}</span>
                                            <span class="old-price">&euro; {{$discount_offer_product->price}}</span>
                                        @else
                                            <span class="new-price">&euro; {{$discount_offer_product->price}}</span>
                                        @endif
                                    </div>
                                    <div class="up-comming-time-2 clearfix">
                                        <div data-countdown="{{$discount_offer_product->discount_end_date}}"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <div class="banner-item banner-1">
                                <div class="ribbon-price">
                                    <span>-&nbsp;{{$discount_offer_product->discount_rate < 10?'&nbsp;':''}}{{$discount_offer_product->discount_rate}}%</span>
                                </div>
                                <div class="banner-img">
                                    <a href="{{route('catalogue.product',$discount_offer_product->id)}}"><img class="loading" src="{{$discount_offer_product->photos && $discount_offer_product->photos()->count() > 1  ? $discount_offer_product->photos->offsetGet(1)->getPhotoPath() : ''}}" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- UP COMMING PRODUCT SECTION END -->
    @endif

    <!-- PRODUCT TAB SECTION START -->
        <div class="product-tab-section section-bg-tb pt-80 pb-55 bg-3" style="margin: 0;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-left mb-40">
                            <h2 class="uppercase">{{trans('front/home.our_local_shop')}}</h2>
                            <h6>{{trans('front/home.we_could_gladly')}},</h6>
                        </div>
                    </div>
                </div>
                <div class="blog">
                    <div class="row active-blog slick-arrow-2">
                        <!-- blog-item start -->
                        <div class="col-xs-12">
                            <div class="blog-item">
                                <img class="loading" src="/images/home/fama2.jpg" alt="">
                                <div class="blog-desc">
                                    <h5 class="blog-title">
                                        <a href="single-blog.html">{{trans('front/home.shop_in_fama')}}</a></h5>
                                    <p>{{trans('front/about_us.about_us_2')}}</p>
                                    <div class="read-more">
                                        <a href="{{route('about-us')}}">{{trans('front/home.read_more')}}...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- blog-item end -->
                        <!-- blog-item start -->
                        <div class="col-xs-12">
                            <div class="blog-item">
                                <img class="loading" src="/images/home/fama3.jpg" alt="">
                                <div class="blog-desc">
                                    <h5 class="blog-title">
                                        <a href="single-blog.html">{{trans('front/home.shop_in_fama')}}</a></h5>
                                    <p>{{trans('front/about_us.about_us_2')}}</p>
                                    <div class="read-more">
                                        <a href="{{route('about-us')}}">{{trans('front/home.read_more')}}...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- blog-item end -->
                        <!-- blog-item start -->
                        <div class="col-xs-12">
                            <div class="blog-item">
                                <img class="loading" src="/images/home/vitrina.jpg" alt="">
                                <div class="blog-desc">
                                    <h5 class="blog-title">
                                        <a href="single-blog.html">{{trans('front/home.shop_in_fama')}}</a></h5>
                                    <p>{{trans('front/about_us.about_us_2')}}</p>
                                    <div class="read-more">
                                        <a href="{{route('about-us')}}">{{trans('front/home.read_more')}}...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- blog-item end -->
                        <!-- blog-item start -->
                        <div class="col-xs-12">
                            <div class="blog-item">
                                <img class="loading" src="/images/home/vitrina2.jpg" alt="">
                                <div class="blog-desc">
                                    <h5 class="blog-title">
                                        <a href="single-blog.html">{{trans('front/home.shop_in_fama')}}</a></h5>
                                    <p>{{trans('front/about_us.about_us_2')}}</p>
                                    <div class="read-more">
                                        <a href="{{route('about-us')}}">{{trans('front/home.read_more')}}...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- blog-item end -->
                        <!-- blog-item start -->
                        <div class="col-xs-12">
                            <div class="blog-item">
                                <img class="loading" src="/images/home/fama1.jpg" alt="">
                                <div class="blog-desc">
                                    <h5 class="blog-title">
                                        <a href="single-blog.html">{{trans('front/home.shop_in_fama')}}</a></h5>
                                    <p>{{trans('front/about_us.about_us_2')}}</p>
                                    <div class="read-more">
                                        <a href="{{route('about-us')}}">{{trans('front/home.read_more')}}...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- blog-item end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- PRODUCT TAB SECTION END -->

        <!-- BLOG SECTION START -->
        <div class="blog-section-2 pt-60 pb-30">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-left mb-40">
                            <h2 class="uppercase">{{trans('front/contacts.our_location')}}</h2>
                            <h6>{{trans('front/contacts.we_located')}}</h6>
                        </div>
                    </div>
                </div>
                <!-- GOOGLE MAP SECTION START -->
                <div class="google-map-section mb-40">
                    <div class="container-fluid" style="padding: 0px">
                        <div class="google-map">
                            <div id="googleMap"></div>
                        </div>
                    </div>
                </div>
                <!-- GOOGLE MAP SECTION END -->
            </div>
        </div>
        <!-- BLOG SECTION END -->

    </section>
    <!-- END PAGE CONTENT -->

@endsection

@section('scripts')

    <!-- Javascript files -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAq-_CTK9RLATxvkPnQ76HLIreXsPVvaHM"></script>
    <script src="{{asset('assets/front/js/map.js')}}"></script>

@endsection