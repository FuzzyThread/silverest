@extends('layouts.front')

@section('title',"Silverest | Product")

@section('styles')

    <!-- Discount -->
    <style>
        #cancel_search:hover {
            color: #1a237e;
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .qtybutton {
            cursor: default;
        }

        .widget-color li span {
            border-radius: 50%;
            content: "";
            height: 12px;
            left: 0;
            margin-top: -6px;
            position: absolute;
            top: 50%;
            width: 12px;
        }

        .widget-color ul li::before {
            background: transparent;
            border-radius: 50%;
            content: "";
            height: 12px;
            left: 0;
            margin-top: -6px;
            position: absolute;
            top: 50%;
            width: 12px;
        }

        a.button.button-black span, a.button span {
            z-index: 0;
        }

        #zoom_03 {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.25);
        }

        .p-c a.active img {
            border: none;
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.35);
        }

        .fancybox-wrap.fancybox-desktop.fancybox-type-image.fancybox-opened {
            z-index: 100000;
        }

        .slick-arrow-2 .arrow-next.slick-arrow, .slick-arrow-2 .arrow-prev.slick-arrow {
            border-radius: 0px;
        }

        .product-item {
            margin-top: 5px;
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.15);
        }

        .product-item:hover {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.25);
        }
    </style>

    <!-- Tags -->
    <style>
        ul.tags-list li {
            display: inline-block;
            margin-bottom: 12px;
            cursor: default;
        }

        ul.tags-list li a {
            border: 1px solid #1a237e;
            padding: 5px 10px;
            margin-right: 5px;
            margin-bottom: 10px;
            cursor: default;
        }

        .color-title.product {
            margin-right: 40px;
        }

        li.active a {
            border-bottom: 1px solid #1a237e !important;
        }

        .s-price-box.product {
            border-top: none;
            padding: 0;
        }

        .discount_list_view {
            margin-left: 30px;
            border: 2px solid #1a237e;
            color: #1a237e;
            padding: 5px 10px;
        }

        .jq-ry-group {
            z-index: 0 !important;
        }

        .product-rating {
            margin: 0;
            margin-left: 20px;
            margin-top: 3px;
        }

        #review_rating {
            margin-bottom: 8px;
        }

        #reviewModal button.close span {
            border: 1px solid #909295;
            border-radius: 60px;
            color: #909295;
            display: block;
            height: 30px;
            line-height: 25px;
            text-align: center;
            width: 30px;
        }

        #reviewModal .modal-header {
            padding-left: 25px;
            padding-top: 30px;
            padding-right: 15px;
            padding-bottom: 8px;
            border-bottom: none;
        }

        #reviewModal {
            margin-top: 60px;
        }

        .shop-pagination {
            margin-top: 40px;
        }

        .shop-pagination.box-shadow {
            box-shadow: none;
            border: 1px solid #d2d2d2;
        }

        .text-uppercase {
            z-index: 0;
        }

        .product-gem img {
            height: 42px;
            width: 42px;
            padding: 0.1rem;
            border-radius: 50px;
            margin-left: 6px;
            margin-bottom: 10px;
        }
    </style>

    <!-- Sizes List -->
    <style>
        .sizes-list.product ul {
            list-style-type: none;
        }

        .sizes-list.product li {
            display: inline-block;
        }

        .sizes-list.product input[type="radio"][id^="size_"] {
            display: none;
        }

        .sizes-list.product label:hover {
            border-color: #1a237e;
        }

        .sizes-list.product label {
            border: 1px solid #dddddd;
            padding: 1px 11px;
            border-width: 2px;
            color: #000;
            font-family: arial;
            display: block;
            position: relative;
            cursor: pointer;
            width: 54px;
            height: 30px;
            margin-right: 6px;
            margin-bottom: 0px;
        }

        .sizes-list.product label:before {
            background-color: white;
            color: white;
            content: " ";
            display: block;
            border-radius: 50%;
            border: 1px solid grey;
            position: absolute;
            top: -5px;
            left: -5px;
            width: 25px;
            height: 25px;
            text-align: center;
            line-height: 28px;
            transition-duration: 0.4s;
            transform: scale(0);
        }

        .sizes-list.product label span {
            height: 94px;
            width: 94px;
            transition-duration: 0.2s;
            transform-origin: 50% 50%;
        }

        .sizes-list.product :checked + label {
            border-color: #1a237e;
        }

        .sizes-list.product :checked + label:before {
            /*content: "✓";*/
            /*background-color: grey;*/
            /*transform: scale(1);*/
        }

        .sizes-list.product :checked + label span {
            /*transform: scale(0.9);*/
            /*box-shadow: 0 0 1px #333;*/
            /*z-index: -1;*/
        }

        .sizes-list.product ul li {
            display: inline-block;
        }
    </style>

    <link rel="stylesheet" href="{{asset('assets/front/css/jquery.rateyo.min.css')}}">

@endsection

@section('content')

    <!-- Start page content -->
    <section id="page-content" class="page-wrapper">

        <!-- SHOP SECTION START -->
        <div class="shop-section mb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <!-- single-product-area start -->
                        <div class="single-product-area mb-50">
                            <div class="row">
                                <!-- imgs-zoom-area start -->
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <div class="imgs-zoom-area">
                                        @if($product->photos->count() > 0)
                                            <img id="zoom_03" src="{{$product->photos->first()->getPhotoPath()}}" alt="{{$product->photos->first()->id}}" data-zoom-image="{{$product->photos->first()->getPhotoPath()}}" style="width: 100%;height: auto" alt="">
                                        @else
                                            <img id="zoom_03" src="{{asset('img/product/6.jpg')}}" data-zoom-image="{{asset('img/product/6.jpg')}}" alt="">
                                        @endif
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div id="gallery_01" class="carousel-btn slick-arrow-3 mt-30">
                                                    @foreach ($product->photos as $photo)
                                                        @if($loop->first)
                                                            <div class="p-c">
                                                                <a href="#" data-image="{{$photo->getPhotoPath()}}" data-zoom-image="{{$photo->getPhotoPath()}}" class="active">
                                                                    <img class="zoom_03" src="{{$photo->getPhotoPath()}}" alt="{{$photo->id}}" style="width: 100%;height: auto" alt="">
                                                                </a>
                                                            </div>
                                                        @else
                                                            <div class="p-c">
                                                                <a href="#" data-image="{{$photo->getPhotoPath()}}" data-zoom-image="{{$photo->getPhotoPath()}}">
                                                                    <img class="zoom_03" src="{{$photo->getPhotoPath()}}" alt="{{$photo->id}}" style="width: 100%;height: auto" alt="">
                                                                </a>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                    <div class="p-c">
                                                        <a href="#" data-image="{{asset('img/product/4.jpg')}}" data-zoom-image="{{asset('img/product/4.jpg')}}">
                                                            <img class="zoom_03" src="{{asset('img/product/4.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="gallery_01" class="carousel-btn slick-arrow-3 mt-30">

                                            </div>
                                        </div>
                                    </div>
                                    {{--</div>--}}
                                </div>
                                <!-- imgs-zoom-area end -->
                                <!-- single-product-info start -->
                                <div class="col-md-1 col-sm-1 col-xs-12"></div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="single-product-info">
                                        <h3 class="text-black-1 clearfix">
                                            <div class="f-left">{{$product->name}}</div>
                                            <div class="pro-rating product-rating f-left" data-rating="{{$product->getRating()}}" style="font-size: 16px"></div>
                                            @if(Auth::check())
                                                @role('superadministrator|administrator|editor')
                                                <div class="action-button f-right" style="margin-right: 20px">
                                                    <li>
                                                        <a class="cart_product tippy" title="{{trans('front/products.edit_product')}}" href="{{route('products.edit',$product->id)}}"><i class="zmdi zmdi-edit"></i></a>
                                                    </li>
                                                </div>
                                                @endrole
                                            @endif
                                        </h3>
                                        <h6 class="brand-name-2 ">{{trans('front/product.code')}} {{$product->code}}</h6>
                                        <hr>
                                        <!-- single-pro-color-rating -->
                                        <div class="single-pro-color-rating clearfix">
                                            <div class="sin-pro-color f-left">
                                                <p class="color-title product f-left">{{trans('front/product.price')}} </p>
                                                <div class="widget-color images f-left">
                                                    <h3>
                                                        <div class="s-price-box product">
                                                            @if($product->getDiscountPrice())
                                                                <span class="new-price">&euro; {{number_format ($product->getDiscountPrice(),2)}}</span>
                                                                <span class="old-price">&euro; {{number_format ($product->price,2)}}</span>
                                                                <span class="discount_list_view">- {{$product->discount_rate}} %</span>
                                                            @else
                                                                <span class="new-price">&euro; {{number_format ($product->price,2)}}</span>
                                                            @endif
                                                        </div>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="f-right" style="margin-right: 18px;margin-top: -3px">
                                                {!! Form::open(['method'=>'POST','route'=>['account.wishlist.change']]) !!}
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <button class="cart_wishlist {{(Auth::check() && Auth::user()->wishlists->contains($product->id))?'active':''}} tippy" title="Add to Wishlist" data-product="{{$product->id}}">
                                                    <i class="zmdi zmdi-favorite"></i>
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                        <hr>
                                        <!-- single-pro-color-rating -->
                                        <div class="single-pro-color-rating clearfix">
                                            <div class="sin-pro-color f-left">
                                                <p class="color-title product f-left">{{trans('front/product.material')}} </p>
                                                <div class="widget-color images f-left">
                                                    <div class="s-price-box product">
                                                        <span class="new-price">
                                                            {{str_limit($product->material->name,20)}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($product->gems->count() > 0)
                                                <div class="f-right" style="margin-right: 15px;margin-top: -3px">
                                                    <p class="color-title product f-left" style="margin-top: 4px;margin-right: 0"> {{trans('front/product.gem')}} </p>
                                                    <div class="widget-color images f-left">
                                                        <div class="s-price-box product">
                                                            <ul class="product-gem" style="    margin-bottom: -4px;">
                                                                @foreach($product->gems->take(4) as $gem)
                                                                    <img src="{{$gem->image}}" alt="{{$gem->name}}" class="img-thumbnail tippy-gem" title="{{$gem->name}}" style="margin-bottom: 0px;height: 35px; width: 35px;">
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>

                                        <hr>
                                        <!-- single-pro-color-rating -->
                                        <div class="single-pro-color-rating clearfix">
                                            <div class="sin-pro-color f-left">
                                                <p class="color-title product f-left">{{trans('front/product.weight')}} </p>
                                                <div class="widget-color images f-left">
                                                    <div class="s-price-box product">
                                                        <span class="new-price">
                                                            {{$product->weight}} {{trans('front/product.grams')}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="f-right" style="margin-right: 18px;margin-top: -3px">

                                            </div>
                                        </div>

                                        {!! Form::open(['method'=>'POST','route'=>'account.cart.add','class'=>'form-horizontal']) !!}

                                        <input type="hidden" name="product_id" value="{{$product->id}}">

                                        @if($sizes->count() > 0)
                                        <!-- hr -->
                                            <hr>
                                            <!-- plus-minus-pro-action -->
                                            <div class="single-pro-color-rating clearfix">
                                                <div class="sin-pro-color f-left">
                                                    <p class="color-title product f-left" style="margin-right: 40px">{{trans('front/product.sizes')}} </p>
                                                    <div class="sizes-list product f-left">
                                                        <ul>
                                                            @foreach($sizes as $size)
                                                                <li>
                                                                    <input type="radio" name="size" id="size_{{$size->id}}" value="{{$size->value}}"/>
                                                                    <label for="size_{{$size->id}}"><span>{{ ( ($size->value < 10)||(strlen(strval($size->value))) < 3 )  ? '&nbsp;'.$size->value : $size->value}}</span></label>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- plus-minus-pro-action end -->

                                        @endif

                                        <hr>
                                        <!-- plus-minus-pro-action -->
                                        <div class="plus-minus-pro-action clearfix">
                                            <div class="sin-plus-minus f-left clearfix">
                                                <p class="color-title product f-left">{{trans('front/product.q_ty')}} </p>
                                                <div class="cart-plus-minus f-left">
                                                    <input type="text" name="quantity" value="1" min="0" max="{{$product->quantity}}" class="cart-plus-minus-box">
                                                </div>
                                            </div>
                                            <div class="sin-pro-action f-right">
                                                <div class="f-right" style="margin-bottom: -8px">
                                                    <button type="submit" href="#" class="button extra-small button-black" tabindex="-1" style="z-index: 0;background-color: #1a237e">
                                                        <span class="text-uppercase" style="padding: 4px 30px">{{trans('front/product.buy_now')}}</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}

                                    <!-- plus-minus-pro-action end -->
                                        <!-- hr -->
                                        <hr>
                                        <div class="single-product-tab">
                                            <ul class="reviews-tab mb-20">
                                                <li class="active">
                                                    <a href="#delivery" data-toggle="tab">{{trans('front/product.delivery')}}</a>
                                                </li>
                                                <li>
                                                    <a href="#reviews" data-toggle="tab">{{trans('front/product.reviews')}} ( {{$reviews->count()}} )</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="delivery">
                                                    <!-- reviews-tab-desc -->
                                                    <hr>
                                                    <div class="reviews-tab-desc">
                                                        <p>{{trans('front/delivery.delivery_1')}}</p>
                                                        <p>{{trans('front/delivery.delivery_2')}}</p>
                                                        <p>{{trans('front/delivery.delivery_3')}}</p>
                                                        <p style="text-align: justify">{{trans('front/delivery.delivery_4')}}</p>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="reviews">
                                                    <!-- reviews-tab-desc -->
                                                    <div class="reviews-tab-desc">
                                                        @if($reviews && $reviews->count() > 0)
                                                            @foreach($reviews as $review)
                                                                <hr>
                                                                <!-- single comments -->
                                                                <div class="media mt-30">
                                                                    <div class="media-body">
                                                                        <div class="clearfix">
                                                                            <div class="name-commenter pull-left">
                                                                                <h6 class="media-heading">
                                                                                    <a href="#">{{$review->user->first_name}}</a>
                                                                                </h6>
                                                                                <p class="mb-10">Published at {{date('j F Y', strtotime($review->created_at))}}</p>
                                                                            </div>
                                                                            <div class="pull-right">
                                                                                <div class="pro-rating" data-rating="{{$review->score}}"></div>
                                                                            </div>
                                                                        </div>
                                                                        <p class="mb-0" style="color: #777676;font-size: 13px ">{{$review->body}}</p>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            {{$reviews->links('vendor.pagination.front')}}
                                                        @else
                                                            <hr>
                                                            <!-- single comments -->
                                                            <div class="media mt-30">
                                                                <div class="media-body">
                                                                    <div class="clearfix">
                                                                        <div class="name-commenter pull-left">
                                                                            <h1 class="media-heading" style="font-size: 16px;margin-bottom: 0">
                                                                                {{trans('front/product.no_reviews')}}
                                                                            </h1>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @if(!$already_reviewed)
                                                            <hr>
                                                            <a data-toggle="modal" data-target="#reviewModal" class="f-right button extra-small " style="    margin-right: 10px;">
                                                                <span class="text-uppercase" style="padding: 4px 30px">{{trans('front/product.write_review')}}</span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  hr -->

                                        <!-- single-product-price -->
                                    </div>
                                </div>
                                <!-- single-product-info end -->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- hr -->
                                    <hr>
                                    <div class="single-product-tab">
                                        <ul class="reviews-tab mb-20">
                                            <li><a href="#tags" data-toggle="tab">{{trans('front/product.tags')}}</a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="tags">
                                                <ul class="tags-list">
                                                    @foreach($product_tags as $product_tag)
                                                        <li>
                                                            <a>{{$product_tag->name}}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  hr -->
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <!-- single-product-area end -->
                    </div>

                    @if($set_products && $set_products->count() > 0)
                        <div class="related-product-area" style="margin-bottom: 50px">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title text-left mb-40">
                                        <h2 class="uppercase">{{trans('front/product.set')}}</h2>
                                        <h6>{{trans('front/product.set_description')}}</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="active-related-product slick-arrow-2">
                                @foreach($set_products as $set_product)
                                    <!-- product-item start -->
                                        <div class="col-xs-12">
                                            <div class="product-item">
                                                <div class="product-img">
                                                    <a href="{{route('catalogue.product',$set_product->id)}}">
                                                        <img src="{{ $set_product->photos()->count() > 0 ? $set_product->photos->first()->getPreviewPath() : ''}}" alt=""/>
                                                    </a>
                                                </div>
                                                <div class="product-info">
                                                    <h6 class="product-title">
                                                        <a href="single-product.html">{{$set_product->name}}</a>
                                                    </h6>
                                                    <div class="pro-rating" data-rating="{{$set_product->getRating()}}"></div>
                                                    <h3 class="pro-price">&euro; {{$set_product->price}}</h3>
                                                    <ul class="action-button">
                                                        <li>
                                                            {!! Form::open(['method'=>'POST','route'=>['account.wishlist.change']]) !!}
                                                            <input type="hidden" name="product_id" value="{{$set_product->id}}">
                                                            <button class="cart_wishlist {{(Auth::check() && Auth::user()->wishlists->contains($set_product->id))?'active':''}} tippy" title="{{trans('front/product.add_to_wishlist')}}" data-product="{{$set_product->id}}">
                                                                <i class="zmdi zmdi-favorite"></i>
                                                            </button>
                                                            {!! Form::close() !!}
                                                        </li>
                                                        <li>
                                                            <a class="cart_product tippy" title="{{trans('front/product.add_to_cart')}}" data-toggle="modal" data-target="#productModal" data-product="{{$set_product->id}}"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-item end -->
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="related-product-area">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title text-left mb-40">
                                    <h2 class="uppercase">{{trans('front/product.we_recommend')}}</h2>
                                    <h6>{{trans('front/product.we_recommend_description')}}.</h6>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="active-related-product slick-arrow-2">
                            @foreach($related_products as $related_product)
                                <!-- product-item start -->
                                    <div class="col-xs-12">
                                        <div class="product-item">
                                            <div class="product-img">
                                                <a href="{{route('catalogue.product',$related_product->id)}}">
                                                    <img src="{{$related_product->photos()->count() > 0 ? $related_product->photos->first()->getPreviewPath() : ''}}" alt=""/>
                                                </a>
                                            </div>
                                            <div class="product-info">
                                                <h6 class="product-title">
                                                    <a href="single-product.html">{{$related_product->name}}</a>
                                                </h6>

                                                <div class="pro-rating" data-rating="{{$related_product->getRating()}}"></div>

                                                <h3 class="pro-price">&euro; {{$related_product->price}}</h3>
                                                <ul class="action-button">
                                                    <li>
                                                        {!! Form::open(['method'=>'POST','route'=>['account.wishlist.change']]) !!}
                                                        <input type="hidden" name="product_id" value="{{$related_product->id}}">
                                                        <button class="cart_wishlist {{(Auth::check() && Auth::user()->wishlists->contains($related_product->id))?'active':''}} tippy" title="Add to Wishlist" data-product="{{$related_product->id}}">
                                                            <i class="zmdi zmdi-favorite"></i>
                                                        </button>
                                                        {!! Form::close() !!}
                                                    </li>
                                                    <li>
                                                        <a class="cart_product tippy" title="Add to Cart" data-toggle="modal" data-target="#productModal" data-product="{{$related_product->id}}"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- product-item end -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- SHOP SECTION END -->

    </section>
    <!-- End page content -->

    <div id="quickview-wrapper">
        <!-- Modal -->
        <div class="modal fade" id="reviewModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="widget-title f-left" style="font-size: 16px;    margin-bottom: 0px;">{{trans('front/product.write_review')}}</h2>
                        <button type="button f-right" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -15px">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="clearfix">
                            <div class="media mt-30" style="padding: 0px 20px;  ">
                                {!! Form::open(['method'=>'POST','route'=>'account.review.publish','class'=>'form-horizontal']) !!}
                                <div class="form-group">
                                    <div id="review_rating"></div>
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Review</label>
                                    <textarea name="review" placeholder="{{trans('front/product.write_review')}}..."></textarea>
                                    @if ($errors->has('review'))
                                        <span class="help-block"><strong>{{ $errors->first('review') }}</strong></span>
                                    @endif
                                </div>
                                <div class="form-group" style="margin-bottom: 5px">
                                    <div class="row">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <input type="hidden" name="score">
                                            <button type="submit" href="#" class="submit-btn-1 mt-20 btn-hover-1 f-right" tabindex="-1" style="margin-top: 40px;;margin-right: 0px;">
                                                <span class="text-uppercase">{{trans('front/product.publish')}}</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                        </div><!-- .modal-product -->
                    </div><!-- .modal-body -->
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div>
        <!-- END Modal -->
    </div>

@endsection

@section('scripts')

    <!-- Javascript files -->
    <script src="{{asset('assets/front/js/jquery.rateyo.min.js')}}"></script>

    <!-- Rating -->
    <script>
        $('#review_rating').rateYo({
            starWidth: "25px",
            halfStar: true,
            spacing: "5px",
            ratedFill: "#1a237e",
            onChange: function (rating, rateYoInstance) {
                $("input[name=score]").val(rating);
            }
        });
    </script>

    <!-- Quantity -->
    <script>
        var maxValue = parseInt({{$product->quantity}});

        /* ********************************************
		   Cart Plus Minus Button
	    ******************************************** */
        $(".cart-plus-minus").prepend('<div class="dec qtybutton">-</div>');
        $(".cart-plus-minus").append('<div class="inc qtybutton">+</div>');
        $(".qtybutton").on("click", function () {
            var $button = $(this);
            var oldValue = $button.parent().find("input").val();
            if ($button.text() == "+") {
                if (oldValue < maxValue) {
                    var newVal = parseFloat(oldValue) + 1;
                } else {
                    var newVal = parseFloat(oldValue);
                }
            }
            else {
                // Don't allow decrementing below zero
                if (oldValue > 0) {
                    var newVal = parseFloat(oldValue) - 1;
                }
                else {
                    newVal = 0;
                }
            }
            $button.parent().find("input").val(newVal);
        });
    </script>

@endsection
