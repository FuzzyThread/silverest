<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--  This file has been downloaded from bootdey.com    @bootdey on twitter -->
    <!--  All snippets are MIT license http://bootdey.com/license -->
    <!--
    	The codes are free, but we require linking to our web site.
    	Why to Link?
    	A true story: one girl didn't set a link and had no decent date for two years, and another guy set a link and got a top ranking in Google!
    	Where to Put the Link?
    	home, about, credits... or in a good page that you want
    	THANK YOU MY FRIEND!
    -->
    <title>simple invoice receipt email template - Bootdey.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
        * {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
        }

        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6;
        }

        /* Let's make sure all tables have defaults */
        table td {
            vertical-align: top;
        }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */
        body {
            background-color: #f6f6f6;
        }

        .body-wrap {
            background-color: #f6f6f6;
            width: 100%;
        }

        .container {
            display: block !important;
            max-width: 600px !important;
            margin: 0 auto !important;
            /* makes it centered */
            clear: both !important;
        }

        .content {
            max-width: 600px;
            margin: 0 auto;
            display: block;
            padding: 20px;
        }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #fff;
            border: 1px solid #e9e9e9;
            border-radius: 3px;
        }

        .content-wrap {
            padding: 20px;
        }

        .content-block {
            padding: 0 0 20px;
        }

        .header {
            width: 100%;
            margin-bottom: 20px;
        }

        .footer {
            width: 100%;
            clear: both;
            color: #999;
            padding: 20px;
        }

        .footer a {
            color: #999;
        }

        .footer p, .footer a, .footer unsubscribe, .footer td {
            font-size: 12px;
        }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1, h2, h3 {
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            color: #1a237e;
            margin: 40px 0 0;
            line-height: 1.2;
            font-weight: 400;
        }

        h1 {
            font-size: 32px;
            font-weight: 500;
        }

        h2 {
            font-size: 24px;
        }

        h3 {
            font-size: 18px;
        }

        h4 {
            font-size: 14px;
            font-weight: 600;
        }

        p, ul, ol {
            margin-bottom: 10px;
            font-weight: normal;
        }

        p li, ul li, ol li {
            margin-left: 5px;
            list-style-position: inside;
        }

        /* -------------------------------------
            LINKS & BUTTONS
        ------------------------------------- */
        a {
            color: #1ab394;
            text-decoration: underline;
        }

        .btn-primary {
            text-decoration: none;
            color: #FFF;
            background-color: #1ab394;
            border: solid #1ab394;
            border-width: 5px 10px;
            line-height: 2;
            font-weight: bold;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 5px;
            text-transform: capitalize;
        }

        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .aligncenter {
            text-align: center;
        }

        .alignright {
            text-align: right;
        }

        .alignleft {
            text-align: left;
        }

        .clear {
            clear: both;
        }

        /* -------------------------------------
            ALERTS
            Change the class depending on warning email, good email or bad email
        ------------------------------------- */
        .alert {
            font-size: 16px;
            color: #fff;
            font-weight: 500;
            padding: 20px;
            text-align: center;
            border-radius: 3px 3px 0 0;
        }

        .order {
            margin-bottom: 20px;
            margin-top: 20px;
            font-size: 14px;
        }

        .alert a {
            color: #fff;
            text-decoration: none;
            font-weight: 500;
            font-size: 16px;
        }

        .alert.alert-warning {
            background: #f8ac59;
        }

        .alert.alert-bad {
            background: #ed5565;
        }

        .alert.alert-good {
            background: #1ab394;
        }

        /* -------------------------------------
            INVOICE
            Styles for the billing table
        ------------------------------------- */
        .invoice {
            margin: 5px auto;
            text-align: left;
            width: 95%;
        }

        .invoice td {
            padding: 5px 0;
        }

        .invoice .invoice-items {
            width: 100%;
        }

        .img-logo {
            width: 150px !important;
        }

        .img-block {
            text-align: center;
            padding: 0px !important;
        }

        .dear {
            margin: 0px !important;
        }

        .invoice .invoice-items td {
            border-top: #eee 1px solid;
        }

        .invoice .invoice-items .total td {
            border-top: 2px solid #1a237e;
            font-weight: 700;
            color: #1a237e;
        }

        .filed{
            color: #5C5959;
        }

        .total-bottom td {
            border-bottom: 2px solid #1a237e;
        }

        .thank-you {
            font-size: 16px;
        }

        h6{
            color: #1a237e;
        }

        p{
            font-size: 14px !important;
        }

        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 640px) {
            h1, h2, h3, h4 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                width: 100% !important;
            }

            .content, .content-wrap {
                padding: 5px !important;
            }

            .invoice {
                width: 100% !important;
            }
        }

    </style>
</head>
<body>
<table class="body-wrap">
    <tbody>
    <tr>
        <td class="container" width="600" style="margin-top: 20px;padding-top: 20px">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="content-wrap aligncenter">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tbody>
                                <tr>
                                    <table class="invoice">
                                        <tbody>
                                        <td class="content-block img-block">
                                            <img class="aligncenter img-logo" src="http://i66.tinypic.com/vy4bj6.jpg">
                                        </td>
                                        </tbody>
                                    </table>
                                </tr>
                                <tr>
                                    <table class="invoice">
                                        <tbody>
                                        <td class="content-block">
                                            <h3 class="dear">
                                                Hello!
                                            </h3>
                                        </td>
                                        </tbody>
                                    </table>
                                </tr>
                                <tr>
                                    <table class="invoice">
                                        <tbody>
                                        <tr>
                                            <td class="content-block alignleft">

                                                You are receiving this email because we received a password reset request for your account.<br><br>

                                                @isset($actionText)
                                                    <?php
                                                    switch ($level) {
                                                        case 'success':
                                                            $color = 'green';
                                                            break;
                                                        case 'error':
                                                            $color = 'red';
                                                            break;
                                                        default:
                                                            $color = 'blue';
                                                    }
                                                    ?>
                                                    @component('mail::button', ['url' => $actionUrl, 'color' => $color])
                                                        {{ $actionText }}
                                                    @endcomponent
                                                @endisset

                                                @isset($actionText)
                                                    @component('mail::subcopy')
                                                        If you’re having trouble clicking the "{{ $actionText }}" button, copy and paste the URL below
                                                        into your web browser: [{{ $actionUrl }}]({{ $actionUrl }})
                                                    @endcomponent
                                                @endisset
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </tr>
                                <tr>
                                    <table class="invoice">
                                        <tbody>
                                        <tr>
                                            <td class="content-block alignleft">
                                                OÜ "SILVEREST"<br>
                                                Phone (mob): 59020001<br>
                                                Fax: (35) 62620<br>
                                                Email:
                                                <a href="mailto:silverest@silverest.ee" target="_blank">silverest@silverest.ee</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td class="aligncenter content-block">
                                OÜ "SILVEREST" is a trading name of Silverest, talu "Hõbe allikas", Soldino küla, Vaivara Vald, Ida-Virumaa, 40103. Registered in Estonia. All services and goods are provided subject to our terms and conditions, copies of which are available free on request.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td></td>
    </tr>
    </tbody>
</table>

<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>

