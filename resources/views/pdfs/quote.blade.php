<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Invoice PDF</title>
    <link rel="stylesheet" href="{{asset('assets/admin/css/invoice_pdf.css')}}" media="all"/>

    <style>
        @font-face {
            font-family: "DejaVu Sans";
            font-style: normal;
            font-weight: 400;
            src: url("/fonts/DejaVuSans.ttf");
            /* IE9 Compat Modes */
            src:
                    local("DejaVu Sans"),
                    local("DejaVu Sans"),
                    url("/fonts/DejaVuSans.ttf") format("truetype");
        }
        body {
            font-family: "DejaVu Sans";
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo">
        <img src="{{asset('assets/admin/images/logo.jpg')}}">
    </div>
    <div id="company">
        <h2 class="name">Silverest</h2>
        <div>Narva Tallinna mnt 19c FAMA KESKUS, Ida-Virumaa, 20303, Estonia</div>
        <div>(372) 59020001</div>
        <div>
            <a href="mailto:silverest@silverest.ee?Subject=Order:{{$order->id}}" target="_blank">silverest@silverest.ee</a>
        </div>
    </div>
</header>
<main>
    <div id="details" class="clearfix">
        <div id="client">
            <div class="to">{{trans('pdfs/invoice.invoice_to')}}:</div>
            <h2 class="name">{{$order->delivery ? $order->delivery->customer_name : ''}}</h2>
            <div class="address">{{$order->delivery ? $order->delivery->customer_address : ''}}</div>
            <div class="email"><a>{{$order->delivery ? $order->delivery->customer_email : ''}}</a></div>
        </div>
        <div id="invoice">
            <h1>{{trans('pdfs/invoice.invoice')}} {{$order->id}}</h1>
            <div class="date">{{trans('pdfs/invoice.date_of_invoice')}}: {{date('j F Y', strtotime($order->created_at))}}</div>
        </div>
    </div>
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th class="no">#</th>
            <th class="desc">{{trans('pdfs/invoice.description')}}</th>
            <th class="unit">{{trans('pdfs/invoice.unit_price')}}</th>
            <th class="qty">{{trans('pdfs/invoice.quantity')}}</th>
            <th class="total">{{trans('pdfs/invoice.total')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($order->items as $item)
            @php
                $product = \App\Product::withTrashed()->find($item->product_id);
            @endphp
            @if($product)
                <tr>
                    <td class="no">{{($loop->index + 1)}}</td>
                    <td class="desc">
                        <h3>{{$product->getTranslation('name','en')}} ({{$product->code}})</h3><br>
                        @if($item->size)
                            <span>{{trans('pdfs/invoice.size')}} <strong>: &nbsp;</strong></span>{{$item->size}}
                            <br>
                        @endif
                    </td>
                    <td class="unit">
                        @if($item->discount_price > 0)
                            <span class="new-price">{{number_format ($item->discount_price,2)}}&euro;</span>
                        @else
                            <span class="new-price">{{number_format ($product->price,2)}}&euro;</span>
                        @endif
                    </td>
                    <td class="qty">{{$item->quantity}}</td>
                    <td class="total">
                        @if($item->discount_price > 0)
                            <span class="new-price">{{number_format ($item->discount_price * $item->quantity,2)}}&euro;</span>
                        @else
                            <span class="new-price">{{number_format ($product->price * $item->quantity,2)}}&euro;</span>
                        @endif
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">{{trans('pdfs/invoice.cart_total')}}</td>
            <td>{{number_format(($order->payment->cart_total),2)}}&euro;</td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">{{trans('pdfs/invoice.shipping_total')}}</td>
            <td>{{number_format(($order->payment->shipping_total),2)}}&euro;</td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">{{trans('pdfs/invoice.total')}}</td>
            <td>{{number_format(($order->payment->subtotal),2)}}&euro;</td>
        </tr>
        </tfoot>
    </table>
    <div id="thanks">{{trans('pdfs/invoice.thank_you')}}!</div>
    <div id="notices">
        <div>{{trans('pdfs/invoice.notice')}}:</div>
        <div class="notice">{{trans('pdfs/invoice.please_send')}}:</div>
        <div class="notice">IBAN:
            <span style="color: #1a237e;">EE722200221024580301</span> &nbsp;&nbsp;&nbsp;&nbsp; Pank:
            <span style="color: #1a237e;">Swedbank</span> &nbsp;&nbsp;&nbsp;&nbsp; SWIFT/BIC:
            <span style="color: #1a237e;">HABAEE2X</span></div>
        <div class="notice">{{trans('pdfs/invoice.order_will_be_dispatched')}}</div>
    </div>
</main>
<footer>
    {{trans('pdfs/invoice.invoice_was_created')}}
</footer>
</body>
</html>
