@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('photos.index')}}">Photos</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="margin-bottom: 0px">PHOTO CRUD</h1>
            </header>
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        {!! Form::model($photo,['method'=>'PATCH','route'=>['photos.update',$photo->id],'files'=>true,'class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>Photo Details</h1>
                        </div>

                        <div class="card-block row">
                            <div class="col-sm-9">
                                <div class="form-group row" style="margin: 0">
                                    <label class="col-sm-3">Name</label>
                                    <div class="col-sm-9">
                                        <input name="name" type="text" placeholder="Name" value="{{$photo->name}}" class="form-control form-control-success">
                                    </div>
                                </div>
                                <div class="form-group row" style="margin: 0;">
                                    <label class="col-sm-3">Image</label>
                                    <div class="col-sm-9">
                                        {!! Form::file('image',null,['class'=>'filestyle']) !!}
                                    </div>
                                </div>
                                <div class="form-group row" style="margin-top: 30px;">
                                    <div class="col-sm-10">
                                        <input type="submit" value="Update Photo" class="btn btn-outline-primary" style="margin-bottom: -60px;margin-left: 12px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <img src="{{$photo->getPreviewPath()}}" style="height: 135px; width: 135px; margin-right: 15px;float: right;" alt="" class="img-thumbnail">
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <script src="{{asset('assets/admin/js/bootstrap-filestyle.min.js')}}"></script>

    <script>
        $(":file").filestyle({placeholder: "{{$photo->getPhotoPath()}}"});
    </script>

@endsection