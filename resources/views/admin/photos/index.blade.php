@extends('layouts.admin')

@section('title',"Manage | Photos")

@section('styles')

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/lightbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/dropzone.min.css')}}">

    <style>
        .dropzone {
            border: 1px solid rgba(0, 0, 0, 0.3);
        }

        .dropzone .dz-message {
            text-align: center;
            margin: 3em 0;
        }

        .dropzone .dz-message span {
            font-size: 20px;
            color: rgba(4, 49, 128, 0.25);
        }

        .stripe-1 {
            color: white;
            background-color: #f3f3f3;
            border: 2px dashed #3c4579;
        }

        .dropzone .dz-preview.dz-image-preview {
            background-color: transparent;
        }

        .dropdown-menu a {
            text-decoration: none;
            color: gray;
            padding: 5px 10px;
        }

        .dropdown-menu a:hover {
            text-decoration: none;
            color: #000000;
        }

        .btn-primary {
            background-color: #0275d8;
            border-color: #0275d8;
        }

    </style>

    <style>
        .sweet-alert button{
            margin: 26px 5px 15px 5px !important;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center;
        }

        @media (min-width: 1600px) {
            .col-not-for-mac{
                -webkit-box-flex: 0;
                -webkit-flex: 0 0 16.666666666666666667% !important;
                -ms-flex: 0 0 16.666666666666666667% !important;
                flex: 0 0 16.666666666666666667% !important;
                max-width: 16.666666666666666667% !important;
            }
        }

    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('admin/orders.home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('admin/layout.photos')}}</li>
            </ul>
        </div>
    </div>

    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="float: left;margin-bottom: 0px">{{trans('admin/layout.photos_crud')}}</h1>
                <h2 class="counter">{{trans('admin/layout.photos_number')}} :
                    <span style="font-size: 17px;color: #0275d8;">{{\App\Photo::all()->count()}}</span>
                </h2>
            </header>

            <div class="row" style="margin-top: 34px">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1>{{trans('admin/layout.photos_upload')}}:</h1>
                        </div>
                        <div class="card-block" style="text-align: center">
                            {!! Form::open(['method'=>'POST', 'route'=>'photos.store','files'=>true, 'class'=>'dropzone stripe-1', 'id'=>'uploadImages']) !!}
                            <div class="dz-message" data-dz-message>
                                <span><i class="fa fa-arrow-circle-o-down  fa-3x" aria-hidden="true"></i><br>Drop files here to upload &nbsp;</span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block" style="text-align: center">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <!-- Sort -->
                                    <div class="btn-group" style="float: left">
                                        <a href="{{route('photos.index', ['search'=>request('search')])}}"
                                           class="btn btn-primary">{{trans('admin/orders.sort')}}</a>
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 0px;background-color: #cbcbcb;">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{route('photos.index', ['search'=>request('search'),'sort'=>'asc'])}}">{{trans('admin/orders.asc')}}</a>
                                            </li>
                                            <li>
                                                <a href="{{route('photos.index', ['search'=>request('search'),'sort'=>'desc'])}}">{{trans('admin/orders.desc')}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <div class="text-right">
                                        {!! Form::open(['method'=>'GET','route'=>'photos.index']) !!}
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="search" placeholder="{{trans('admin/layout.photo_id')}}"
                                                   value="{{isset($search_field) ? $search_field : ''}}">
                                            <a href="{{route('photos.index')}}" class="btn btn-secondary" style="height: 38px;border-left: none;display: none"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            <span class="input-group-btn">
                                                   {!! Form::submit('Go!',['class'=>'btn btn-primary']) !!}
                                            </span>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="gallery_box">
                @include('partials.admin._photo_gallery')
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <!-- Javascript Files -->
    <script src="{{asset('assets/admin/js/lightbox.js')}}"></script>
    <script src="{{asset('assets/admin/js/dropzone.min.js')}}"></script>

    <script>
        Dropzone.options.uploadImages = {
            maxFilesize: 5,
            acceptedFiles: ".png,.jpg,.jpeg",
            success: function (file, response) {
                if (file.status == 'success') {
                    handleDropzoneFileUpload.handleSuccess(response);
                } else {
                    handleDropzoneFileUpload.handleError(response);
                }
            },
            init: function () {
                this.on("complete", function (file) {
                    this.removeFile(file);
                });
            }
        };

        var handleDropzoneFileUpload = {

            handleError: function (response) {
                console.log(response);
            },

            handleSuccess: function (response) {
                var gallery = $('#gallery_box');
                gallery.html(response);
            }
        };

        if($("input[name = 'search']").val().length > 0){
            $(".btn-secondary").show();
        }

    </script>

    <script>
        $(".delete_photo").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this photo!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Photo Deleted!',
                            text: 'Photo is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your photo is safe", "error");
                    }
                }
            );
        });
    </script>

@endsection
