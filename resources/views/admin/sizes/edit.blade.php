@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('sizes.index')}}">Sizes</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1>SIZE CRUD</h1>
            </header>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        {!! Form::model($size,['method'=>'PATCH','route'=>['sizes.update',$size->id],'class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>Size Details</h1>
                        </div>

                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Value</label>
                                <div class="col-sm-9">
                                    <input name="value" type="number" step=0.5 placeholder="Value" value="{{$size->value}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Update Size" class="btn btn-outline-primary">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
