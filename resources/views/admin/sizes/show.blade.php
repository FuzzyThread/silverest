@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('sizes.index')}}">Sizes</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1>SIZE CRUD</h1>
            </header>
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">Size Details</h1>
                            <a href="{{route('sizes.edit',$size->id)}}" class="btn btn-outline-primary float-right">Edit Size</a>
                        </div>
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-sm-4">Value</label>
                                <h2 class="col-sm-8">{{$size->value}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h1>Products:</h1>
                        </div>
                        <div class="card-block" style="margin-left: 20px">
                            <ul>
                                @foreach($size->products as $product)
                                    <li>{{$product->name}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

