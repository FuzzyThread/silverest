@extends('layouts.admin')

@section('title',"Manage | Reviews")

@section('styles')

    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-toggle.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/lightbox.min.css')}}">

    <style>
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }

        .android {
            margin-left: 30px;
        }

        .dropdown-menu a {
            text-decoration: none;
            color: gray;
            padding: 5px 10px;
        }

        .dropdown-menu a:hover {
            text-decoration: none;
            color: #000000;
        }

        .sweet-alert button {
            margin: 26px 5px 15px 5px !important;
        }

        a.product-code:hover {
            text-decoration: none;
        }

        #snackbar {
            margin-left: -120px;
        }


    </style>

    <style>
        .android {
            margin-right: 20px !important;
            margin-top: -3px;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }

    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item active">Reviews</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="float: left;margin-bottom: 0px">REVIEW CRUD</h1>
                <h2 class="counter">Review Number :
                    <span style="font-size: 17px;color: #0275d8;">{{\App\Review::all()->count()}}</span>
                </h2>
            </header>

            <div class="row" style="margin-top: 37px">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block" style="text-align: center">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <!-- Sort -->
                                    <div class="btn-group" style="float: left">
                                        <a href="{{route('reviews.index', ['search'=>request('search')])}}"
                                           class="btn btn-primary">Sort by Date</a>
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 0px;    background-color: #cbcbcb;">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{route('reviews.index', ['search'=>request('search'),'sort'=>'asc'])}}">Ascending</a>
                                            </li>
                                            <li>
                                                <a href="{{route('reviews.index', ['search'=>request('search'),'sort'=>'desc'])}}">Descending</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <div class="text-right">
                                        {!! Form::open(['method'=>'GET','route'=>'reviews.index']) !!}
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="search" placeholder="Keyword"
                                                   value="{{isset($search_field) ? $search_field : ''}}">
                                            <a href="{{route('reviews.index')}}" class="btn btn-secondary" style="height: 38px;border-left: none;display: none"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            <span class="input-group-btn">
                                                   {!! Form::submit('Go!',['class'=>'btn btn-primary']) !!}
                                            </span>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($reviews as $review)
                    <div class="col-lg-12" style="margin-bottom: 30px">
                        <div class="card">
                            <div class="card-header" style="padding: 0px">
                                <div class="row">
                                    <div class="col-md-1" style="margin: auto auto;">
                                        @if($review->product->photos->count() > 0)
                                            <img src="{{$review->product->photos->first()->getPreviewPath()}}" style="height: 50px; width: 50px; border-radius: 0px;border: none;border-right: 1px solid #f2f2f2;border-bottom: 1px solid #f2f2f2;" alt="" class="img-thumbnail">
                                        @else
                                            <img src="https://www.greatmats.com/images/placeholder-all.png" style="height: 50px; width: 50px; border-radius: 0px;border: none;border-right: 1px solid #f2f2f2;border-bottom: 1px solid #f2f2f2;" alt="" class="img-thumbnail">
                                        @endif
                                    </div>
                                    <div class="col-md-2" style="margin: auto 0;">
                                        <div class="pro-rating" data-rating="{{$review->score}}"></div>
                                    </div>
                                    <div class="col-md-6" style="margin: auto 0;">
                                        <div class="row">
                                            <div class="col-md-5" style="margin: 0">
                                                <p style="color: #043180;margin-bottom: 0px;font-size: 16px">
                                                    {{str_limit($review->product->name,10)}}
                                                    <span style="font-size: 16px">(<a class="product-code" href="{{route('catalogue.product',$review->product->id)}}" target="_blank">{{$review->product->code}}</a>)</span>
                                                </p>
                                            </div>
                                            <div class="col-md-4" style="margin: 0">
                                                <p style="color: #043180;margin-bottom: 0px;font-size: 16px">
                                                    <a class="product-code" href="{{route('customers.show',$review->user->id)}}" target="_blank">{{str_limit($review->user->email,22)}}</a>
                                                </p>
                                            </div>
                                            <div class="col-md-3" style="margin: 0;text-align: right">
                                                {{($review->created_at  ? $review->created_at->diffForHumans():'No Date')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="margin: auto 0;">
                                        <div style="float: right;margin-right: 35px;margin-top: 0px">
                                            <div class="row" style=" height: 24px;">
                                                <div class="col-xs-6">
                                                    <input class="toggle-event" type="checkbox" {{$review->is_visible == 1 ? 'checked':''}} value="{{$review->id}}" data-toggle="toggle" data-style="android" data-on="&nbsp;<i class='fa fa-eye fa-lg' aria-hidden='true'></i>" data-off="<i class='fa fa-eye-slash fa-lg' aria-hidden='true'></i>&nbsp;" data-width="60" data-height="20" data-size="small" data-onstyle="primary" style="    margin-right: 20px;">
                                                </div>
                                                <div class="col-xs-3" style="margin: 0;">
                                                    <a href="{{route('reviews.edit',$review->id)}}" style="margin-left: 20px;margin-right: 20px;color: #0275d8;cursor: pointer"><i class="fa fa-pencil  fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-3" style="margin: 0;margin-right: -20px">
                                                    {!! Form::open(['method'=>'DELETE','route'=>['reviews.destroy', $review->id],'class'=>'delete_review']) !!}
                                                    <button type="submit" style="border: none;background: transparent;padding: 0;outline: none;color: red;margin-right: 20px">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row" style="padding: 30px;padding-top:20px;padding-bottom: 0px">
                                    <div class="col-md-12" style="margin-bottom: 25px;">
                                        <p style="margin-top: 5px;font-size: 16px;margin-bottom: 0">{{$review->body}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div style="margin-bottom: 20px;margin-top: 20px">
                {{$reviews->links()}}
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <script src="{{asset('assets/admin/js/lightbox.js')}}"></script>
    <script src="{{asset('assets/admin/js/bootstrap-toggle.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>

    <script>
        $(function () {
            $('.toggle-event').change(function () {
                var is_visible = $(this).is(":checked");
                var product_id = $(this).val();
                var data = {_token: '{!! csrf_token() !!}', product_id: product_id, is_visible: is_visible}

                console.log(data);

                $.ajax({
                    'url': "{{route('review.visible')}}",
                    'method': 'POST',
                    'data': data,
                    success: function (msg) {
                        console.log(msg)
                    }
                });
                x
            });

            // reset button:
            if ($("input[name = 'search']").val().length > 0) {
                $(".btn-secondary").show();
            }
        })

    </script>

    <script>
        $(".delete_review").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this review!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Review Deleted!',
                            text: 'Review is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your review is safe", "error");
                    }
                }
            );
        });
    </script>

    <script>
        $('.pro-rating').each(function () {
            var rating = $(this).attr('data-rating');
            $(this).html(getStars(rating));
        });

        function getStars(rating) {
            rating = Math.round(rating * 2) / 2;
            let output = [];
            for (var i = rating; i >= 1; i--) {
                output.push('<i class="fa fa-star" aria-hidden="true" style="color: #1a237e;"></i>&nbsp;');
            }
            if (i == .5) {
                output.push('<i class="fa fa-star-half-o" aria-hidden="true" style="color: #1a237e;"></i>&nbsp;');
            }
            for (let i = (5 - rating); i >= 1; i--) {
                output.push('<i class="fa fa-star-o" aria-hidden="true" style="color: #1a237e;"></i>&nbsp;');
            }
            return output.join('');
        }
    </script>

@endsection


