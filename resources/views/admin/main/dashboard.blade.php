@extends('layouts.admin')

@section('content')

    <!-- Counts Section -->
    <section class="dashboard-counts section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="fa fa-user-plus fa-lg"></i></div>
                        <div class="name"><strong class="text-uppercase">New Clients</strong><span>Last 7 days</span>
                            <div class="count-number">{{$new_clients}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="fa fa-cubes fa-lg"></i></div>
                        <div class="name"><strong class="text-uppercase">Work Orders</strong><span>Last month</span>
                            <div class="count-number">{{$preparation}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="fa fa-comments fa-lg"></i></div>
                        <div class="name"><strong class="text-uppercase">New Reviews</strong><span>Last 2 months</span>
                            <div class="count-number">{{$new_reviews}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="fa fa-briefcase fa-lg"></i></div>
                        <div class="name"><strong class="text-uppercase">Income</strong><span>Last month</span>
                            <div class="count-number" style="font-size: 2em;margin-top: 15px;margin-left: -37px;">{{$monthly_income}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="fa fa-user fa-lg"></i></div>
                        <div class="name"><strong class="text-uppercase">All Users</strong><span>All time</span>
                            <div class="count-number">{{$all_users}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="fa fa-tasks fa-lg"></i></div>
                        <div class="name"><strong class="text-uppercase">All Orders</strong><span>All time</span>
                            <div class="count-number">{{$all_orders}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Header Section-->
    <section class="dashboard-header section-padding">
        <div class="container-fluid">
            <div class="row d-flex align-items-md-stretch">
                <!-- Pie Chart-->
                <div class="col-lg-6 col-md-6">
                    <div class="wrapper project-progress">
                        <h2 class="display h4">Orders</h2>
                        <p> Statistics based on order statuses.</p>
                        <div class="pie-chart">
                            <canvas id="pieChart"></canvas>
                        </div>
                    </div>
                </div>
                <!-- Line Chart -->
                <div class="col-lg-6 col-md-12 flex-lg-last flex-md-first align-self-baseline">
                    <div class="wrapper sales-report">
                        <h2 class="display h4">Sales marketing report</h2>
                        <p> Report based on monthly income for year {{date('Y')}}</p>
                        <div class="line-chart">
                            <canvas id="lineCahrt"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Statistics Section-->
    <section class="statistics section-padding section-no-padding-bottom" style="margin-bottom: 40px">
        <div class="container-fluid">
            <div class="row d-flex align-items-stretch">
                <div class="col-lg-4">
                    <!-- Income-->
                    <div class="wrapper income text-center">
                        <div class="icon"><i class="icon-line-chart"></i></div>
                        <div class="number">{{$overall_income}}</div>
                        <strong class="text-primary">All Income</strong>
                        <p>Income gathered from completed orders for all time</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <!-- Monthly Usage-->
                    <div class="wrapper data-usage">
                        <h2 class="display h4">System Usage</h2>
                        <div class="row d-flex align-items-center">
                            <div class="col-sm-6">
                                <div id="progress-circle" class="d-flex align-items-center justify-content-center"></div>
                            </div>
                            <div class="col-sm-6"><strong class="text-primary">{{$memory_usage}}</strong>
                                <small>Stats:</small>
                                <span>4 Gb Total</span></div>
                        </div>
                        <p>Get current total usage of the system</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <!-- User Actibity-->
                    <div class="wrapper user-activity">
                        <h2 class="display h4">User Activity</h2>
                        <div class="number">{{$online_users}}</div>
                        <h3 class="h4 display">Logged Users</h3>
                        <div class="progress">
                            <div role="progressbar" style="width: {{round(($online_users/$all_users*100),2)}}%" aria-valuenow="{{round(($online_users/$all_users*100),2)}}" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar bg-primary"></div>
                        </div>
                        <div class="page-statistics d-flex justify-content-between">
                            <div class="page-visites"><span>Total Users</span><strong>{{$all_users}}</strong></div>
                            <div class="new-visites">
                                <span>Logged users</span><strong>{{round(($online_users/$all_users*100),2)}} %</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <!-- Javascript Files -->
    <script src="{{asset('assets/admin/js/chart.min.js')}}"></script>

    <script>

        // Main Template Color
        var brandPrimary = '#33b35a';

        var jan_income = parseInt('{{$income_by_months[0]}}');
        var feb_income = parseInt('{{$income_by_months[1]}}');
        var mar_income = parseInt('{{$income_by_months[2]}}');
        var apr_income = parseInt('{{$income_by_months[3]}}');
        var may_income = parseInt('{{$income_by_months[4]}}');
        var jun_income = parseInt('{{$income_by_months[5]}}');
        var jul_income = parseInt('{{$income_by_months[6]}}');
        var aug_income = parseInt('{{$income_by_months[7]}}');
        var sep_income = parseInt('{{$income_by_months[8]}}');
        var oct_income = parseInt('{{$income_by_months[9]}}');
        var nov_income = parseInt('{{$income_by_months[10]}}');
        var dec_income = parseInt('{{$income_by_months[11]}}');

        // ------------------------------------------------------- //
        // Line Chart
        // ------------------------------------------------------ //
        var LINECHART = $('#lineCahrt');
        var myLineChart = new Chart(LINECHART, {
            type: 'line',
            options: {
                legend: {
                    display: false
                }
            },
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                datasets: [
                    {
                        label: "My First dataset",
                        fill: true,
                        lineTension: 0.3,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        borderWidth: 1,
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [jan_income, feb_income, mar_income, apr_income, may_income, jun_income, jul_income, aug_income, sep_income, oct_income, nov_income, dec_income],
                        spanGaps: false
                    }
                ]
            }
        });

        // ------------------------------------------------------- //
        // Pie Chart
        // ------------------------------------------------------ //

        var awaiting_payment = parseInt('{{$awaiting_payment}}');
        var preparation = parseInt('{{$preparation}}');
        var refunded = parseInt('{{$refunded}}');
        var dispatched = parseInt('{{$dispatched}}');
        var completed = parseInt('{{$completed}}');
        var cancelled = parseInt('{{$cancelled}}');
        var payment_declined = parseInt('{{$payment_declined}}');
        var error = parseInt('{{$error}}');

        var PIECHART = $('#pieChart');
        var myPieChart = new Chart(PIECHART, {
            type: 'doughnut',
            data: {
                labels: [
                    "Awaiting Payment",
                    "Preparation",
                    "Refunded ",
                    "Dispatched",
                    "Completed",
                    "Cancelled",
                    "Payment Declined",
                    "Error",
                ],
                datasets: [
                    {
                        data: [awaiting_payment, preparation, refunded, dispatched, completed, cancelled, payment_declined, error],
                        borderWidth: [1, 1, 1],
                        backgroundColor: [
                            "#8a008a",
                            "#003adc",
                            "#5bc0de",
                            "#f98e00",
                            "#4ac32b",
                            "#ff0000",
                            "#f27474",
                            "#888888"
                        ],
                        hoverBackgroundColor: [
                            "#8a008a",
                            "#003adc",
                            "#5bc0de",
                            "#f98e00",
                            "#4ac32b",
                            "#ff0000",
                            "#f27474",
                            "#888888"
                        ]
                    }]
            }
        });
    </script>

@endsection