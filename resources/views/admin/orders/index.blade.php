@extends('layouts.admin')

@section('title',"Manage | Orders")

@section('styles')

    <style>
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }

        .android {
            margin-left: 30px;
        }

        .dropdown-menu a {
            text-decoration: none;
            color: gray;
            padding: 5px 10px;
        }

        .dropdown-menu a:hover {
            text-decoration: none;
            color: #000000;
        }

        .sweet-alert button {
            margin: 26px 5px 15px 5px !important;
        }

        a.product-code:hover {
            text-decoration: none;
        }

        #snackbar {
            margin-left: -120px;
        }

        .status {
            content: " ";
            width: 4px;
            height: 44px;
            position: absolute;
            top: -10px;
        }

        .custom-dropdown{
            margin: 0.125rem 0px 0px -100px !important;
        }
    </style>
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-toggle.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/lightbox.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <style>
        a.link-order-status {
            text-decoration: none;
        }

        .link-order-status.dropdown-toggle::after {
            margin-left: 0;
            content: none;
            border-top: 0;
            border-right: 0;
            border-left: 0;
        }

        .dropdown-menu {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.1);
        }
    </style>

    <style>
        .product-list {
            margin-bottom: 20px;
            width: 376px;
            height: 111px;
            /*flex: 0 0 33.333333%;*/
            /*max-width: 33.333333%;*/
            padding-right: 15px;
            padding-left: 15px;
        }

        .product-thumbnail {
            border: 1px solid #dfdfdf;
            width: 100%;
            height: 107px;
        }

        .pro-thumbnail-img {
            float: left;
            width: 32%;
        }

        .pro-thumbnail-info {
            float: left;
            padding-left: 20px;
            width: 68%;
        }

        .product-title-2 {
            color: #666666;
            font-weight: 500;
            margin-top: 15px;
            overflow: hidden;
            text-overflow: ellipsis;
            text-transform: uppercase;
            white-space: nowrap;
            margin-bottom: 5px;
        }

        p.small-title {
            margin-bottom: 0px;
            font-size: 14px !important;
        }

        img {
            width: 105px;
            height: auto;
            border-right: 1px solid #dfdfdf;
        }

        .pro-thumbnail-info span {
            font-size: 14px !important;
            color: #1a237e !important;
        }
    </style>

    <style>
        .payment-details td {
            padding: 5px 0;
        }

        .td-title-2 {
            color: #999;
            font-family: roboto;
            font-weight: 500;
            text-align: right;
        }

        .td-title-1 {
            color: #999;
            font-size: 14px;
            text-align: left;
        }

        .payment-details tr {
            border-bottom: 1px solid #eee;
        }

        .order-total {
            color: #1a237e;
            font-weight: 500;
            text-align: left;
        }

        .order-total-price {
            color: #1a237e;
            font-family: roboto;
            font-weight: 700;
            text-align: right;
        }

        .payment-details {
            width: 100%;
        }
    </style>

    <style>
        .dropdown-item {
            padding-left: 20px;
            padding-right: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .dropdown-header {
            padding-left: 20px;
            padding-right: 10px;
        }

        .tracking_number {
            max-width: 200px;
            float: right;
        }

        .tracking_number input, .tracking_number input:focus {
            color: #999;
            font-family: roboto;
            font-weight: 500;
            font-size: 14px;
            text-align: center;
            outline: 0;
        }

        .tracking_number input:focus {
            border-color: #1a237e;
            box-shadow: none;
        }

        .tracking_number button, .tracking_number button:hover, .tracking_number button:focus {
            background-color: #1a237e !important;
            border-color: #1a237e !important;
            outline: 0;
            box-shadow: none !important;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('admin/orders.home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('admin/orders.orders')}}</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="float: left;margin-bottom: 0px">{{trans('admin/orders.crud')}} </h1>
                <h2 class="counter">{{trans('admin/orders.order_number')}} : <span style="font-size: 17px;color: #0275d8;">{{\App\Order::all()->count()}}</span></h2>
            </header>

            <div class="row" style="margin-top: 37px">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block" style="text-align: center">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <!-- Sort -->
                                    <div class="btn-group" style="float: left">
                                        <a href="{{route('orders.index', ['search'=>request('search')])}}"
                                           class="btn btn-primary">{{trans('admin/orders.sort')}}</a>
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 0px;    background-color: #cbcbcb;">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{route('orders.index', ['search'=>request('search'),'sort'=>'asc'])}}">{{trans('admin/orders.asc')}}</a>
                                            </li>
                                            <li>
                                                <a href="{{route('orders.index', ['search'=>request('search'),'sort'=>'desc'])}}">{{trans('admin/orders.desc')}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <div class="text-right">
                                        {!! Form::open(['method'=>'GET','route'=>'orders.index']) !!}
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="search" placeholder="{{trans('admin/orders.order_number')}}"
                                                   value="{{isset($search_field) ? $search_field : ''}}">
                                            <a href="{{route('orders.index')}}" class="btn btn-secondary" style="height: 38px;border-left: none;display: none"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            <span class="input-group-btn">
                                                   {!! Form::submit('Go!',['class'=>'btn btn-primary']) !!}
                                            </span>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="orders">
                @include('partials.admin._orders_list')
            </div>

            <div class="row" style="margin-top: 30px">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block" style="padding: 0px;display: inline-block">
                            <h2 style="margin-bottom: 20px;padding: 20px;border-bottom: 1px solid #dfdfdf">{{trans('admin/orders.status_legend')}}:</h2>
                            <div class="row" style="padding-bottom: 0px;padding-top: 10px;margin-left: 20px;display: inline-flex">
                                @foreach($statuses as $status)
                                    <div style="margin-bottom: 20px;width: 215px;height: 30px">
                                        <div class="product-thumbnail cart_thumbnail" style="border: none">
                                            <i class="fa fa-square" style="color: {{$status->value}}"></i> &nbsp;&nbsp;{{$status->name}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('scripts')

    <!-- Javascript Files -->
    <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>

    <script>
        $('.open-order').click(function () {

            var order_id = $(this).attr('value');
            var data = {_token: '{!! csrf_token() !!}', order_id: order_id}

            $.ajax({
                'url': "{{route('order.viewed')}}",
                'method': 'POST',
                'data': data,
                success: function (msg) {
                    console.log(msg)
                }
            });


            $('#order_viewed' + order_id).css('color', '#999;');

        });
    </script>

    <script>
        $("#orders").on('click', '.order_status', function (e) {

            var order_id = $(this).attr('data-order-id');
            var status_id = $(this).attr('data-status-id');
            var search = '{{request('search')}}';
            var sort = '{{request('sort')}}';
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to change status back!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Order Status Changed!',
                            text: 'Order is successfully changed!',
                            type: 'success'
                        }, function () {

                            var data = {
                                _token: '{!! csrf_token() !!}',
                                order_id: order_id,
                                status_id: status_id,
                                search: search,
                                sort: sort
                            }

                            $.ajax({
                                'url': "{{route('order.status.ajax')}}",
                                'method': 'POST',
                                'data': data,
                                success: function (ignore) {
                                    location.reload();
                                }
                            });

                        });

                    } else {
                        swal("Cancelled", "Order status remain the same", "error");
                    }
                }
            );
        });
    </script>

@endsection


