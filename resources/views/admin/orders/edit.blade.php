@extends('layouts.admin')


@section('styles')

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/admin/themes/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/themes/classic.date.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/rangeslider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-toggle.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.css">

    <!-- Dropdown Elements -->
    <style>
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }

        .dropdown-menu a {
            text-decoration: none;
            color: gray;
            padding: 5px 10px;
        }

        .dropdown-menu a:hover {
            text-decoration: none;
            color: #000000;
            font-size: 1rem;
            font-weight: bold;
            line-height: 1.25;
            color: #464a4c;
        }

        .card-header {
            background-color: #fcfcfc;
        }

        section.forms label {
            font-size: 1em;
        }

        section.forms textarea {
            font-size: 1.1em;
        }
    </style>

    <!-- Select Elements -->
    <style>
        section span {
            font-size: 0.98em !important;
            display: block;
        }

        span.select2-selection.select2-selection--multiple {
            width: 100% !important;
            border: 1px solid rgba(0, 0, 0, .15);
            border-radius: 0px;
        }

        .select2-container--default.select2-container--focus {
            border-radius: 0px !important;
            width: 100% !important;
        }

        .select2-container--open .select2-dropdown--below {
            border: 1px solid #aaa;
        }

        span.select2.select2-container.select2-container--default.select2-container--below.select2-container--focus.select2-container--open {
            width: 100% !important;
        }

        .select2-results__option[aria-selected=true] {
            display: none;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: solid #279aff 1px !important;
        }

        .select2 .select2-container .select2-container--default .select2-container--below {
            width: 100% !important;
        }

        .select2-container--default .select2-selection--single {
            border-radius: 0px !important;
            outline: none !important;
        }

        .select2-container .select2-selection--single {
            height: 38px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 5px;
            right: 10px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            padding-top: 4px;
            margin-left: 4px;
            font-size: 16px !important;
        }

        span.select2-dropdown.select2-dropdown--below {
            border-top: none !important;
        }

        .select2-container--default .select2-selection--single {
            border: 1px solid rgba(170, 170, 170, 0.46);
        }

        .select2-search__field {
            height: 25px;
        }

        span.select2-selection__choice__remove {
            margin-left: 8px;
            float: right;
        }

        .color_select span.select2-selection__choice__remove {
            padding-top: 2px;
            margin-left: 22px;
        }

        .gem_select span.select2-selection__choice__remove {
            margin-left: 30px;
        }

        li.select2-selection__choice {
            padding: 3px !important;
            padding-left: 4px !important;
            padding-right: 4px !important;
        }

        .color_select li.select2-selection__choice {
            padding: 1px !important;
            padding-left: 4px !important;
            padding-right: 4px !important;

        }
    </style>

    <!-- File Upload Select Elements -->
    <style>

        .images ul {
            list-style-type: none;
        }

        .images li {
            display: inline-block;
        }

        .images input[type="checkbox"][id^="photo_"] {
            display: none;
        }

        .images label {
            border: 1px solid #dddddd;
            padding: 10px;
            display: block;
            position: relative;
            margin: 10px;
            cursor: pointer;
        }

        .images label:before {
            background-color: white;
            color: white;
            content: " ";
            display: block;
            border-radius: 50%;
            border: 1px solid grey;
            position: absolute;
            top: -5px;
            left: -5px;
            width: 25px;
            height: 25px;
            text-align: center;
            line-height: 28px;
            transition-duration: 0.4s;
            transform: scale(0);
        }

        .images label img {
            height: 94px;
            width: 94px;
            transition-duration: 0.2s;
            transform-origin: 50% 50%;
        }

        .images :checked + label {
            border-color: #ddd;
        }

        .images :checked + label:before {
            content: "✓";
            background-color: grey;
            transform: scale(1);
        }

        .images :checked + label img {
            transform: scale(0.9);
            box-shadow: 0 0 5px #333;
            z-index: -1;
        }
    </style>

    <!-- Discount -->
    <style>
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
        }

        .add-on .input-group-btn > .btn {
            border-left-width: 0;
            left: -2px;
            border: 1px solid #cccccc;
            border-radius: 0px;
        }

        /* stop the glowing blue shadow */
        .add-on .form-control:focus {
            box-shadow: none;
            -webkit-box-shadow: none;
            border-color: #cccccc;
        }

        rangeslider-wrap {
            padding-top: 100px;
        }

        .rangeslider {
            position: relative;
            height: 4px;
            border-radius: 5px;
            width: 100%;
            background-color: gray;
        }

        .rangeslider__handle {
            transition: background-color .2s;
            box-sizing: border-box;
            width: 20px;
            height: 20px;
            border-radius: 100%;
            background-color: #0099FF;
            touch-action: pan-y;
            cursor: pointer;
            display: inline-block;
            position: absolute;
            z-index: 3;
            top: -8px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.5), inset 0 0 0 2px white;
        }

        .rangeslider__handle__value {
            transition: background-color .2s, box-shadow .1s, transform .1s;
            box-sizing: border-box;
            width: 90px;
            text-align: center;
            padding: 10px;
            background-color: #0099FF;
            border-radius: 5px;
            color: white;
            left: -35px;
            top: -55px;
            position: absolute;
            white-space: nowrap;
            border-top: 1px solid #007acc;
            box-shadow: 0 -4px 1px rgba(0, 0, 0, 0.07), 0 -5px 20px rgba(0, 0, 0, 0.3);
        }

        .rangeslider__handle__value:before {
            transition: border-top-color .2s;
            position: absolute;
            bottom: -10px;
            left: calc(50% - 10px);
            content: "";
            width: 0;
            height: 0;
            border-left: 10px solid transparent;
            border-right: 10px solid transparent;
            border-top: 10px solid;
            border-top-color: #0099FF;
        }

        .rangeslider__handle__value:after {
            content: " %";
        }

        .rangeslider__fill {
            position: absolute;
            top: 0;
            z-index: 1;
            height: 100%;
            background-color: #0099FF;
            border-radius: 5px;
        }

        .rangeslider__labels {
            position: absolute;
            width: 100%;
            z-index: 2;
            display: flex;
            justify-content: space-between;
        }

        .rangeslider__labels__label {
            font-size: 0.75em;
            position: relative;
            padding-top: 15px;
            color: gray;
        }

        .rangeslider__labels__label:before {
            position: absolute;
            top: 0;
            left: 50%;
            transform: translateX(-50%);
            content: "";
            width: 1px;
            height: 9px;
            border-radius: 1px;
            background-color: rgba(128, 128, 128, 0.5);
        }

        .rangeslider__labels__label:first-child:before, .rangeslider__labels__label:last-child:before {
            height: 12px;
            width: 2px;
        }

        .rangeslider__labels__label:first-child:before {
            background-color: #0099FF;
        }

        .rangeslider__labels__label:last-child:before {
            background-color: gray;
        }

        .rangeslider__labels__label:first-child {
            transform: translateX(-48%);
        }

        .rangeslider__labels__label:last-child {
            transform: translateX(48%);
        }

        .rangeslider.rangeslider--active .rangeslider__handle, .rangeslider.rangeslider--active .rangeslider__handle * {
            background-color: #33adff;
        }

        .rangeslider.rangeslider--active .rangeslider__handle *:before {
            border-top-color: #33adff;
        }

        .rangeslider.rangeslider--active .rangeslider__handle__value {
            transform: translateY(-5px);
            box-shadow: 0 -3px 2px rgba(0, 0, 0, 0.04), 0 -9px 25px rgba(0, 0, 0, 0.15);
        }

        .separator{
            border-top: 1px solid #e4e4e4;
            height: 2px;
        }

    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('admin/orders.home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{route('orders.index')}}">{{trans('admin/orders.orders')}}</a></li>
                <li class="breadcrumb-item active">{{trans('admin/orders.edit')}}</li>
            </ul>
        </div>
    </div>

    {{-- Form --}}
    {!! Form::model($order,['method'=>'PATCH','route'=>['orders.update',$order->id],'class'=>'form-horizontal']) !!}

    {{-- Inputs --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="margin-bottom: 0px">{{trans('admin/orders.crud')}}</h1>
            </header>
            <div class="row">
                {{-- Main Details --}}
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h1>{{trans('admin/orders.order_delivery')}}</h1>
                        </div>
                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">{{trans('admin/orders.customer_name')}}</label>
                                <div class="col-sm-9">
                                    <input name="customer_name" type="text" placeholder="{{trans('admin/orders.customer_name')}}" value="{{$order->delivery->customer_name}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">{{trans('admin/orders.customer_addr')}}</label>
                                <div class="col-sm-9">
                                    <input name="customer_address"  type="text" placeholder="{{trans('admin/orders.customer_addr')}}" value="{{$order->delivery->customer_address}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">{{trans('admin/orders.customer_phn')}}</label>
                                <div class="col-sm-9">
                                    <input name="customer_phone" type="text" placeholder="{{trans('admin/orders.customer_phn')}}" value="{{$order->delivery->customer_phone}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">{{trans('admin/orders.customer_email')}}</label>
                                <div class="col-sm-9">
                                    <input name="customer_email" type="text" placeholder="{{trans('admin/orders.customer_email')}}" value="{{$order->delivery->customer_email}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="separator" style="margin-bottom: 25px;margin-top: 20px"></div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Tracking {{trans('admin/orders.number')}}</label>
                                <div class="col-sm-9">
                                    <input name="tracking_number" type="text" placeholder="Tracking {{trans('admin/orders.number')}}" value="{{$order->delivery->tracking_number}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">{{trans('admin/orders.order_notes')}}</label>
                                <div class="col-sm-9">
                                    <input name="notes" type="text" placeholder="{{trans('admin/orders.order_notes')}}" value="{{$order->notes}}" class="form-control form-control-success">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Submit --}}
                <div class="col-md-12" style="margin-bottom: 10px">
                    <div class="form-group row" style="margin-bottom: 0px">
                        <div class="col-sm-12">
                            <input type="submit" value="{{trans('admin/orders.update_order')}}" class="btn btn-outline-primary" style="float: right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection




