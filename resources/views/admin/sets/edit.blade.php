@extends('layouts.admin')

@section('styles')

    <!-- File Upload Select Elements -->
    <style>
        .images ul {
            list-style-type: none;
        }

        .images li {
            display: inline-block;
        }

        .images input[type="checkbox"][id^="product_"] {
            display: none;
        }

        .images label {
            border: 1px solid #dddddd;
            padding: 10px;
            display: block;
            position: relative;
            margin: 10px;
            cursor: pointer;
        }

        .images label:before {
            background-color: white;
            color: white;
            content: " ";
            display: block;
            border-radius: 50%;
            border: 1px solid grey;
            position: absolute;
            top: -5px;
            left: -5px;
            width: 25px;
            height: 25px;
            text-align: center;
            line-height: 28px;
            transition-duration: 0.4s;
            transform: scale(0);
        }

        .images label img {
            height: 94px;
            width: 94px;
            transition-duration: 0.2s;
            transform-origin: 50% 50%;
        }

        .images :checked + label {
            border-color: #ddd;
        }

        .images :checked + label:before {
            content: "✓";
            background-color: grey;
            transform: scale(1);
        }

        .images :checked + label img {
            transform: scale(0.9);
            box-shadow: 0 0 5px #333;
            z-index: -1;
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('sets.index')}}">Sets</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1>SET CRUD</h1>
            </header>
            <div class="row">
                {!! Form::model($set,['method'=>'PATCH','route'=>['sets.update',$set->id],'class'=>'form-horizontal']) !!}
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h1>Set Details</h1>
                        </div>

                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name</label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" placeholder="Name" value="{{$set->name}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Update Style" class="btn btn-outline-primary">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h1>Product Photos:</h1>
                        </div>
                        <div class="card-block images">
                            <div class="pre-scrollable" style="max-height: 500px;overflow-y:scroll;">
                                <ul class=" d-flex flex-wrap" v-sortable>
                                    @foreach($products as $product)
                                        @if($product->photos->count() > 0)
                                            <li>
                                                <span style="margin-left: 10px;">{{$product->code}}</span>
                                                <input type="checkbox" id="product_{{$product->id}}" name="products[]" value="{{$product->id}}" {{ in_array($product->id, $set->products()->pluck('id')->toArray()) ? 'checked':''}}/>
                                                <label style="margin-top: 0px" for="product_{{$product->id}}"><img src="{{$product->photos->first()->getPreviewPath()}}"/></label>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>

@endsection
