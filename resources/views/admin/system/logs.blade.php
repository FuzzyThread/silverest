@extends('layouts.admin')

@section('title',"Manage | Logs")

@section('styles')

    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-toggle.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/lightbox.min.css')}}">

    <style>
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }

        .android {
            margin-left: 30px;
        }

        .dropdown-menu a {
            text-decoration: none;
            color: gray;
            padding: 5px 10px;
        }

        .dropdown-menu a:hover {
            text-decoration: none;
            color: #000000;
        }

        .sweet-alert button {
            margin: 26px 5px 15px 5px !important;
        }

        a.product-code:hover {
            text-decoration: none;
        }

        #snackbar {
            margin-left: -120px;
        }

        .status {
            content: " ";
            width: 4px;
            height: 44px;
            position: absolute;
            top: -10px;
        }

        .custom-dropdown {
            margin: 0.125rem 0px 0px -100px !important;
        }
    </style>

    <style>
        a.link-order-status {
            text-decoration: none;
        }

        .link-order-status.dropdown-toggle::after {
            margin-left: 0;
            content: none;
            border-top: 0;
            border-right: 0;
            border-left: 0;
        }

        .dropdown-menu {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.1);
        }

    </style>

    <style>
        .product-list {
            margin-bottom: 20px;
            width: 376px;
            height: 111px;
            /*flex: 0 0 33.333333%;*/
            /*max-width: 33.333333%;*/
            padding-right: 15px;
            padding-left: 15px;
        }

        .product-thumbnail {
            border: 1px solid #dfdfdf;
            width: 100%;
            height: 107px;
        }

        .pro-thumbnail-img {
            float: left;
            width: 32%;
        }

        .pro-thumbnail-info {
            float: left;
            padding-left: 20px;
            width: 68%;
        }

        .product-title-2 {
            color: #666666;
            font-weight: 500;
            margin-top: 15px;
            overflow: hidden;
            text-overflow: ellipsis;
            text-transform: uppercase;
            white-space: nowrap;
            margin-bottom: 5px;
        }

        p.small-title {
            margin-bottom: 0px;
            font-size: 14px !important;
        }

        img {
            width: 105px;
            height: auto;
            border-right: 1px solid #dfdfdf;
        }

        .pro-thumbnail-info span {
            font-size: 14px !important;
            color: #1a237e !important;
        }

    </style>

    <style>
        .payment-details td {
            padding: 5px 0;
        }

        .td-title-2 {
            color: #999;
            font-family: roboto;
            font-weight: 500;
            text-align: right;
        }

        .td-title-1 {
            color: #999;
            font-size: 14px;
            text-align: left;
        }

        .payment-details tr {
            border-bottom: 1px solid #eee;
        }

        .order-total {
            color: #1a237e;
            font-weight: 500;
            text-align: left;
        }

        .order-total-price {
            color: #1a237e;
            font-family: roboto;
            font-weight: 700;
            text-align: right;
        }

        .payment-details {
            width: 100%;
        }

    </style>

    <style>
        .dropdown-item {
            padding-left: 20px;
            padding-right: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .dropdown-header {
            padding-left: 20px;
            padding-right: 10px;
        }

        .tracking_number {
            max-width: 200px;
            float: right;
        }

        .tracking_number input, .tracking_number input:focus {
            color: #999;
            font-family: roboto;
            font-weight: 500;
            font-size: 14px;
            text-align: center;
            outline: 0;
        }

        .tracking_number input:focus {
            border-color: #1a237e;
            box-shadow: none;
        }

        .tracking_number button, .tracking_number button:hover, .tracking_number button:focus {
            background-color: #1a237e !important;
            border-color: #1a237e !important;
            outline: 0;
            box-shadow: none !important;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('admin/orders.home')}}</a></li>
                <li class="breadcrumb-item active">Logs</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">

            <header>
                <h1 style="float: left;margin-bottom: 0px">LOG CRUD </h1>
                <div class="row">
                    <div class="col-md-7" style="margin-bottom: 0 !important;">
                    </div>
                    <div class="col-md-2" style="padding-top: 3px;margin-bottom: 0 !important;">
                        <input class="toggle-event" type="checkbox" {{request('sort') && request('sort') == 'with_logs' ? 'checked':''}} data-toggle="toggle" data-style="android" data-on="&nbsp;<i class='fa fa-eye fa-lg' aria-hidden='true'></i>" data-off="<i class='fa fa-eye-slash fa-lg' aria-hidden='true'></i>&nbsp;" data-width="60" data-height="20" data-size="small" data-onstyle="primary">
                    </div>
                    <div class="col-md-3" style="padding-right: 0 !important;margin-bottom: 0 !important;">
                        <h2 class="counter">Logs Number :
                            <span style="font-size: 17px;color: #0275d8;">{{\App\LogGroup::all()->count()}}</span></h2>
                    </div>
                </div>

            </header>

            <div id="orders" style="margin-top: 50px">
                <div class="row">
                    @foreach($log_groups as $log_group)
                        <div class="col-lg-12" style="margin-bottom: 10px">
                            <div class="card">
                                <div class="card-header" style="padding: 0px;height: 44px;">
                                    <div class="row">
                                        <div class="col-md-2" style="margin: auto 0;">
                                            <p id="order_viewed{{$log_group->id}}" style="margin-left: 20px;margin-bottom: 0px;font-size: 16px;color:#043180;">
                                                {{strtoupper($log_group->name)}}
                                            </p>
                                            <span class="status" style="background-color:#391fff"></span>
                                        </div>
                                        <div class="col-md-7" style="margin: auto 0;">
                                            <div class="row" style="padding: 0 auto;">
                                                @php
                                                    $errors = $log_group->logs()->where('type','=','error')->get()->count();
                                                    $warnings = $log_group->logs()->where('type','=','warning')->get()->count();
                                                    $infos = $log_group->logs()->where('type','=','info')->get()->count();
                                                    $successes = $log_group->logs()->where('type','=','success')->get()->count();
                                                @endphp
                                                <p class="col-sm-3" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: blue;font-size: 16px;">
                                                    Info: {{$infos}}
                                                </p>
                                                <p class="col-sm-3" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: green;font-size: 16px;">
                                                    Success: {{$successes}}
                                                </p>
                                                <p class="col-sm-3" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: purple;font-size: 16px;">
                                                    Warnings: {{$warnings}}
                                                </p>
                                                <p class="col-sm-3" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: red;font-size: 16px;">
                                                    Errors: {{$errors}}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="margin: auto 0;">
                                            <div style="float: right;margin-right: 35px;margin-top: 0px">
                                                <div class="row" style=" height: 24px;">
                                                    <div class="col-xs-8" style="margin: 0;margin-right: 30px">
                                                        <span style="font-size: 16px;color: #043180">{{($log_group->created_at  ? $log_group->created_at->format('j M Y - H:i:s'):'No Date')}} </span>
                                                    </div>
                                                    <div class="col-xs-4" style="margin: 0">
                                                        <a value="{{$log_group->id}}" style="color: #515354;" data-toggle="collapse" href="#product-{{$log_group->id}}-details" aria-expanded="true" aria-controls="test-block"><i class="fa fa-expand fa-lg" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="product-{{$log_group->id}}-details" class="collapse">
                                    <div class="card-block" style="border-bottom: 1px solid #f2f2f2; padding-top: 0px;padding-bottom: 0px">
                                        <div class="row" style="padding-top: 20px;padding-bottom: 0px">

                                            @foreach($log_group->logs as $log)

                                                <div class="col-md-12" style="margin-bottom: 20px">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            @php
                                                                $color = 'red';

                                                                switch ($log->type){

                                                                case 'info':
                                                                   $color = 'blue';
                                                                   break;

                                                                case 'error':
                                                                   $color = 'red';
                                                                   break;

                                                                case 'warning':
                                                                   $color = 'purple';
                                                                   break;

                                                                case 'success':
                                                                   $color = 'green';
                                                                   break;
                                                                }
                                                            @endphp

                                                            <div class="row">
                                                                <div class="col-md-1">
                                                                    <a class="dropdown-toggle link-order-status" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="">
                                                                        <i class="fa fa-square" style="color: {{$color}};font-size: 18px;margin-top: 2px"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <p style="margin-bottom: 20px;margin-top: 3px;font-style: italic;margin-left: -20px;color: {{$color}}">{{strtoupper($log->key)}}</p>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <p style="margin-bottom: 20px;margin-top: 3px;text-transform: uppercase;">{{ str_limit($log->description,200)}}</p>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <span style="font-size: 16px;float: right;color: #043180">{{($log->created_at  ? $log->created_at->format('j M Y - H:i:s'):'No Date')}} </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div style="margin-top: 20px">
                    {{$log_groups->links()}}
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <!-- Javascript Files -->
    <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/bootstrap-toggle.js')}}"></script>

    <script>
        $(function () {
            $('.toggle-event').change(function () {
                var is_visible = $(this).is(":checked");

                if (is_visible) {
                    window.location.replace("{{route('system.logs', ['sort'=>'with_logs'])}}");
                } else {
                    window.location.replace("{{route('system.logs')}}");
                }
            });
        })
    </script>

@endsection


