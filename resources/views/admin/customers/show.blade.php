@extends('layouts.admin')

@section('styles')

    <style>
        a.link-order-status {
            text-decoration: none;
        }

        .dropdown-toggle::after {
            margin-left: 0;
            content: none;
            border-top: 0;
            border-right: 0;
            border-left: 0;
        }

        .dropdown-menu {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.1);
        }
    </style>

    <style>
        .product-list {
            margin-bottom: 20px;
            width: 376px;
            height: 111px;
            padding-right: 15px;
            padding-left: 15px;
        }

        .product-thumbnail {
            border: 1px solid #dfdfdf;
            width: 100%;
            height: 107px;
        }

        .pro-thumbnail-img {
            float: left;
            width: 32%;
        }

        .pro-thumbnail-info {
            float: left;
            padding-left: 20px;
            width: 68%;
        }

        .product-title-2 {
            color: #666666;
            font-weight: 500;
            margin-top: 15px;
            overflow: hidden;
            text-overflow: ellipsis;
            text-transform: uppercase;
            white-space: nowrap;
            margin-bottom: 5px;
        }

        p.small-title {
            margin-bottom: 0px;
            font-size: 14px !important;
        }

        img {
            width: 105px;
            height: auto;
            border-right: 1px solid #dfdfdf;
        }

        .pro-thumbnail-info span {
            font-size: 14px !important;
            color: #1a237e !important;
        }

    </style>

    <style>
        .payment-details td {
            padding: 5px 0;
        }

        .td-title-2 {
            color: #999;
            font-family: roboto;
            font-weight: 500;
            text-align: right;
        }

        .td-title-1 {
            color: #999;
            font-size: 14px;
            text-align: left;
        }

        .payment-details tr {
            border-bottom: 1px solid #eee;
        }

        .order-total {
            color: #1a237e;
            font-weight: 500;
            text-align: left;
        }

        .order-total-price {
            color: #1a237e;
            font-family: roboto;
            font-weight: 700;
            text-align: right;
        }

        .payment-details {
            width: 100%;
        }
    </style>

    <style>
        .dropdown-item {
            padding-left: 20px;
            padding-right: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .dropdown-header {
            padding-left: 20px;
            padding-right: 10px;
        }

        .tracking_number {
            max-width: 200px;
            float: right;
        }

        .tracking_number input, .tracking_number input:focus {
            color: #999;
            font-family: roboto;
            font-weight: 500;
            font-size: 14px;
            text-align: center;
            outline: 0;
        }

        .tracking_number input:focus {
            border-color: #1a237e;
            box-shadow: none;
        }

        .tracking_number button, .tracking_number button:hover, .tracking_number button:focus {
            background-color: #1a237e !important;
            border-color: #1a237e !important;
            outline: 0;
            box-shadow: none !important;
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('customers.index')}}">Customers</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1>{{$customer->first_name}} {{$customer->last_name}}
                    <span style="color: #1a237e">({{$customer->email}})</span></h1>
            </header>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="float-left">Orders:</h1>
                        </div>
                    </div>
                </div>
                @foreach($customer->orders as $order)
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header" style="padding: 0px;height: 44px;">
                                <div class="row">
                                    <div class="col-md-1" style="margin: auto 0;">
                                        <p id="order_viewed{{$order->id}}" style="margin-left: 20px;margin-bottom: 0px;font-size: 16px;{{$order->is_viewed?'':'color:#043180;'}}">
                                            {{$order->id}}
                                        </p>
                                    </div>
                                    <div class="col-md-5" style="margin: auto 0;">
                                        <div class="row" style="padding: 0 auto;">
                                            <p class="col-sm-7" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                {{$order->delivery ? $order->delivery->customer_email : 'Error'}}
                                            </p>
                                            <p class="col-sm-5" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                Delivery:
                                                <span style="font-size: 16px;">{{ucfirst($order->delivery ? $order->delivery->method: 'Error')}}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="margin: auto 0;">
                                        <div class="row" style="padding: 0 auto;">
                                            <p class="col-sm-5" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                Total:
                                                <span style="font-size: 16px;">{{$order->payment ? $order->payment->subtotal . '&euro;' : 'Error'}}</span>
                                            </p>

                                            <p class="col-sm-7" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                Placed:
                                                <span style="font-size: 16px">{{($order->created_at  ? $order->created_at->diffForHumans():'No Date')}} </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="margin: auto 0;">
                                        <div style="float: right;margin-right: 35px;margin-top: 0px">
                                            <div class="row" style=" height: 24px;">
                                                <div class="col-xs-4" style="margin: 0">
                                                    @if($order->status && ($order->status->id == config('constants.STATUS.DISPATCHED') || $order->status->id == config('constants.STATUS.PREPARATION')))
                                                        <a>
                                                            <i class="fa fa-square" style="color: {{$order->status->value}}"></i>
                                                        </a>
                                                    @else
                                                        <a class="dropdown-toggle link-order-status">
                                                            <i class="fa fa-square" style="color: white"></i>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('orders.edit',$order->id)}}" style="margin-right: 20px;margin-left: 22px;color: #0275d8;cursor: pointer"><i class="fa fa-pencil  fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a class="{{$order->is_viewed ? '':'open-order'}}" value="{{$order->id}}" style="color: #515354;" data-toggle="collapse" href="#product-{{$order->id}}-details" aria-expanded="true" aria-controls="test-block"><i class="fa fa-expand fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-block" style="text-align: center">
                                <div class="row">

                                    <div class="col-md-6" style="border-right: 1px solid #dfdfdf;padding:20px;margin-bottom: 0px">
                                        <div class="billing-details pr-10">
                                            <h5 class="widget-title mb-20 mt-10" style="color: #1a237e;margin-bottom: 15px">
                                                <i class="fa fa-truck" aria-hidden="true"></i>&nbsp; Delivery:
                                            </h5>
                                        </div>
                                        <table class="payment-details">
                                            @if($order->delivery)
                                                <tr>
                                                    <td class="td-title-1">
                                                        {{($order->delivery->method == 'omniva') ? 'Shipping Notes' : 'Address' }}
                                                    </td>
                                                    <td class="td-title-2">
                                                        {{$order->delivery->customer_address}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td-title-1">Name</td>
                                                    <td class="td-title-2">
                                                        {{$order->delivery->customer_name}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td-title-1">Phone Number</td>
                                                    <td class="td-title-2">{{$order->delivery->customer_phone}}</td>
                                                </tr>
                                                <tr style="border: none">
                                                    <td class="td-title-1">Email</td>
                                                    <td class="td-title-2">{{$order->delivery->customer_email}}</td>
                                                </tr>
                                                <tr style="border: none">
                                                    <td class="td-title-1">Tracking number</td>
                                                    <td class="td-title-2">
                                                        {{$order->delivery?$order->delivery->tracking_number:''}}
                                                    </td>
                                                </tr>
                                            @else
                                                @php
                                                    $order->status_id = config('constants.STATUS.ERROR');
                                                    $order->save();
                                                @endphp
                                                <tr style="border: none">
                                                    <td class="td-title-1">Delivery</td>
                                                    <td class="td-title-2" style="color: red">Error</td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                    <div class="col-md-6" style="padding:20px;margin-bottom: 0px">
                                        <div class="billing-details pr-10">
                                            <h5 class="widget-title mb-20 mt-10" style="color: #1a237e;margin-bottom: 15px">
                                                <i class="fa fa-info" aria-hidden="true"></i>&nbsp; Order Details:
                                            </h5>
                                        </div>
                                        <table class="payment-details">
                                            @if($order->delivery)
                                                <tr>
                                                    <td class="td-title-1">Payment Method</td>
                                                    <td class="td-title-2">{{ucwords($order->payment->method)}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="td-title-1">Cart Total</td>
                                                    <td class="td-title-2">{{number_format(($order->payment->cart_total),2)}} &euro;</td>
                                                </tr>
                                                <tr>
                                                    <td class="td-title-1">Shipping Total</td>
                                                    <td class="td-title-2">{{number_format(($order->payment->shipping_total),2)}} &euro;</td>
                                                </tr>
                                                <tr>
                                                    <td class="td-title-1">Order Subtotal</td>
                                                    <td class="td-title-2">{{number_format(($order->payment->subtotal),2)}} &euro;</td>
                                                </tr>
                                                <tr style="border: none">
                                                    <td class="td-title-1">Date Ordered</td>
                                                    <td class="td-title-2">{{date('j F Y', strtotime($order->created_at))}}</td>
                                                </tr>
                                            @else
                                                @php
                                                    $order->status_id = config('constants.STATUS.ERROR');
                                                    $order->save();
                                                @endphp
                                                <tr style="border: none">
                                                    <td class="td-title-1">Payment</td>
                                                    <td class="td-title-2" style="color: red">Error</td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                </div>

                                @if($order->notes)
                                    <div class="row" style="border-top: 1px solid #dfdfdf;padding-bottom: 0px;padding-top: 20px">
                                        <div class="col-md-12" style="margin-bottom: 0px">
                                            <h5 class="widget-title mt-10" style="color: #1a237e;margin-bottom: 20px;line-height: 1.6;">
                                                <i class="fa fa-comments" aria-hidden="true"></i>&nbsp; Order Notes: &nbsp;
                                                <span style="font-size: 14px">{{$order->notes}}</span>
                                            </h5>
                                        </div>
                                    </div>
                                @endif

                                <div class="row" style="border-top: 1px solid #dfdfdf;padding-bottom: 0px;padding-top: 20px">
                                    @if($order->items()->count() > 0)
                                        @foreach($order->items as $item)
                                            @php
                                                $product = \App\Product::withTrashed()->find($item->product_id);
                                            @endphp
                                            <div class="product-list" style="margin-bottom: 20px;">
                                                @if($product)
                                                    <div class="product-thumbnail cart_thumbnail">
                                                        <div class="pro-thumbnail-img">
                                                            <img src="{{$product->photos()->count() > 0 ?  $product->photos->first()->getPreviewPath() : ''}}" alt="" class="cart_product_img">
                                                        </div>
                                                        <div class="pro-thumbnail-info text-left">
                                                            <h6 class="product-title-2">
                                                                <a>{{$product->name}} ( {{$product->code}} )</a>
                                                            </h6>
                                                            @if($product->gems()->count() > 0)
                                                                <p class="small-title">
                                                                    <span>Gem <strong>: &nbsp;</strong></span>
                                                                    @foreach($product->gems as $gem)
                                                                        @if($loop->last)
                                                                            {{$gem->name}}
                                                                        @else
                                                                            {{$gem->name}},
                                                                        @endif
                                                                    @endforeach
                                                                </p>
                                                            @endif
                                                            @if($item->size)
                                                                <p class="small-title">
                                                                    <span>Size <strong>: &nbsp;</strong></span>{{$item->size}}
                                                                </p>
                                                            @endif
                                                            <p class="small-title">
                                                                <span>Quantity <strong>: &nbsp;</strong></span>{{$item->quantity}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="product-thumbnail cart_thumbnail">
                                                        <div class="pro-thumbnail-img">
                                                        </div>
                                                        <div class="pro-thumbnail-info text-left">
                                                            <h6 class="product-title-2">
                                                                Error
                                                            </h6>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                    @else
                                        @php
                                            $order->status_id = config('constants.STATUS.ERROR');
                                            $order->save();
                                        @endphp
                                        <div class="col-md-12 payment-method">
                                            <!-- our order -->
                                            <div class="payment-details">
                                                <h4 style="color: #1a237e;margin-bottom: 0px"> There are no products in this order (ERROR)</h4>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="float-left">Wishlist:</h1>
                        </div>
                    </div>
                </div>
                @foreach($customer->wishlists()->where('is_visible', '1')->get() as $product)
                    <div class="col-lg-2 col-md-2 col-sm-6">
                        <div class="card">
                            <div class="card-header" style="height: auto">
                                <h4>{{str_limit($product->name,12)}}</h4>
                                <p>{{$product->code}}</p>
                            </div>
                            <div class="card-block" style="text-align: center">
                                <img src="{{$product->photos->first()->getPreviewPath()}}" style="" alt="" class="img-thumbnail">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection

