@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('permissions.index')}}">Permissions</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1>PERMISSION CRUD</h1>
            </header>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h1>Permission Details</h1>
                        </div>
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-sm-4">Name</label>
                                <h2 class="col-sm-8">{{$permission->display_name}}</h2>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4">Slug</label>
                                <h2 class="col-sm-8">{{$permission->name}}</h2>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4">Description</label>
                                <h2 class="col-sm-8">{{$permission->description}}</h2>
                            </div>
                            <div class="form-group row" style="margin-bottom: 0">
                                <div class="col-sm-10">
                                    <a href="{{route('permissions.edit',$permission->id)}}" class="btn btn-outline-primary">Edit Permission</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

