@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('gems.index')}}">Gems</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="margin-bottom: 0px">GEM CRUD</h1>
            </header>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h1>Create Gem</h1>
                        </div>

                        <div class="card-block">
                            {!! Form::open(['method'=>'POST','route'=>'gems.store','class'=>'form-horizontal','files'=>true]) !!}
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name (ENG)</label>
                                <div class="col-sm-9">
                                    <input name="name_en" type="text" placeholder="enter name..." value="{{old('name_en')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name (RUS)</label>
                                <div class="col-sm-9">
                                    <input name="name_ru" type="text" placeholder="enter name..." value="{{old('name_ru')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name (EST)</label>
                                <div class="col-sm-9">
                                    <input name="name_et" type="text" placeholder="enter name..." value="{{old('name_et')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Type</label>
                                <div class="col-sm-9">
                                    <input name="type" type="text" placeholder="Type" value="{{old('type')}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Code</label>
                                <div class="col-sm-9">
                                    <input name="code" type="text" placeholder="Code" value="{{old('code')}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0;">
                                <label class="col-sm-3">Image</label>
                                <div class="col-sm-9">
                                <input type="file" class="filestyle" name="image" value="{{old('image')}}" data-placeholder="No file">
                                </div>
                            </div>

                            <div class="form-group row" style="margin-top: 30px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Create Gem" class="btn btn-outline-primary">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <script src="{{asset('assets/admin/js/bootstrap-filestyle.min.js')}}"></script>

    <script>
        $(":file").filestyle({placeholder: "No file"});
    </script>

@endsection

