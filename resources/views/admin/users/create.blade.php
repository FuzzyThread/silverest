@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1>USER CRUD</h1>
            </header>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        {!! Form::open(['method'=>'POST','route'=>'users.store','class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>Create User</h1>
                        </div>
                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">First Name</label>
                                <div class="col-sm-9">
                                    <input name="first_name" type="text" placeholder="First Name" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Last Name</label>
                                <div class="col-sm-9">
                                    <input name="last_name" type="text" placeholder="Last Name" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Email</label>
                                <div class="col-sm-9">
                                    <input name="email" type="email" placeholder="Email Address" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" id="generate_password" style="margin: 0">
                                <label class="col-sm-3">Password</label>
                                <div class="col-sm-9">
                                    <input name="password" type="password" placeholder="Password" class="form-control form-control-warning">
                                </div>
                            </div>

                            <div class="form-group row" style="margin: 0;margin-top: 20px">
                                <div class="i-checks col-sm-offset-1 col-sm-5">
                                    <input id="checkboxCustom1" type="checkbox" value="" class="form-control-custom">
                                    <label for="checkboxCustom1" style="font-size: 14px">Generate Password</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h1>Assign Roles</h1>
                        </div>

                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                @foreach($roles as $role)
                                    <div class="i-checks col-sm-12">
                                        {!! Form::checkbox('roles[]', $role->id, false,['class' => 'form-control-custom','id'=>$role->id]) !!}
                                        <label for="{{$role->id}}" style="font-size: 15px;padding-top: 0">{{$role->display_name}}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                    <div class="col-sm-10">
                        <input type="submit" value="Update User" class="btn btn-outline-primary">
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>

@endsection


@section('scripts')

    <script>
        $(function () {
            $('#checkboxCustom1').change(function () {
                $('#generate_password').toggle(!this.checked);
            }).change(); //ensure visible state matches initially
        });
    </script>

@endsection
