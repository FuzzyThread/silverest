@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1>USER CRUD</h1>
            </header>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        {!! Form::model($user,['method'=>'PATCH','route'=>['users.update',$user->id],'class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>User Details</h1>
                        </div>

                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">First Name</label>
                                <div class="col-sm-9">
                                    <input name="first_name" type="text" placeholder="First Name" value="{{$user->first_name}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Last Name</label>
                                <div class="col-sm-9">
                                    <input name="last_name" type="text" placeholder="Last Name" value="{{$user->last_name}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Email</label>
                                <div class="col-sm-9">
                                    <input name="email" type="email" placeholder="Email Address" value="{{$user->email}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" id="type_password" style="margin: 0;display: none">
                                <label class="col-sm-3">Password</label>
                                <div class="col-sm-9">
                                    <input name="password" type="password" placeholder="Password" class="form-control form-control-warning">
                                </div>
                            </div>

                            <div class="line"></div>

                            <div class="form-group row" style="margin-left: 0px;margin-bottom: 15px">
                                <div class="i-checks col-sm-offset-1 col-sm-5">
                                    <input id="keep_pwd" type="radio" checked="" value="keep" name="password_options" class="form-control-custom radio-custom">
                                    <label for="keep_pwd">Keep Old Password</label>
                                    <input id="generate_pwd" type="radio" value="auto" name="password_options" class="form-control-custom radio-custom">
                                    <label for="generate_pwd">Generate New Password</label>
                                    <input id="type_pwd" type="radio" value="manual" name="password_options" class="form-control-custom radio-custom">
                                    <label for="type_pwd">Type New Password</label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h1>Assign Roles</h1>
                        </div>

                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                @foreach($roles as $role)
                                    <div class="i-checks col-sm-12">
                                        {!! Form::checkbox('roles[]', $role->id, in_array($role->id, $user->roles->pluck('id')->toArray()) ? true : false,['class' => 'form-control-custom','id'=>$role->id]) !!}
                                        <label for="{{$role->id}}" style="font-size: 15px;padding-top: 0">{{$role->display_name}}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                    <div class="col-sm-10">
                        <input type="submit" value="Update User" class="btn btn-outline-primary">
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <script>
        $(function () {
            $("input[name=password_options]").click(function () {
                var visible;
                switch ($(this).val()) {
                    case "keep":
                        visible = false;
                        break;
                    case "manual":
                        visible = true;
                        break;
                    case "auto":
                        visible = false;
                        break;
                }
                $('#type_password').toggle(visible);
            });
        });
    </script>

@endsection
