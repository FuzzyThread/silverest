@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1>ROLE CRUD</h1>
            </header>
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">Role Details</h1>
                            <a href="{{route('roles.edit',$role->id)}}" class="btn btn-outline-primary float-right">Edit Role</a>
                        </div>
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-sm-4">Name</label>
                                <h2 class="col-sm-8">{{$role->display_name}}</h2>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4">Slug</label>
                                <h2 class="col-sm-8">{{$role->name}}</h2>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4">Description</label>
                                <h2 class="col-sm-8">{{$role->description}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h1>Permissions Assigned:</h1>
                        </div>
                        <div class="card-block" style="margin-left: 20px">
                            <ul>
                                @foreach($role->permissions as $permission)
                                    <li>{{$permission->display_name}} <em>({{$permission->description}})</em></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

