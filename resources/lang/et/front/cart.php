<?php

return [
    'home' => 'Koduleht',
    'cart' => 'Korv',
    'shopping_cart' => 'Ostukorv',
    'wishlist' => 'Lemmikud',
    'checkout' => 'Maksma',
    'order_complete' => 'Tellimus täidetud',
    'product' => 'Toode',
    'price' => 'Hind',
    'quantity' => 'Kogus',
    'total' => 'Kokku',
    'remove' => 'Eemalda',
    'gem' => 'Kivi',
    'size' => 'Suurus',
    'cart_details' => 'Ostukorv',
    'vat_already' => 'Vat on juba sisse lülitatud hind',
    'cart_subtotal' => 'Kokku on ostukorvis',
    'next' => 'Edasi',
    'shopping_cart_is_empty' => 'Ostukorv on tühi',
    'material' => 'Materjal',
    'gems' => 'Kivid',
    'weight' => 'Kaal',
    'grams' => 'gramm',
    'add_to_cart' => 'Lisa Ostukorvi',
    'sizes' => 'Suurused'
];
