<?php

return [
    'wishlist' => 'Lemmikud',
    'home' => 'Koduleht',
    'shopping_cart' => 'Ostukorv',
    'checkout' => 'Vaata',
    'order_complete' => 'Tellimus on valmis',
    'product' => 'Toode',
    'price' => 'Hind',
    'stock_status' => 'Laos',
    'add_cart' => 'Lisa ostukorvi',
    'remove' => 'Eemalda',
    'gem' => 'Kivi',
    'size' => 'Suurus',
    'back' => 'Tagasi',
    'next' => 'Edasi',
    'is_empty' => 'Lemmikud tühjad',
];
