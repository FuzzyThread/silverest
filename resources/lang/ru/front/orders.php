<?php

return [
    'back' => 'Назад',
    'home' => 'Главная',
    'account_dashboard' => 'Кабинет Пользователя',
    'orders' => 'Заказы',
    'order' => 'Заказ',
    'gem' => 'Камень',
    'size' => 'Размер',
    'quantity' => 'Количество',
    'there_are_no_products' => 'В этом заказе нет изделий',
    'show_more_details' => 'Показать Подробнее',
    'download_invoice' => 'Скачать счет',
    'extra_notes' => 'Дополнительные Заметки',
    'cancel_order' => 'Отменить Заказ',
    'no_orders' => 'У вас еще нету заказов',
    'please_leave' => 'Пожалуйста напишите о дополнительных пожеланиях для заказа',
    'submit' => 'Отправить'
];
