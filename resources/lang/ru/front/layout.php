<?php

return [
    'hello' => 'Здравствуйте',
    'hello_guest' => 'Здравствуйте Гость',
    'my_account' => 'Мой аккаунт',
    'wishlist' => 'Лист Пожеланий',
    'admin' => 'Админ',
    'logout' => 'Выйти',
    'login' => 'Войти',
    'total' => 'Итого',
    'home' => 'Главная',
    'catalogue' => 'Каталог',
    'warranty' => 'Гарантия',
    'delivery' => 'Доставка',
    'about_us' => 'О Нас',
    'contacts' => 'Контакты',
    'your_cart' => 'Корзина',
    'price' => 'Цена',
    'size' => 'Размер',
    'q_ty' => 'Кол-во',
    'your_cart_is_empty' => 'Ваша корзина пуста',
    'view_cart' => 'Показать корзину',
    'check_out' => 'Оплатить',
    'login_to_see' => 'Войдите чтобы увидеть',
    'welcome_to_the_website' => 'Добро пожаловать в интернет-магазин ювелирной компании SILVEREST!',
    'jewelry_company' => 'Ювелирная компания "SILVEREST" занимается производством ювелирных изделий из серебра 925 пробы с 2004 года. Нашей целью всегда был выпуск высококачественных изделий по доступным ценам.',
    'manufacture_of_jewelry' => 'Производство ювелирных изделий находится на северо-востоке Эстонии. Продукция нашей компании выполнена в лучших традициях классического ювелирного производства, а также учитывает тенденции современной ювелирной моды. Наша компания постоянно обновляет свой ассортимент и оперативно отслеживает вкусы и запросы покупателей.',
    'new_products' => 'Новые изделия',
    'discount_products' => 'Изделия со скидкой',
    'best_sell_products' => 'Лидеры продаж',
    'popular_products' => 'Популярные изделия',
    'my_wishlist' => 'Лист Пожеланий',
    'my_cart' => 'Моя Корзина',
    'my_orders' => 'Мои Заказы',
    'all_rights_reserved' => 'Все права соблюдены.',
    'sign_in' => 'Войти',
    'new_account' => 'Новый Аккаунт',
    'email_address' => 'Email Адрес',
    'password' => 'Пароль',
    'remember_me' => 'Запомнить меня',
    'forgot_your_password' => 'Забыли пороль',
    'last_name' => 'Фамилия',
    'first_name' => 'Имя',
    'confirm_password' => 'Подтвердить Пароль',
    'lost_your_password' => 'Потярили пароль? Пожалуйста введите свой Email адрес. Вы получите ссылку для создания нового пароля.',
    'back_to_login' => 'Обратно к входу',
    'close' => 'Закрыть',
    'search' => 'Поиск изделия',
    'cookie_text' => 'Сообщаем, что на этом сайте используются файлы cookie',
    'cookie_btn' => 'Разрешить',
    'language' => 'Рус'
];
