<?php

return [
    'home' => 'Главная',
    'cart' => 'Корзина',
    'shopping_cart' => 'Корзина для покупок',
    'wishlist' => 'Избранное',
    'checkout' => 'Оплатить',
    'order_complete' => 'Заказ Оформлен',
    'product' => 'Изделие',
    'price' => 'Цена',
    'quantity' => 'Количество',
    'total' => 'Итого',
    'remove' => 'Убрать',
    'gem' => 'Камень',
    'size' => 'Размер',
    'cart_details' => 'Корзина',
    'vat_already' => 'Vat уже включен в цену изделия',
    'cart_subtotal' => 'Итого в Корзине',
    'next' => 'Вперед',
    'shopping_cart_is_empty' => 'Корзина для покупок пуста',
    'material' => 'Материал',
    'gems' => 'Камни',
    'weight' => 'Вес',
    'grams' => 'грамм',
    'add_to_cart' => 'Добавить в Корзину',
    'sizes' => 'Размеры'
];
