<?php

return [
    'invoice_to' => 'СЧЕТ НА',
    'invoice' => 'СЧЕТ',
    'date_of_invoice' => 'Дата счета',
    'description' => 'ОПИСАНИЕ',
    'unit_price' => 'ЦЕНА ТОВАРА',
    'quantity' => 'КОЛ-ВО',
    'total' => 'ИТОГО',
    'size' => 'Размер',
    'cart_total' => 'ВСЕГО В КОРЗИНЕ',
    'shipping_total' => 'ЦЕНА ДОСТАВКИ',
    'thank_you' => 'Спасибо за заказ',
    'notice' => 'УВЕДОМЛЕНИЕ',
    'order_will_be_dispatched' => 'Заказ будет отправлен после получения успешной оплаты.',
    'invoice_was_created' => 'Счет создан на компьютере и действителен без подписи и печати.',
    'please_send' => 'Отправьте платеж, используя банковские реквизиты, указанные ниже',
    'bank_details' => 'Банковские реквизиты',
];
