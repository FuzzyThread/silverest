<?php

return [
    'invoice_to' => 'INVOICE TO',
    'invoice' => 'INVOICE',
    'date_of_invoice' => 'Date of Invoice',
    'description' => 'DESCRIPTION',
    'unit_price' => 'UNIT PRICE',
    'quantity' => 'QUANTITY',
    'total' => 'TOTAL',
    'size' => 'Size',
    'cart_total' => 'CART TOTAL',
    'shipping_total' => 'SHIPPING TOTAL',
    'thank_you' => 'Thank you',
    'notice' => 'NOTICE',
    'order_will_be_dispatched' => 'An order will be dispatched upon receiving a successful payment',
    'invoice_was_created' => 'Invoice was created on a computer and is valid without the signature and seal.',
    'please_send' => 'Please send your payment using bank details below',
    'bank_details' => 'Bank details',
];
