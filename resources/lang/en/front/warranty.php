<?php

return [
    'home' => 'Home',
    'warranty' => 'Warranty',
    'warranty_1' => 'Guarantee Quality Guarantee Exchange, back Guarantee!',
    'warranty_2' => 'Buy without risk!',
    'warranty_3' => 'We guarantee the quality of our products: 925. By buying our products, you can take full advantage of your rights:',
    'warranty_4' => '1. Warranty refund or exchange goods within 14 days.',
    'warranty_5' => '2. The warranty on workmanship for 1 year.',
    'warranty_6' => 'Prices Terms',
    'warranty_7' => 'All prices are valid only for our online store, excluding shipping cost. With online orders total cost reported in the ordering process ("cash"). All registered customers in our store is 2.5% discount from the amount of the order. Additional discounts are a customer purchases automatically as you get the status of various "cards". At the moment, there are three species: the silver card "," Gold Card "and" platinum card ".',
    'warranty_8' => 'Product Image',
    'warranty_9' => '925 products from may appear on the screen partially enlarged. Each and every gem stone unique - so may vary slightly in an article in relation to the image.Submitted online product may differ with respect to color, surface, size, etc. on the form of the original product.',
    'warranty_10' => 'Introduction to the Terms of avoidance',
    'warranty_11' => 'We want you to be satisfied with our products. If you do not like jewelry, you can return it or exchange for any other decoration for 14 days.',
    'warranty_12' => 'Return policy',
    'warranty_13' => '1. Goods must be returned in a package which would prevent damage.',
    'warranty_14' => '2. The product must be in the same condition as when it was at the time of purchase. If the product there are signs that it has been damaged by the buyer (scratches, mechanical damage, etc.) - the goods will not be refunded.',
    'warranty_15' => '3. When returning the postage paid by the buyer.',
    'warranty_16' => '4. Money for the returned goods listed on the settlement bank account provided by the buyer no later than 14 days after termination of the contract with the buyer.',
    'warranty_17' => 'Responsibility',
    'warranty_18' => 'We can not guarantee that all of the online store of products in stock.
                      Online at our website www.silverest.ee submitted product may differ slightly with respect to color, surface, size, etc. on the form of the original product.
                      We are not responsible for improper use of the goods.',
];
