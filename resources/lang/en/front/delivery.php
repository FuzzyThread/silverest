<?php

return [
    'home' => 'Home',
    'delivery' => 'Delivery',
    'delivery_1' => 'The goods are delivered by EESTI POST or OMNIVA.',
    'delivery_2' => 'Shipping and handling within Estonia 5,00 € prepay account.',
    'delivery_3' => 'Parcels are insured. Date of delivery to Estonia, 3-5 days.',
    'delivery_4' => 'Delivery order is given by the customer delivery address. Costs that arise due to wrong address delivery instructions or because of not receiving parcels covered by customer.
                     The client must check when receiving the parcel. If the parcel is damaged, the customer must take the package only with a reservation with a categorical directions given by mail to injury. The employee must address urgently draw up a statement of the damage.',
];
