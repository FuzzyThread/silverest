<?php

return [
    'code' => 'CODE',
    'check_out' => 'More Details',
    'new_products' => 'New Products',
    'catalogue_update' => 'Catalogue is constantly updating to keep with the latest trends',
    'production' => 'Production',
    'our_production' => 'Our production consists of many stages performed by qualified masters',
    'add_to_wishlist' => 'Add to Wishlist',
    'add_to_cart' => 'Add to Cart',
    'shop_in_fama' => 'Shop in Fama',
    'read_more' => 'Read more',
    'our_local_shop' => 'Our local shop',
    'we_could_gladly' => 'We would gladly meet you in person, please come to our local shop located in Narva Fame Keskus'
];
