<?php

return [
    'wishlist' => 'Wishlist',
    'home' => 'Home',
    'shopping_cart' => 'Shopping Cart',
    'checkout' => 'Checkout',
    'order_complete' => 'Order Complete',
    'product' => 'Product',
    'price' => 'Price',
    'stock_status' => 'Stock Status',
    'add_cart' => 'Add To Cart',
    'remove' => 'Remove',
    'gem' => 'Gem',
    'size' => 'Size',
    'back' => 'Back',
    'next' => 'Next',
    'is_empty' => 'Wishlist is Empty',
];
