<?php

return [
    'home' => 'Home',
    'my_account' => 'My Account',
    'account_dashboard' => 'Account Dashboard',
    'orders' => 'ORDERS',
    'no_orders' => 'NO ORDERS YET',
    'click_to_see_orders' => 'Click to see your orders history',
    'click_to_see_cart' => 'Click to see your cart items',
    'click_to_see_wishlist' => 'Click to see your wishlist items',
    'edit_account' => 'EDIT ACCOUNT DETAILS',
    'click_to_edit' => 'Click to edit your personal details and delivery address',
    'cart' => 'CART',
    'cart_is_empty' => 'YOUR CART IS EMPTY',
    'wishlist' => 'WISHLIST',
    'wishlist_empty' => 'YOUR WISHLIST IS EMPTY',
];
