<?php

return [
    'home' => 'Home',
    'our_location' => 'Our Location',
    'we_located' => 'We are located in Fama Keskus',
    'contacts' => 'Contacts',
    'contacts_1' => 'Company:',
    'contacts_2' => 'OÜ "SILVEREST" reg.n.11003444',
    'contacts_3' => 'Address:',
    'contacts_4' => 'Narva Tallinna mnt 19c FAMA KESKUS, Ida-Virumaa, 20303, Estonia, Phone mob. 59020001, Phone / Fax (35) 62620',
    'contacts_5' => 'Email Address:',
    'contacts_6' => 'silverest@silverest.ee',
    'contacts_7' => 'Bank details:',
    'contacts_8' => 'Swedbank Eesti, 221024580301, IBAN: EE722200221024580301',
];
