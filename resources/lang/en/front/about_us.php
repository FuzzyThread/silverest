<?php

return [
    'home' => 'Home',
    'about_us' => 'About Us',
    'about_us_1' => 'Welcome to the shop of the jewelry company SILVEREST!',
    'about_us_2' => 'Jewelry Company "SILVEREST" engaged in the production of jewelry from silver 925 in 2004.
                     Our goal has always been a release of high-quality products at affordable prices.
                     Products are produced with natural and synthetic inserts such as: rauchtopaz, rock crystal, amethyst, zircon, beryl, tourmaline, aquamarine, malachite, agate, onyx and many others.',
    'about_us_3' => 'Manufacture of jewelry located in the north-east Estonia.
                     Our company\'s products executed in the best traditions of classical jewelry production, and also takes into account the tendency of modern fashion jewelry.
                     Our company is continually updating its product range and to track the tastes and demands of customers.
                     For more convenient service, our company opened in 2011, the Internet store.',
    'about_us_4' => 'We wish you a pleasant shopping and meet your requests!',
];
